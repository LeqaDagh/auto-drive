<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

	use RecordsActivity;

		protected $fillable = [
								'name','label'
							  ];

     public function permissions(){

    	return $this->belongsToMany(Permission::class);
    }

    public function givePermissionTo($permission){

    	return $this->permissions()->sync($permission);
    }
}
