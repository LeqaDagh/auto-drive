<?php

namespace App\Http\Controllers;

use App\Model\Equipment;
use App\Model\Make;
use App\Model\Media;
use App\Model\Models;
use App\User;
use App\Model\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class VehicleSellerController extends Controller
{
    private $filters = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Display all the static pages when authenticated
     *
     * @param array $makes, $firstMakes
     * @return \Illuminate\View\View
     */
    public function index()
    {
        
    }

    /**
     * Display all the static pages when authenticated
     *
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function show($id) { 

        $vehicles =  Vehicle::where('user_id', '=', $id)
            ->where('is_deleted', 'no')
            ->join('makes', 'vehicles.make_id', '=', 'makes.id')
            ->join('moulds', 'vehicles.mould_id', '=', 'moulds.id')
            ->select('vehicles.*', 'makes.name', 'moulds.name as model_name')
            ->get();

        return view('pages.more.vehicles', compact('vehicles'));
    }

    /**
     * Create a new vehicle.
     *
     * @param  Illuminate\Http\Request  $request
     * @return \App\Vehicle
     */
    protected function store(Request $request)
    {
       
    }

    /**
     * Display the edit page
     *
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function edit($id) { 

        $vehicle =  DB::table('vehicles')
            ->join('makes', 'vehicles.make_id', '=', 'makes.id')
            ->join('moulds', 'vehicles.mould_id', '=', 'moulds.id')
            ->select('vehicles.*', 'makes.name', 'makes.logo', 'moulds.name as model_name')
            ->where('vehicles.id', '=', $id)
            ->get();  

        $makes = Make::all();
        $firstMakes = Make::all()->take(8);
        $equipments = Equipment::where('vehicle_id', '=', $id)
            ->select('vehicle_id', 'equipment_value', 'equipment_type')
            ->get();
        $bodyColor = $vehicle[0]->body_color;
        $bodyColor= substr($bodyColor, 1);
        $interiorColor = $vehicle[0]->interior_color;
        $interiorColor= substr($interiorColor, 1);

        $payment = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'payment_method')
            ->get();
        $payment = $payment->toArray(); 
        $payment = array_column($payment, 'equipment_value');
        $payment =  implode(',', $payment);

        $screens = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_int_screens')
            ->get();
        $screens = $screens->toArray(); 
        $screens = array_column($screens, 'equipment_value');
        $screens =  implode(',', $screens);

        $seats = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_int_seats')
            ->get();
        $seats = $seats->toArray(); 
        $seats = array_column($seats, 'equipment_value');
        $seats =  implode(',', $seats);
        
        
        $steering = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_int_steering')
            ->get();
        $steering = $steering->toArray(); 
        $steering = array_column($steering, 'equipment_value');
        $steering =  implode(',', $steering);

        $glass = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_int_glass')
            ->get();
        $glass = $glass->toArray(); 
        $glass = array_column($glass, 'equipment_value');
        $glass =  implode(',', $glass);

        $intOthers = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_int_other')
            ->get();
        $intOthers = $intOthers->toArray(); 
        $intOthers = array_column($intOthers, 'equipment_value');
        $intOthers =  implode(',', $intOthers);

        $lights = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_ext_light')
            ->get();
        $lights = $lights->toArray(); 
        $lights = array_column($lights, 'equipment_value');
        $lights =  implode(',', $lights);

        $mirrors = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_ext_mirrors')
            ->get();
        $mirrors = $mirrors->toArray(); 
        $mirrors = array_column($mirrors, 'equipment_value');
        $mirrors =  implode(',', $mirrors);

        $cameras = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_ext_cameras')
            ->get();
        $cameras = $cameras->toArray(); 
        $cameras = array_column($cameras, 'equipment_value');
        $cameras =  implode(',', $cameras);

        $genOther = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_gen_other')
            ->get();
        $genOther = $genOther->toArray(); 
        $genOther = array_column($genOther, 'equipment_value');
        $genOther =  implode(',', $genOther);

        $sensors = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_ext_sensors')
            ->get();
        $sensors = $sensors->toArray(); 
        $sensors = array_column($sensors, 'equipment_value');
        $sensors =  implode(',', $sensors);

        $extOther = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_ext_other')
            ->get();
        $extOther = $extOther->toArray(); 
        $extOther = array_column($extOther, 'equipment_value');
        $extOther =  implode(',', $extOther);

        $furniture = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_int_furniture')
            ->get();
        $sunroof = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_int_sunroof')
            ->get();
        $rims = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_ext_rims')
            ->get();

        return view('pages.more.edit', compact('vehicle', 'makes', 'firstMakes', 'equipment', 'bodyColor', 
            'interiorColor', 'steering', 'screens', 'glass', 'intOthers', 'lights', 'mirrors', 'cameras', 
            'genOther', 'sensors', 'extOther', 'first_names', 'seats', 'furniture', 'sunroof', 'rims', 'payment'));
    }


    /**
     * Update Vehicle.
     *
     * @param  Illuminate\Http\Request  $request, int $id
     * @return \App\Vehicle
     */
    public function update(Request $request, $id) {

        $vehicle = Vehicle::findOrFail($id);
        $vehicle->update($request->all());
        $vehicle->save();
        $equipments = Equipment::where('vehicle_id', $id)->delete();
        
        $types = [
            'payment_method',
            'ext_int_sunroof',
            'ext_int_furniture',
            'ext_int_seats',			
            'ext_int_steering',
            'ext_int_screens',
            'ext_int_glass',
            'ext_int_other',
            'ext_ext_light',
            'ext_ext_mirrors',
            'ext_ext_rims',
            'ext_ext_cameras',
            'ext_ext_sensors',
            'ext_ext_other',
            'ext_gen_other'
        ];

        foreach ($types as $key => $type) {
            if(is_array($request->$type)) {
                foreach ($request->$type as $key => $req) {
                    Equipment::create([
                        'equipment_value' => $req,
                        'equipment_type' => $type,
                        'vehicle_id' => $vehicle->id,
                    ]);
                }
            }
            else {
                Equipment::create([
                    'equipment_value' => $request->$type,
                    'equipment_type' => $type,
                    'vehicle_id' => $vehicle->id,
                ]);	
            }
        }
        return $vehicle;
        //$vehicle->fill($request->all());
       // $vehicle->save();
        //die($request);

    }
    
    /**
     * Display the Image edit page
     *
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function vehicleImages($id) { 

        $photos = Media::where('fileable_id', '=', $id)
            ->where('disk_name', 'vehicles')
            ->get();
         
		$collection = [];

		if(count($photos)) {
			$collection = collect($photos)->map(function ($photo) {
                $photo['thumb_path']   = url('public/storage/'.@$photo['disk_name'],@$photo['file_name']);

			    return ($photo);
			});
		}
       // die($collection);
        return view('pages.more.images', compact('collection', 'id'));
    }

    /**
     * Display all information for special vehicle
     *
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function vehicleInformation($id) { 

        $vehicle = Vehicle::findOrFail($id);
        $user = User::where("id", "=", $vehicle->user_id)->get();
        $photos = Media::where('fileable_id', '=', $id)->get();
        $furniture = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_int_furniture')
            ->get();
        $sunroof = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_int_sunroof')
            ->get();
        $rims = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_ext_rims')
            ->get();

        $sensors = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_ext_sensors')
            ->get();
        $sensors = $sensors->toArray(); 
        $sensors = array_column($sensors, 'equipment_value');
        $sensors =  implode(',', $sensors);

        $screens = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_int_screens')
            ->get();
        $screens = $screens->toArray(); 
        $screens = array_column($screens, 'equipment_value');
        $screens =  implode(',', $screens);

        $seats = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_int_seats')
            ->get();
        $seats = $seats->toArray(); 
        $seats = array_column($seats, 'equipment_value');
        $seats =  implode(',', $seats);
        
        
        $steering = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_int_steering')
            ->get();
        $steering = $steering->toArray(); 
        $steering = array_column($steering, 'equipment_value');
        $steering =  implode(',', $steering);

        $glass = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_int_glass')
            ->get();
        $glass = $glass->toArray(); 
        $glass = array_column($glass, 'equipment_value');
        $glass =  implode(',', $glass);

        $intOthers = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_int_other')
            ->get();
        $intOthers = $intOthers->toArray(); 
        $intOthers = array_column($intOthers, 'equipment_value');
        $intOthers =  implode(',', $intOthers);

        $lights = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_ext_light')
            ->get();
        $lights = $lights->toArray(); 
        $lights = array_column($lights, 'equipment_value');
        $lights =  implode(',', $lights);

        $mirrors = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_ext_mirrors')
            ->get();
        $mirrors = $mirrors->toArray(); 
        $mirrors = array_column($mirrors, 'equipment_value');
        $mirrors =  implode(',', $mirrors);

        $cameras = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_ext_cameras')
            ->get();
        $cameras = $cameras->toArray(); 
        $cameras = array_column($cameras, 'equipment_value');
        $cameras =  implode(',', $cameras);

        $genOther = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_gen_other')
            ->get();
        $genOther = $genOther->toArray(); 
        $genOther = array_column($genOther, 'equipment_value');
        $genOther =  implode(',', $genOther);

        $extOther = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_ext_other')
            ->get();
        $extOther = $extOther->toArray(); 
        $extOther = array_column($extOther, 'equipment_value');
        $extOther =  implode(',', $extOther);
		$collection = [];

		if(count($photos)) {
			$collection = collect($photos)->map(function ($photo) {
                $photo['thumb_path']   = url('public/storage/'.@$photo['disk_name'],@$photo['file_name']);

			    return ($photo);
			});
		}
        
        $make =  DB::table('vehicles')
            ->join('makes', 'vehicles.make_id', '=', 'makes.id')
            ->join('moulds', 'vehicles.mould_id', '=', 'moulds.id')
            ->select('makes.name', 'makes.logo', 'moulds.name as model_name')
            ->where('vehicles.id', '=', $id)
            ->get(); 

        return view('pages.more.information', compact('collection', 'vehicle', 'furniture', 'sensors', 'seats',
        'steering', 'glass', 'intOthers', 'screens', 'sunroof', 'mirrors', 'lights', 'cameras', 'genOther', 
        'extOther', 'rims', 'user', 'make'));
    }

    /**
     * get all images 
     *
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function vehicleAllImages($id) { 

        $photos = Media::where('fileable_id', '=', $id)->get();
         
		$collection = [];

		if(count($photos)) {
			$collection = collect($photos)->map(function ($photo) {
                $photo['thumb_path']   = url('public/storage/'.@$photo['disk_name'],@$photo['file_name']);

			    return ($photo);
			});
		}
      
        $make =  DB::table('vehicles')
            ->join('makes', 'vehicles.make_id', '=', 'makes.id')
            ->join('moulds', 'vehicles.mould_id', '=', 'moulds.id')
            ->select('makes.name', 'makes.logo', 'moulds.name as model_name')
            ->where('vehicles.id', '=', $id)
            ->get(); 

        return view('pages.more.all-images', compact('collection', 'make'));
    }

}