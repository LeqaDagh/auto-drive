<?php

namespace App\Http\Controllers;

use App\Permission;

use App\Activity;

use Illuminate\Http\Request;

use App\Http\Requests\PermissionRequest;

use App\Http\Requests\NullRequest;

use App\Http\Requests;

use Auth;

class PermissionsController extends Controller
{


    public function __construct()
	{


	}


	public function getPermission(){

		 $permissions = Permission::latest('id');

		return $permissions; 				
	}

	public function index()
	{
		
	 	$permissions  =$this->getPermission()->paginate(10);


		 return view('permissions.index',compact('permissions'));
	}

	public function show($id)
	{

		 $permission = Permission::findOrFail($id);


		 return view('permissions.show',compact('permission'));
	}

	public function create()
	{


		 return view('permissions.create');
	}

	public function store(PermissionRequest $request)
	{

		$permissions = Permission::create($request->all());

		return redirect('permissions');
	}


	public function edit($id)
	{

		$permission= Permission::findOrFail($id);


		 return view('permissions.edit',compact('permission'));
	}

	public function update($id,PermissionRequest $request)
	{

		 $permission= Permission::findOrFail($id);

		 $permission->update($request->all());

		return redirect('permissions');
	}

	public function destroy(NullRequest $request)
    {

      

    }
}
