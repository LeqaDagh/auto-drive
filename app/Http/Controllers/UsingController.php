<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Model\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UsingController extends Controller
{
 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the used page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $used = Setting::where('settings_key', 'privacy_and_policy')->get();

        return view('pages.more.using', compact('used'));
    }

}
