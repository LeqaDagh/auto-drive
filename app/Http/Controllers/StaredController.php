<?php

namespace App\Http\Controllers;

use App\Model\Equipment;
use App\User;
use App\Model\Media;
use App\Model\Models;
use App\Model\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StaredController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $vehicles =  Vehicle::where('stared', '=', 'yes')
            ->where('is_deleted', 'no')
            ->join('makes', 'vehicles.make_id', '=', 'makes.id')
            ->join('moulds', 'vehicles.mould_id', '=', 'moulds.id')
            ->select('vehicles.*', 'makes.name', 'moulds.name as model_name')
            ->get();
        
        return view('pages.stared.index', compact('vehicles'));
    }

    /**
     * Display all information for special vehicle
     *
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function show($id) { 

        $vehicle = Vehicle::findOrFail($id);
        $user = User::where("id", "=", $vehicle->user_id)->get();
        $photos = Media::where('fileable_id', '=', $id)->get();
        $furniture = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_int_furniture')
            ->get();
        $sunroof = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_int_sunroof')
            ->get();
        $rims = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_ext_rims')
            ->get();

        $sensors = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_ext_sensors')
            ->get();
        $sensors = $sensors->toArray(); 
        $sensors = array_column($sensors, 'equipment_value');
        $sensors =  implode(',', $sensors);

        $screens = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_int_screens')
            ->get();
        $screens = $screens->toArray(); 
        $screens = array_column($screens, 'equipment_value');
        $screens =  implode(',', $screens);

        $seats = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_int_seats')
            ->get();
        $seats = $seats->toArray(); 
        $seats = array_column($seats, 'equipment_value');
        $seats =  implode(',', $seats);
        
        
        $steering = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_int_steering')
            ->get();
        $steering = $steering->toArray(); 
        $steering = array_column($steering, 'equipment_value');
        $steering =  implode(',', $steering);

        $glass = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_int_glass')
            ->get();
        $glass = $glass->toArray(); 
        $glass = array_column($glass, 'equipment_value');
        $glass =  implode(',', $glass);

        $intOthers = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_int_other')
            ->get();
        $intOthers = $intOthers->toArray(); 
        $intOthers = array_column($intOthers, 'equipment_value');
        $intOthers =  implode(',', $intOthers);

        $lights = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_ext_light')
            ->get();
        $lights = $lights->toArray(); 
        $lights = array_column($lights, 'equipment_value');
        $lights =  implode(',', $lights);

        $mirrors = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_ext_mirrors')
            ->get();
        $mirrors = $mirrors->toArray(); 
        $mirrors = array_column($mirrors, 'equipment_value');
        $mirrors =  implode(',', $mirrors);

        $cameras = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_ext_cameras')
            ->get();
        $cameras = $cameras->toArray(); 
        $cameras = array_column($cameras, 'equipment_value');
        $cameras =  implode(',', $cameras);

        $genOther = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_gen_other')
            ->get();
        $genOther = $genOther->toArray(); 
        $genOther = array_column($genOther, 'equipment_value');
        $genOther =  implode(',', $genOther);

        $extOther = Equipment::where('vehicle_id', '=', $id)
            ->where('equipment_type', '=', 'ext_ext_other')
            ->get();
        $extOther = $extOther->toArray(); 
        $extOther = array_column($extOther, 'equipment_value');
        $extOther =  implode(',', $extOther);
		$collection = [];

		if(count($photos)) {
			$collection = collect($photos)->map(function ($photo) {
                $photo['thumb_path']   = url('public/storage/'.@$photo['disk_name'],@$photo['file_name']);

			    return ($photo);
			});
		}
        
        $make =  DB::table('vehicles')
            ->join('makes', 'vehicles.make_id', '=', 'makes.id')
            ->join('moulds', 'vehicles.mould_id', '=', 'moulds.id')
            ->select('makes.name', 'makes.logo', 'moulds.name as model_name')
            ->where('vehicles.id', '=', $id)
            ->get(); 

        return view('pages.stared.show', compact('collection', 'vehicle', 'furniture', 'sensors', 'seats',
        'steering', 'glass', 'intOthers', 'screens', 'sunroof', 'mirrors', 'lights', 'cameras', 'genOther', 
        'extOther', 'rims', 'user', 'make'));
    }
}
