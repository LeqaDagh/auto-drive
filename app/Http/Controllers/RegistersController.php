<?php

namespace App\Http\Controllers;

use App\User;

use App\Activity;

use App\Region;

use App\Station; 

use App\Role; 

use App\Account; 

use App\Seller; 

use App\Attribute; 

use App\Http\Requests;

use App\Http\Vehicle;

use App\Http\Requests\RegisterRequest;

use App\Http\Requests\NullRequest;

use App\Http\Requests\PhotosRequest;

use App\Http\Requests\SellerRequest;

use App\Http\Requests\VehicleRequest;

use Illuminate\HttpResponse;

use Request;

use Auth;

use DB;

use PDF;

use App\Enums\UserGroups;

use Illuminate\Support\Str;

use App\Enums\EquipmentsTypes;


class RegistersController extends Controller
{
    
	private $METHOD ;

	private $role ;

	private $group = UserGroups::REGISTER; 

  	public function __construct()
	{
			 $this->role = trans('main.user_groups')[$this->group]['default_role'];

	}




	private function getRegisters(NullRequest $request)
	{
		$registers = User::register()
					  ->hisOwen()
 		   			  ->equalRegion($request->get('region_id'))
			          ->likeUsername($request->get('username'))
			          ->likeName($request->get('name'))
			          ->likeEmail($request->get('email'))
			          ->likeMobile($request->get('mobile'))
			          ->equalUserStatus($request->get('user_status'));

		return $registers;		          

	}
    public function index(NullRequest $request)
	{

		$paginate  = 10;
		if($request->pagination)
		{
			$paginate  = $request->pagination;	
		}

		if($paginate>2000)
		{
			$paginate = 2000;
		}


 		 $registers = $this->getRegisters($request)->paginate($paginate);


		 $regions 	= Region::pluck('name','id');
		 $regions->prepend(trans('main.all'),0);

		 $user_status = collect(trans('main.user_status'));
		 $user_status->prepend(trans('main.all'),0);

		 Auth::user()->recordUserActivity($this->METHOD);

		 return view('registers.index',compact('registers',"regions","user_status"));
	}

    public function pdf(NullRequest $request)
	{
		$paginate  = 10;

		if($request->pagination)
		{
			$paginate  = $request->pagination;	
		}

		if($paginate>2000)
		{
			$paginate = 2000;
		}


 		$registers = $this->getRegisters($request)->paginate($paginate);


		 $view = \View::make('registers.pdf')->with(['registers'=>$registers]);
         $html = $view->render();
		 PDF::SetFooterMargin(40);
         PDF::SetTitle('طباعة تقرير');
         PDF::AddPage();
         PDF::SetAutoPageBreak(TRUE, 40);
         PDF::setRTL(true);
         PDF::SetFont('dejavusans', '', 10); 
         PDF::writeHTML($html, true, false, true, false, '');
       	 ob_end_clean();
         PDF::Output(time(). '_' . rand(1,999) . '_' .'registers.pdf');

	}

	public function create()
	{


		 $regions 	= Region::whereNull('parent')->pluck('name','id');

		 Auth::user()->recordUserActivity($this->METHOD);

		 return view('registers.create',compact("regions"));	
	}

	public function store(RegisterRequest $request)
	{


		$region = Region::find($request->region_id);

		$role = Role::find($this->role);

		$request->merge(['group' => $this->group]);

		$register = new \App\User($request->all());

		$register->region()->associate($region);

		$register->save();

		$register->roles()->sync([@$role->id]);

		return redirect('registers');	
	}

	public function edit($id)
	{
		 $register 		= User::register()->hisOwen()->findOrFail($id);

		 $regions 	= Region::whereNull('parent')->pluck('name','id');	

		 Auth::user()->recordUserActivity($this->METHOD);

		 return view('registers.edit',compact('register','regions'));	
	}
	public function update($id,RegisterRequest $request)
	{
	

		$region = Region::find($request->region_id);

		$register = User::register()->hisOwen()->findOrFail($id);

		$register->fill($request->all());

		$register->region()->associate($region);


		$register->save();
		
		return redirect('registers/'.$id.'/edit');
		
	}

	public function show($id)
	{
		$register = User::register()->hisOwen()->findOrFail($id);


		 return view('registers.show',compact('register'));

	}


	public function destroy(NullRequest	 $request)
	{
		
	}

	public function addSellerForRegister(NullRequest $request)
	{
		$register = User::register()->hisOwen()->findOrFail($request->register_id);

		$regions = Region::all()->pluck('name','id');
		$regions->prepend(trans('main.choose'),0);

		$publish_status = collect(trans('main.one_two_status'));
		$can_ignor_price = collect(trans('main.one_two_status'));

		

		return view('registers.js.show.add_seller_for_register_modal',compact('register','regions','publish_status','can_ignor_price'));
	}
	public function addSellerForRegisterPost(SellerRequest $request,PhotosRequest $photos_request)
	{

		
		$register = User::register()->hisOwen()->findOrFail($request->register_id);

		$region = Region::find($request->region_id);

		$slug = Str::slug($request->name, '_');

		$request->merge(['slug'=>$slug]);

		$seller = new \App\Seller($request->all());

		$seller->user()->associate($register);

		$seller->region()->associate($region);

		$seller->save();

		 $photos  = [];

 			 if( $request->hasFile('photos'))
 			 {
	 			 foreach ($request->file('photos') as $key => $file) 
	 			 {
						 $original_name = $file->getClientOriginalName();
						 $extra_name    = uniqid().'_'.time().'_'.uniqid().'.'.$file->extension();
			        	 $file->storeAs('sellers', $extra_name);
			        	 $photos[] = [
			        	 	'disk_name' =>'sellers',
			        	 	'file_name' =>$extra_name,
			        	 	'file_size' =>$file->getSize(),
			        	 	'content_type'=>$file->getClientMimeType(),
			        	 	'title'=>$request->name,
			        	 	'description'=>'',
			        	 	'field'=>'photos',
			        	 	'sort_order'=>'1'
			        	 ];

			 

			        	 
	 			 }	
        	 }
        	






		$seller->photos()->createMany($photos);

		return $seller;
	}


	public function getIdForType($field)
	{
  		return DB::table('types')->select('id')->where('search_field',$field)->first()->id;
	}

}
