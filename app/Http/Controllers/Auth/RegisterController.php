<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Rules\UniqueMobile;
use App\Rules\UniqueUserName;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    private $client = null;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
           
             'username'=>['required','regex:/^[a-z0-9]{4,16}+$/',new UniqueUserName($this->client)],
             'name' => 'required|regex:/^[\p{Arabic}a-z 0-9]{1,}$/u',
             'region_id'=>'required|integer',
             'mobile'=> ['digits:10','starts_with:05',new UniqueMobile($this->client)],
             'email' => 'required|string|email|max:255|unique:users,email,'.$this->client,
             'password' => 'required|string|min:6|confirmed',
        ]);



    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // return User::create([
        //     'name' => $data['name'],
        //     'email' => $data['email'],
        //     'password' => Hash::make($data['password']),
        // ]);


        $region = \App\Region::find($data['region_id']);

        $role = \App\Role::find(2);

        $client = new \App\User([
            'name' => $data['name'],
            'username'=>$data['username'],
            'mobile'=>$data['mobile'],
            'group'=>'cl',
            'email' => $data['email'],
        ]);

        $client->region()->associate($region);

        $client->save();

        $client->roles()->sync([@$role->id]);

        return $client;
    }
}
