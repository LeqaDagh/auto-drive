<?php

namespace App\Http\Controllers;

use App\Type;

use App\Activity;

use Illuminate\Http\Request;

use App\Http\Requests\TypeRequest;

use App\Http\Requests\NullRequest;

use App\Http\Requests;

use Auth;

use Illuminate\Support\Str;

class TypesController extends Controller
{
	private $METHOD ;

    public function __construct()
	{

	$this->METHOD = request()->route()->getAction()['controller'];	

	}


	public function getType(){

		 $types = Type::latest('id');

		return $types; 				
	}

	public function index(NullRequest $request )
	{
	
		
	 	$types  =$this->getType()->paginate(10);

	 	Auth::user()->recordUserActivity($this->METHOD);

		 return view('types.index',compact('types'));
	}

	public function show($id)
	{

		 $type = Type::findOrFail($id);

		 Auth::user()->recordUserActivity($this->METHOD);

		 return view('types.show',compact('type'));
	}

	public function create()
	{
		 $search_types = trans('main.search_types');

		 Auth::user()->recordUserActivity($this->METHOD);

		 return view('types.create',compact('search_types'));
	}

	public function store(TypeRequest $request)
	{


		$slug = Str::slug($request->name, '_');

		$request->merge(['slug'=>$slug]);

		$types = Type::create($request->all());

		return redirect('types');
	}


	public function edit($id)
	{

		  $type = Type::findOrFail($id);

		  $search_types = trans('main.search_types');

		 Auth::user()->recordUserActivity($this->METHOD,Auth::user());

		 return view('types.edit',compact('type','search_types'));
	}

	public function update($id,TypeRequest $request)
	{

		 $type= Type::findOrFail($id);

		 $type->update($request->all());

		return redirect('types');
	}

	public function destroy(NullRequest $request)
    {

      

    }

 
}
