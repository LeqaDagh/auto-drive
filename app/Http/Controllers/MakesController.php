<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use Request;

use App\Make;

use App\Activity;

use App\Http\Requests\MakeRequest;

use App\Http\Requests\NullRequest;

use Auth;

class MakesController extends Controller
{

    	public function __construct()
	{


	}

    public function getMakesByParentId(Request $request){

    	$makes = Make::get();

    	return $makes;
    }

    public function getMakes($request)
	{
		return $makes = Make::latest();
			}

	public function getXhrMakes(NullRequest $request)

	{

			$makes = Make::select("makes.*")
						  ->equalParent($request->get('parent'))
						  ->get();
		 
	

			return $makes;
	}

	public function index(NullRequest $request)
	{
	
		 $makes =$this->getMakes($request)->paginate(50);


		 return view('makes.index',compact('makes'));
	}

	public function show($id,NullRequest $request)
	{

		 $make 				= Make::findOrFail($id);



		 return view('makes.show',compact('make'));
	}

	public function create(NullRequest $request)
	{

	


		 return view('makes.create');
	}

	public function store(MakeRequest $request)
	{
			
		$make = new \App\Make($request->all());

		$make->save();


		return redirect('makes');
	}


	public function edit($id,NullRequest $request)
	{


		$make = Make::findOrFail($id);


		 return view('makes.edit',compact('make'));
	}

	public function update($id,MakeRequest $request)
	{

		$make = Make::findOrFail($id);
		$make->fill($request->all());
		$make->save();
		return redirect('makes');
	}

	 public function destroy(NullRequest $request)
    {

  

    }
}
