<?php

namespace App\Http\Controllers;

use App\Payment;

use App\Activity;

use Illuminate\Http\Request;

use App\Http\Requests\PaymentRequest;

use App\Http\Requests\NullRequest;

use App\Http\Requests;

use Auth;

class PaymentsController extends Controller
{
	private $METHOD ;

    public function __construct()
	{


	}


	public function getPayment(){

		 $payments = Payment::latest('id');

		return $payments; 				
	}

	public function index()
	{
		
	 	$payments  =$this->getPayment()->paginate(10);

	 	Auth::user()->recordUserActivity($this->METHOD);

		 return view('payments.index',compact('payments'));
	}

	public function show($id)
	{

		 $payment = Payment::findOrFail($id);

		 Auth::user()->recordUserActivity($this->METHOD);

		 return view('payments.show',compact('payment'));
	}

	public function create()
	{

		 Auth::user()->recordUserActivity($this->METHOD);

		 return view('payments.create');
	}

	public function store(PaymentRequest $request)
	{

		$payments = Payment::create($request->all());

		return redirect('payments');
	}


	public function edit($id)
	{

		$payment= Payment::findOrFail($id);

		 Auth::user()->recordUserActivity($this->METHOD,Auth::user());

		 return view('payments.edit',compact('payment'));
	}

	public function update($id,PaymentRequest $request)
	{

		 $payment= Payment::findOrFail($id);

		 $payment->update($request->all());

		return redirect('payments');
	}

	public function destroy(NullRequest $request)
    {

      

    }
}
