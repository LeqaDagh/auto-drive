<?php
	namespace App\Http\Controllers\Api\V1;

	trait IniFunctions{

	private function appendSellers(){

		$sellers =  \App\Seller::all();

		$sellers = collect($sellers)->map(function ($element) {

				$element['checked'] = false;
			 return $element;

		});


		$this->filters['seller_id'] =$sellers;
	}
	private function appendMakes(){

		$type = \App\Type::where('search_field','make_id')->first();
		$this->filters['make_id'] = \App\Attribute::with('childs.childs')->where('type_id',$type->id)->orderBy('order','asc')->get();
		$this->appendMakeLogo();
	}
	private function getItemsArray(){
		$arr = [
				['item_key'=>'sell_type','types_key'=>'sell_type_types'],
				['item_key'=>'vehicle_type','types_key'=>'vehicle_type_types'],
				['item_key'=>'body','types_key'=>'body_types'],
				['item_key'=>'gear','types_key'=>'gear_types'],
				['item_key'=>'fuel','types_key'=>'fuel_types'],
				['item_key'=>'drivetrain_system','types_key'=>'drivetrain_system_types'],
				['item_key'=>'drivetrain_type','types_key'=>'drivetrain_type_types'],
				['item_key'=>'payment_method','types_key'=>'payment_method_types'],
				['item_key'=>'num_of_seats','types_key'=>'num_of_seats_types'],
				['item_key'=>'num_of_doors','types_key'=>'num_of_doors_types'],
				['item_key'=>'num_of_keys','types_key'=>'num_of_keys_types'],
				['item_key'=>'vehicle_status','types_key'=>'vehicle_status_types'],
				['item_key'=>'customs_exemption','types_key'=>'customs_exemption_types'],
				['item_key'=>'previous_owners','types_key'=>'previous_owners_types'],
				['item_key'=>'origin','types_key'=>'origin_types'],
				['item_key'=>'colors','types_key'=>'color_types'],
				['item_key'=>'ext_int_furniture','types_key'=>'ext_int_furniture'],
				['item_key'=>'ext_int_seats','types_key'=>'ext_int_seats'],
				['item_key'=>'ext_int_steering','types_key'=>'ext_int_steering'],
				['item_key'=>'ext_int_screens','types_key'=>'ext_int_screens'],
				['item_key'=>'ext_int_sunroof','types_key'=>'ext_int_sunroof'],
				['item_key'=>'ext_int_glass','types_key'=>'ext_int_glass'],
				['item_key'=>'ext_int_other','types_key'=>'ext_int_other'],
				['item_key'=>'ext_ext_light','types_key'=>'ext_ext_light'],
				['item_key'=>'ext_ext_mirrors','types_key'=>'ext_ext_mirrors'],
				['item_key'=>'ext_ext_rims','types_key'=>'ext_ext_rims'],
				['item_key'=>'ext_ext_cameras','types_key'=>'ext_ext_cameras'],
				['item_key'=>'ext_ext_sensors','types_key'=>'ext_ext_sensors'],
				['item_key'=>'ext_ext_other','types_key'=>'ext_ext_other'],
				['item_key'=>'ext_gen_other','types_key'=>'ext_gen_other']
			];
			return $arr;
	}
	private function appendMakeLogo()
	{
		$this->filters['make_id'] = collect($this->filters['make_id'])->map(function ($element) {

				$element['logo'] = url('public/makes',$element['logo']);
			 return $element;

		});

	}
	private function appendSelectedMakeLogo()
	{
		$this->filters['selected_model']['make']['logo'] = url('public/makes',$this->filters['selected_model']['make']['logo']);


	}
	
	private function appendChecked($elements,$is_exist=false)
	{
		$this->is_exist = $is_exist;
		$collect = collect($elements)->map(function ($element,$key) {
				
		
			 $element['checked'] = false;
		 	if( $this->is_exist AND $this->filters['submit_data']['body']==$key)
		 	{
			 $element['checked'] = true;
		 	}

			    return $element;
			});

		return $collect->toArray();
	}

	private function appendToFilter()
	{
		$items = $this->getItemsArray();

		foreach ($items as $key => $item) 
		{
			$this->filters[$item['item_key']] = $this->appendChecked(trans('types.'.$item['types_key']));
		}
		
	}


	private function appendLogos(){


   		 $this->filters['body'] = collect($this->filters['body'])->map(function ($att) {
				

			$att['logo'] = url('public/cars',$att['logo']);

			$att['alt_logo'] = url('public/cars',$att['alt_logo']);

			$att['title']  = strtoupper($att['title']);

			    return $att;
			});

	}

	private function appendSubmitData($id)
	{


						  $vehicle = \App\Vehicle::findOrFail($id);
						  $equipments = $vehicle->equipments()->pluck('item')->toArray();
						
					 	  $submit_data = [];
					      $submit_data['seller_id'] = $vehicle->seller_id;
                          $submit_data['sell_type'] = $vehicle->sell_type;
                          $submit_data['vehicle_type'] = $vehicle->vehicle_type;
                          $submit_data['make_id'] = $vehicle->make_id;
                          $submit_data['model_id'] = $vehicle->model_id;
                          $submit_data['body'] = $vehicle->body;
                          $submit_data['extra_title'] = $vehicle->extra_title;
                          $submit_data['year_of_product'] = $vehicle->year_of_product;
                          $submit_data['year_of_work'] = $vehicle->year_of_work;
                          $submit_data['price'] = $vehicle->price;
                          $submit_data['power'] = $vehicle->power;
                          $submit_data['hp'] = $vehicle->hp;
                          $submit_data['mileage'] = $vehicle->mileage;
                          $submit_data['gear'] = $vehicle->gear;
                          $submit_data['fuel'] = $vehicle->fuel;
                          $submit_data['drivetrain_system'] = $vehicle->drivetrain_system;
                          $submit_data['drivetrain_type'] = $vehicle->drivetrain_type;
                          $submit_data['payment_method'] = $vehicle->payment_method;
                          $submit_data['num_of_seats'] = $vehicle->num_of_seats;
                          $submit_data['vehicle_status'] = $vehicle->vehicle_status;
                          $submit_data['body_color'] = $vehicle->body_color;
                          $submit_data['interior_color'] = $vehicle->interior_color;
                          $submit_data['customs_exemption'] = $vehicle->customs_exemption;
                          $submit_data['num_of_doors'] = $vehicle->num_of_doors;
                          $submit_data['license_date'] = $vehicle->license_date; 
                          $submit_data['previous_owners'] = $vehicle->previous_owners;
                          $submit_data['origin'] = $vehicle->origin;
                          $submit_data['num_of_keys'] = $vehicle->num_of_keys;
                          $submit_data['desc'] = $vehicle->desc;
                          $submit_data['ext_int_furniture'] = 'colt';
                          $submit_data['ext_int_sunroof'] = 'pano';

                          
                          
                  
                          $submit_data['ext_int_seats']=array_intersect(array_keys(trans('types.ext_int_seats')), $equipments);
                          $submit_data['ext_int_steering']=array_intersect(array_keys(trans('types.ext_int_steering')), $equipments);
                          $submit_data['ext_int_screens']=array_intersect(array_keys(trans('types.ext_int_screens')), $equipments);
                  
                          $submit_data['ext_int_glass']=array_intersect(array_keys(trans('types.ext_int_glass')), $equipments);
                          $submit_data['ext_int_other']=array_intersect(array_keys(trans('types.ext_int_other')), $equipments);
                          $submit_data['ext_ext_light']=array_intersect(array_keys(trans('types.ext_ext_light')), $equipments);
                          $submit_data['ext_ext_mirrors']=array_intersect(array_keys(trans('types.ext_ext_mirrors')), $equipments);
                          $submit_data['ext_ext_rims']=array_intersect(array_keys(trans('types.ext_ext_rims')), $equipments);
                          $submit_data['ext_ext_cameras']=array_intersect(array_keys(trans('types.ext_ext_cameras')), $equipments);
                          $submit_data['ext_ext_sensors']=array_intersect(array_keys(trans('types.ext_ext_sensors')), $equipments);
                          $submit_data['ext_ext_other']=array_intersect(array_keys(trans('types.ext_ext_other')), $equipments);
                          $submit_data['ext_gen_other']=array_intersect(array_keys(trans('types.ext_gen_other')), $equipments);

						  $this->filters['submit_data'] = $submit_data;

						  $this->filters['selected_model'] = $vehicle->model;
						  $this->filters['selected_model']['make'] = $vehicle->make;
	}

	private function setCheckedStatus()
	{

		$eqs = $this->getEquipmentsArray();

		foreach ($eqs as $key => $eq) {
	
			$arr = $this->filters['submit_data'][$eq];

				foreach ($arr as $key => $ar) {

					$this->filters[$eq][$ar]['checked'] = true;		
				}

			}
	}


	

	private function getEquipmentsArray(){

						return [
                        'ext_int_seats',
                        'ext_int_steering',
                        'ext_int_screens',
                        'ext_int_glass',
                        'ext_int_other',
                        'ext_ext_light',
                        'ext_ext_mirrors',
                        'ext_ext_rims',
                        'ext_ext_cameras',
                        'ext_ext_sensors',
                        'ext_ext_other',
                        'ext_gen_other'];
	}
      

       }