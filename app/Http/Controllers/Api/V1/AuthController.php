<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\NullRequest;

use App\User;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;
use DB, Hash, Illuminate\Support\Facades\Password;
use App\Transformers\UserTransformer;


use Auth;


class AuthController extends Controller
{
  
    protected $userTransformer;

    public function __construct(UserTransformer $userTransformer)
    {

       $this->userTransformer = $userTransformer;


    }


    /**
     * API Login, on success return JWT Auth token
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {

         $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];



        try {
            // attempt to verify the credentials and create a token for the user
            if (! Auth::attempt($credentials)) {
                return response()->json(['success' => false, 'error' => 'We cant find an account with this credentials.'], 401);
            }
        } 
        catch (Exception $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'error' => 'Failed to login, please try again.'], 500);
        }


        $token = \Str::random(60);

        Auth::user()->update(['api_token'=>$token]);

        $user =  $this->userTransformer->transform(Auth::user()->toArray());

        

        // all good so return the token
        return response()->json(['success' => true, 'data'=> [ 'token' => $token,'user'=>$user]]);
    }


    /**
     * Log out
     * Invalidate the token, so user cannot use it anymore
     * They have to relogin to get a new token
     *
     * @param Request $request
     */
    public function logout(Request $request) {

   
        $this->validate($request, ['token' => 'required']);

        try {
            JWTAuth::invalidate($request->input('token'));
            return response()->json(['success' => true]);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
        }
    }


    public function profile()
    {
        
        $user = Auth::user();
        
        return  $this->userTransformer->transform($user->toArray());    
    
    }



}