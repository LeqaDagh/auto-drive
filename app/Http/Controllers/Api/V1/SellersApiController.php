<?php
namespace App\Http\Controllers\Api\V1;

use App\Http\Requests;
use Illuminate\HttpResponse;
use App\Http\Controllers\ApiController;
use Request;
use App\Http\Requests\NullRequest;
use App\User;
use App\Vehicle;
use App\Transformers\SellerTransformer;
use App\Transformers\VehicleTransformer;


class SellersApiController extends ApiController
{
	protected $sellerTransformer;

    public function __construct(SellerTransformer $sellerTransformer,VehicleTransformer $vehicleTransformer)
	{
		 $this->sellerTransformer = $sellerTransformer;
		 $this->vehicleTransformer = $vehicleTransformer;
	}
	


	public function index(NullRequest $request)
	{
	
		$sellers = Seller::with(['region','cover','photos'])
						->paginate(1);

		return $this->respondWithPagination($sellers, ['data' => $this->sellerTransformer->transformCollection($sellers->all())]);

	

	

	}	


	public function getSeller($id)
	{
	
		$seller = User::with(['region','cover','photos'])
						->seller()
						->canPublish()
						->likeSellerName(request()->text)
						->orderBy('seller_order','desc')
						->findOrFail($id);

		$vehicles = Vehicle::notDeleted()->with(['seller','make','mould','cover','photos'])
							->where('user_id',$seller->id)
							->limit(10)
							->get();
		$result = [];
		
		$result['seller'] = $this->sellerTransformer->transform($seller)	;

		$result['vehicles'] = $this->vehicleTransformer->transformCollection($vehicles->all());
	

		return $result;

	

	}

	public function mySellers(NullRequest $request){

		$sellers = Seller::with(['region','cover','photos'])
						->paginate(10);

		return $this->respondWithPagination($sellers, ['data' => $this->sellerTransformer->transformCollection($sellers->all())]);

	}

	public function sellers(NullRequest $request){

		$sellers = User::with(['region','cover','photos'])
						->seller()
						->canPublish()
						->likeSellerName(request()->text)
						->paginate(10);

		return $this->respondWithPagination($sellers, ['data' => $this->sellerTransformer->transformCollection($sellers->all())]);

	}


	
	
}

