<?php
namespace App\Http\Controllers\Api\V1;

use App\Http\Requests;
use App\Http\Requests\NullRequest;
use App\Http\Requests\CompanyRegistrationRequest;
use App\Http\Requests\PersonalRegistrationRequest;
use App\Http\Requests\EditPersonalRequest;

use App\Http\Requests\EditPasswordRequest;






use Illuminate\HttpResponse;

use App\Http\Controllers\ApiController;

use Request;

use App\Type;
use App\Vehicle;
use App\Region;
use App\Seller;
use App\Attribute;
use App\Transformers\VehicleTransformer;
use Illuminate\Support\Str;
use Auth;
use App\SimpleXLSX;
use DB;
use Image;
use App\Transformers\UserTransformer;

class HomeApiController extends ApiController
{
	use IniFunctions;

	protected $vehicleTransformer;

	private $filters;
	private $is_exist = false;
	protected $userTransformer;

    public function __construct(VehicleTransformer $vehicleTransformer,UserTransformer $userTransformer)
	{
		 $this->vehicleTransformer = $vehicleTransformer;
		 $this->userTransformer = $userTransformer;
	}

		public function doData()
		{
			set_time_limit(30000);
		



			$files = (\File::files(public_path('makes/data/data/')));

			foreach ($files as $key => $file) 
			{

	
				$items = array_map('str_getcsv', file(public_path('makes/data/data/'.$file->getFileName()))); 
		

				$data = [
								'name'=>str_replace('.csv','',$file->getFileName()),
								'slug'=>Str::random(40),
								'logo'=>$items[0][0],
								'order'=>rand(1,9),
								'visible'=>'yes',
							];


				$make_id = DB::table('makes')->insertGetId($data);

				foreach ($items as $key => $item) 
				{
				
				if($key>0)
				{		
					$data = [
									'make_id'=>	$make_id,
									'name'=>$item[0],
									'slug'=>Str::random(40),
									'logo'=>$items[0][0],
									'order'=>rand(1,9),
									'visible'=>'yes',
								];

					$mould_id =  DB::table('moulds')->insertGetId($data);


				if(count($item)>1)
				{


					foreach ($item as $k => $i) 
					{
						if($k>0 AND $i!=""){
									$data = [
									'make_id'=>	NULL,
									'parent'=>$mould_id,
									'name'=>$i,
									'slug'=>Str::random(40),
									'logo'=>$items[0][0],
									'order'=>rand(1,9),
									'visible'=>'yes',
								];

				
						  DB::table('moulds')->insertGetId($data);
						}
					}
				}

				}

			
				
				}

			}

			

			dd(1);



			foreach ($items as $key => $item) 
			{
				
				$start = false;

				$ind = 0;
				$make_id = NULL;
				$parent  = NULL;
				$ignor_insert = false;
			
			

				foreach ($item as $key => $it) 
				{

					if($ind!=2 OR $ind!=1)
					{

					if($ind==0){
						$update_parent = true;
					}	

					$data = [
								'name'=>$it,
								'slug'=>Str::random(40),
								'logo'=>$item[1],
								'mobile_logo'=>$item[1],
								'order'=>rand(1,9),
								'childs_select_type'=>'single',
								'visible'=>'yes',
							];

						if($ind==0){
							$data['type_id']=$make;
							$data['parent']=NULL;

						}	
						else
						{
							$data['type_id']=$model;
							$data['parent']=$parent;
						}

						if($ind==1 OR $ind==2 OR $it=="" OR $it=="|++" OR $it=="|--")
						{
							$ignor_insert = true;
						}
						else
						{
							$ignor_insert = false;	
						}

						if(!$ignor_insert)
						{
							
						
						 $id = DB::table('attributes')->insertGetId($data);

						
						 if( $ind==0   OR (isset($item[$ind+1]) AND $item[$ind+1]=="|++")  OR (isset($item[$ind-1]) AND $item[$ind-1]=="|--")  )
						 {
							
							 if( (isset($item[$ind+1]) AND $item[$ind+1]=="|++") )
							{
							 $parent = $make_id;	
							}
							else if( (isset($item[$ind-1]) AND $item[$ind-1]=="|--") )
							{
							 $parent = $make_id;	
							}
							
							else
							{
								$parent = $id;	
								if($ind=0)
							 	{
							 		$make_id = $id;
							 	}
							}
							
						 }
						 else
						 {
						 	$parent = $parent;	
						 		
						 }
						}
					

				


				}
				$ind++;
				}
			}
		}


	public function editMyProfile(EditPersonalRequest $request)
	{
		
		$region = Region::find($request->region_id);

		$client = Auth::user();

		$client->fill($request->all());

		$client->region()->associate($region);

		$client->save();


		return $client;
	}
	public function editPassword(EditPasswordRequest $request)
	{
		
		$client  = Auth::user();
		$client->update(['password'=>$request->password]);
		return $client;
	}
	
	public function myProfile(){

		$user = Auth::user();

		$regions = Region::all();

		$max_vehicles = $user->max_vehicles;

		$vehicles = $user->vehicles()->count();

		$log_user = $this->userTransformer->transform(Auth::user()->toArray());
 				

		return ['log_user'=>$log_user,'user'=>$user , 'regions'=>$regions,'max_vehicles'=>$max_vehicles,'vehicles'=>$vehicles];

	}

	public function addVehicleFilters()
	{

				 $this->appendMakes();
				 $this->appendSellers();
				 $this->appendToFilter();
				 $this->appendLogos();
		return   $this->filters;
	}
	public function editVehicles($id)
	{


 
				 $this->appendMakes();
				 $this->appendSellers();
				 $this->appendToFilter();
				 $this->appendSubmitData($id);
				 $this->setCheckedStatus();
				 $this->filters['body'] =  $this->appendChecked(trans('types.body_types'),true);
				 $this->appendLogos();
				 $this->appendSelectedMakeLogo();
		return   $this->filters;
	}
	

	
	public function searchFilters()
	{
		$attributes 			= [];
		
		$types = Type::with('attributes.childs.childs')->get();
		foreach ($types as $key => $type) 
		{
			
			$filters[$type->search_field] = $type->toArray();
		}
		$filters['sell_type'] 			= trans('types.sell_type_types');
		$filters['vehicle_type'] 		= trans('types.vehicle_type_types');

		$filters['body'] 				= collect(trans('types.body_types'))->map(function ($item) {
																					  $item['checked'] = false; 	
																			    return ($item);
																			});;
		$filters['gear'] 				= collect(trans('types.gear_types'))->map(function ($item) {
																					  $arr = [];
																					  $arr ['name']= $item;
																					  $arr['checked'] = false; 	
																					  $item = $arr;
																			    return ($arr);
																			});;

		$filters['fuel'] 				= trans('types.fuel_types');
		$filters['drivetrain_system'] 	= trans('types.drivetrain_system_types');
		$filters['drivetrain_type'] 	= trans('types.drivetrain_type_types');
		$filters['payment_method'] 		= trans('types.payment_method_types');
		$filters['num_of_seats'] 		= trans('types.num_of_seats_types');
		$filters['vehicle_status'] 		= trans('types.vehicle_status_types');
		$filters['customs_exemption'] 	= trans('types.customs_exemption_types');
		$filters['previous_owners'] 	= trans('types.previous_owners_types');
		$filters['origin'] 				= trans('types.origin_types');
		$filters['colors'] 				= trans('types.color_types');
		$filters['interior_colors'] 	= trans('types.interior_color_types');
		

		
		
		$arr = [];

		foreach ($filters['body'] as $key => $body) 
		{
		
			$att = $body;
		
			$att['logo'] = url('public/cars',$att['logo']);

			$att['title']  = strtoupper($att['title']);

			$arr [] = $att; 
		}

		$filters['body'] = $arr;




		return $filters;
	}



	public function filters()
	{
		$attributes 			= [];
		$filters['attributes']  = Type::with('attributes.childs')->get()->toArray();;	
		$filters['regions'] 	= Region::get()->toArray();
		$filters['sellers'] 	= Seller::get()->toArray();

		$filters['attributes'] [] =
		 ['mobile_list_type'=>'slider','name'=>'number_of_seats','min'=>2,'max'=>8,'steps'=>2,'snaps'=>false,'ticks'=>false,'dualKnobs'=>false];


		return $filters;
	}	
	public function regions()
	{
		$regions 	= Region::all();

		return $regions;
	}
	public function companyRegistration(CompanyRegistrationRequest $request)
	{
		
		$region = Region::find($request->region_id);

		$seller_region = Region::find($request->region_id);

		$username = Str::random(14);

		$request->merge([
						'username'=>$username,
						'group' => 'se',
						'max_vehicles'=>2,
						'seller_email' => request()->email,
						'seller_address' => request()->address,
					]);

		$client = new \App\User($request->all());

		$client->region()->associate($region);

		$client->seller_region()->associate($region);

		$client->save();

		$client->roles()->sync([2]);

		$photos  = [];

		 if( $request->has('photos'))
		 {

		foreach ($request->photos as $key => $image) 
		{

	 		if (preg_match('/^data:image\/(\w+);base64,/', $image, $type)) {  

	     	   $extra_name    = uniqid().'_'.time().'_'.uniqid().'.'.$type[1];
	           $encoded_base64_image = substr($image, strpos($image, ',') + 1);
	           $resized_image = Image::make($encoded_base64_image);
	           $resized_image->save(storage_path('app/public/sellers/'.$extra_name));
			  $photos[] = [
				        	 	'disk_name' =>'sellers',
				        	 	'file_name' =>$extra_name,
				        	 	'file_size' =>11,
				        	 	'content_type'=>11,
				        	 	'title'=>$client->seller_name,
				        	 	'description'=>'',
				        	 	'field'=>'photos',
				        	 	'sort_order'=>'1'
				        	 ];
		        
			 }			 
			}
			}        	


		$client->photos()->createMany($photos);


	

		return $client;
	}	


	public function personalRegistration(PersonalRegistrationRequest $request)
	{
		
		$region = Region::find($request->region_id);

		$username = Str::random(14);

		$request->merge(['group' => 'cl','max_vehicles'=>2,'username'=>$username]);

		$client = new \App\User($request->all());

		$client->region()->associate($region);

		$client->save();

		$client->roles()->sync([3]);

		return $client;
	}	

	

	public function home(NullRequest $request)
	{
	
		$vehicles = Vehicle::with(['region','cover','photos','seller','model','fuel_type','gear','condtion'])
							->doFilters($request->all())
							->orderBy('order_type_id','asc')
							->orderBy('order','asc')
							->LikeName($request->name)
							->PriceBetween($request->price_min,$request->price_max)
							->YearBetween($request->year_min,$request->year_max)
							->PowerBetween($request->power_min,$request->power_max)
							->EqualNumOfSeat($request->num_of_seat)
							->EqualBodyColor($request->body_color)
							->EqualBodyColor($request->interior_body_color)
							->EqualPreviousWwners($request->previous_owners)
							->paginate(10);

		return $this->respondWithPagination($vehicles, ['data' => $this->vehicleTransformer->transformCollection($vehicles->all())]);

	

	

	}

	public function contactUs(NullRequest $request)
	{

		$contact_us = \App\Setting::where('settings_key','contact_us')->first();

		return ['data'=>$contact_us->settings_value];

	}	
	public function privacyAndPolicy(NullRequest $request)
	{

		$privacy_and_policy = \App\Setting::where('settings_key','privacy_and_policy')->first();

		return ['data'=>$privacy_and_policy->settings_value];

	}	

	
	public function aboutUs(NullRequest $request)
	{
		$about_us = \App\Setting::where('settings_key','about_us')->first();
		
		return ['data'=>$about_us->settings_value];

	}	
	
}

