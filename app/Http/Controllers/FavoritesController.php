<?php

namespace App\Http\Controllers;

use App\Favorite;

use App\Activity;

use Illuminate\Http\Request;

use App\Http\Requests\FavoriteRequest;

use App\Http\Requests\NullRequest;

use App\Http\Requests;

use Auth;

class FavoritesController extends Controller
{
	private $METHOD ;

    public function __construct()
	{


	}


	public function getFavorite(){

		 $favorites = Favorite::latest('id');

		return $favorites; 				
	}

	public function index()
	{
		
	 	$favorites  = $this->getFavorite()->paginate(100);



		 return view('favorites.index',compact('favorites'));
	}

	public function show($id)
	{

		 $favorite = Favorite::findOrFail($id);

		 Auth::user()->recordUserActivity($this->METHOD);

		 return view('favorites.show',compact('favorite'));
	}

	public function create()
	{

		 Auth::user()->recordUserActivity($this->METHOD);

		 return view('favorites.create');
	}

	public function store(FavoriteRequest $request)
	{

		$favorites = Favorite::create($request->all());

		return redirect('favorites');
	}


	public function edit($id)
	{

		$favorite= Favorite::findOrFail($id);

		 Auth::user()->recordUserActivity($this->METHOD,Auth::user());

		 return view('favorites.edit',compact('favorite'));
	}

	public function update($id,FavoriteRequest $request)
	{

		 $favorite= Favorite::findOrFail($id);

		 $favorite->update($request->all());

		return redirect('favorites');
	}

	public function destroy(NullRequest $request)
    {

      

    }
}
