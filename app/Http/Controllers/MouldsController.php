<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use Request;

use App\Mould;

use App\Make;

use App\Activity;

use App\Review;

use App\Http\Requests\MouldRequest;

use App\Http\Requests\NullRequest;

use App\Http\Requests\ReviewRequest;

use Auth;

class MouldsController extends Controller
{

    	public function __construct()
	{


	}

    public function getMouldsByParentId(Request $request){

    	$moulds = Mould::where('parent',$request::input('id'))->get();

    	return $moulds;
    }

    public function getMoulds($request)
	{

		return $moulds = Mould::latest();
			}

	public function getXhrMoulds(NullRequest $request)

	{

			$moulds = Mould::select("moulds.*")
						  ->equalParent($request->get('parent'))
						  ->get();
		 
	

			return $moulds;
	}

	public function index(NullRequest $request)
	{
		
		 $moulds = $this->getMoulds($request)->paginate(50);


		 return view('moulds.index',compact('moulds'));
	}

	public function show($id,NullRequest $request)
	{

		 $mould 				= Mould::findOrFail($id);

		 return view('moulds.show',compact('mould'));
	}

	public function create(NullRequest $request)
	{

	
		 $parents = Mould::whereNull('parent')->pluck('name','id');
		 $parents->prepend(trans('main.no_one'));

		 $makes = Make::pluck('name','id');


		 return view('moulds.create',compact('parents','makes'));
	}

	public function store(MouldRequest $request)
	{
		
		$parent = Mould::find($request->parent);

		$make = Make::find($request->make_id);

		$mould = new \App\Mould($request->all());

		$mould->father()->associate($parent);

		$mould->make()->associate($make);

		$mould->save();


		return redirect('moulds');
	}


	public function edit($id,NullRequest $request)
	{

	
		 $parents = Mould::whereNull('parent')->pluck('name','id');
		 $parents->prepend(trans('main.no_one'));

		 $makes = Make::pluck('name','id');


		$mould= Mould::findOrFail($id);

		 return view('moulds.edit',compact('mould','parents','makes'));
	}

	public function update($id,MouldRequest $request)
	{

		
		$parent = Mould::find($request->parent);
		$make = Make::find($request->make_id);

		$mould = Mould::findOrFail($id);
		$mould->fill($request->all());

		$mould->father()->associate($parent);
		$mould->make()->associate($make);

		$mould->save();

		

		return redirect('moulds');
	}

	public function addReview(ReviewRequest $request,$id)
	{
		$mould = Mould::findOrFail($id);
		$review = 	new Review(request()->all());
		$review->mould()->associate($mould);
		$review->save();

		return redirect('moulds/'.$id)->with('message', trans('main.success_adding'));

	}

	 public function destroy(NullRequest $request)
    {

  

    }
}
