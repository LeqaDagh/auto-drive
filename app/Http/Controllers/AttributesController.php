<?php

namespace App\Http\Controllers;

use App\Attribute;

use App\Type;

use App\Activity;

use Illuminate\Http\Request;

use App\Http\Requests\AttributeRequest;

use App\Http\Requests\NullRequest;

use App\Http\Requests;

use Auth;

use Illuminate\Support\Str;

class AttributesController extends Controller
{
	private $METHOD ;

    public function __construct()
	{

	$this->METHOD = request()->route()->getAction()['controller'];	

	}


	public function getAttribute(){

		 $attributes = Attribute::latest('id');

		return $attributes; 				
	}

	public function index()
	{
		
	 	$attributes  =$this->getAttribute()->paginate(10);

	 	Auth::user()->recordUserActivity($this->METHOD);

		 return view('attributes.index',compact('attributes'));
	}

	public function show($id)
	{

		 $attribute = Attribute::findOrFail($id);

		 Auth::user()->recordUserActivity($this->METHOD);

		 return view('attributes.show',compact('attribute'));
	}

	public function create()
	{


		$types = Type::all()->pluck('name','id');
		$types->prepend(trans('main.choose'),0);

		$parents = Attribute::whereNotNull('parent')->get()->pluck('name','id');
		$parents->prepend(trans('main.choose'),0);


		 Auth::user()->recordUserActivity($this->METHOD);

		 return view('attributes.create',compact('attribute','types','parents'));
	}

	public function store(AttributeRequest $request)
	{
		

		$slug = Str::slug($request->name, '_');
		$request->merge(['slug'=>$slug]);

		 $attribute = Attribute::create($request->all());
		 $attribute->father()->associate($request->parent);
		 $attribute->type()->associate($request->type_id);
		 $attribute->save();

		return redirect('attributes');
	}


	public function edit($id)
	{

		$attribute = Attribute::findOrFail($id);

		$types = Type::all()->pluck('name','id');
		$types->prepend(trans('main.choose'),0);

		$parents = Attribute::whereNotNull('parent')->get()->pluck('name','id');
		$parents->prepend(trans('main.choose'),0);

		 Auth::user()->recordUserActivity($this->METHOD,Auth::user());

		 return view('attributes.edit',compact('attribute','types','parents'));
	}

	public function update($id,AttributeRequest $request)
	{

		 $attribute= Attribute::findOrFail($id);

		 $attribute->update($request->all());

		 $attribute->father()->associate($request->parent);
		 $attribute->type()->associate($request->type_id);
		 $attribute->save();

		return redirect('attributes');
	}

	public function destroy(NullRequest $request)
    {

      

    }

     public function getChilds(NullRequest $request)
    {
    	return Attribute::findOrFail($request->attribute_id)->childs;
    }
}
