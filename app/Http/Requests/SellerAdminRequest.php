<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\UniqueMobile;
use App\Rules\UniqueUserName;
use App\Rules\CheckPhoneNumber;

class SellerAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

    	 if(isset($this->seller_id))
        {
           $this->seller = $this->seller_id;  

       
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        		'name' => 'required|regex:/^[\p{Arabic}a-z 0-9]{1,}$/u',
        		'email' => 'required|string|email|max:255|unique:users,email,'.$this->seller,
        		'mobile'=> [new CheckPhoneNumber(),new UniqueMobile($this->seller)],
             	'region_id'=>'required|integer',
             	'user_status'=>'required',
             	'address'=>'required',
                'seller_name' => 'required',
                'seller_mobile' => ['required',new CheckPhoneNumber()],
                'seller_email' => 'required|email',
                'max_vehicles' => 'required|integer',
                'seller_order'=>'required|integer',
                'seller_address'=>'required',
                'seller_region_id'=>'integer|min:1',
                'seller_lat' => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
                'seller_lon' => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
                'publish_status'=>'required',
                'can_ignor_price'=>'required'
        ];
    }
}
