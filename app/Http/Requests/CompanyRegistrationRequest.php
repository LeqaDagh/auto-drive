<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequest;

use App\Rules\UniqueMobile;
use App\Rules\UniqueUserName;
use App\Rules\CheckPhoneNumber;


class CompanyRegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	   if(isset($this->client_id))
        {
           $this->client = $this->client_id;  
        }
           

  
        return [
                'seller_name'=>'required',
                'seller_mobile'=>['required',new CheckPhoneNumber()],
                'seller_lat' => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
                'seller_lon' => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],

                'name'=>'required',
                'mobile'=> ['required',new CheckPhoneNumber(),new UniqueMobile($this->client)],
                'region_id'=>'required',
                'address'=>'required',
            	'email' => 'required|string|email|max:255|unique:users,email,'.$this->client,
                'password' => 'required|string|min:6|confirmed',

        ];
    }
}

