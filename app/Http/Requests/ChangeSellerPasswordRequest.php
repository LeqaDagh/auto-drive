<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangeSellerPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(isset($this->admin_id))
        {
           $this->admin = $this->admin_id;  
        }
           

          return [
             'password' => 'required|string|min:6|confirmed'
        ];
    }
}
