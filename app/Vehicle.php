<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Enums\EquipmentsTypes;
use Auth;

class Vehicle extends Model
{

  

      protected $fillable = [
                              "sell_type" ,
                              "vehicle_type" ,
                              "body",
                              "extra_title" ,
                              "year_of_product" ,
                              "year_of_work",
                              "price" ,
                              "first_payment" ,
                              "price_type" ,
                              "power" ,
                              "hp" ,
                              "mileage",
                              "gear" ,
                              "fuel" ,
                              "drivetrain_system" ,
                              "drivetrain_type" ,
                              "num_of_seats" ,
                              "vehicle_status",
                              "body_color",
                              "interior_color" ,
                              "customs_exemption",
                              "num_of_doors" ,
                              "license_date",
                              "previous_owners" ,
                              "origin",
                              "num_of_keys" ,
                              "desc" ,
                              "stared",
                              "is_deleted",
                              "slider_id",
                              "publish_status",
                              "delete_at",
                              "order"
                            ];

    
       //  protected $dates = ['delete_at'];

     


    public function user()
    {
        return $this->belongsTo(User::class);
    } 
    public function seller()
    {
        return $this->belongsTo(User::class,'user_id');
    } 
    public function make()
    {
        return $this->belongsTo(Make::class);
    }  
    public function Mould()
    {
        return $this->belongsTo(Mould::class);
    }
    public function photos()
    {
        return $this->morphMany(Media::class, 'fileable');
    }
 	public function cover()
    {
         return $this->morphOne(Media::class, 'fileable');
    } 
    public function favorites()
    {
        return $this->hasMany(Favorite::class);
    }

    public function equipments()
    {
        return $this->hasMany(Equipment::class);
    }

    

    

    public function scopeDoFilters($query)
    {   



   
        $this->getArrayFromString($query,'body');
        $this->getArrayFromString($query,'mould_id');
        $this->getBetween($query,'year_of_product');
        $this->getBetween($query,'year_of_work');
        $this->getBetween($query,'price');
        $this->getBetween($query,'power');
        $this->getBetween($query,'hp');
        $this->getBetween($query,'mileage');
        $this->getArrayFromString($query,'gear');
        $this->getArrayFromString($query,'fuel');
        $this->getArrayFromString($query,'drivetrain_type');
        $this->getArrayFromString($query,'drivetrain_system');
        $this->getArrayFromString($query,'num_of_seats');
        $this->getArrayFromString($query,'vehicle_status');
        $this->getExact($query,'interior_color');
        $this->getExact($query,'body_color');

        
        $this->getArrayFromString($query,'customs_exemption');
        $this->getArrayFromString($query,'num_of_doors');
        $this->getArrayFromString($query,'previous_owners');
        $this->getArrayFromString($query,'origin');
        $this->getArrayFromString($query,'num_of_keys');
        
        $this->getFromTrueFalse($query,'ext_int_furniture');
        $this->getFromTrueFalse($query,'ext_int_seats');
        $this->getFromTrueFalse($query,'ext_int_steering');
        $this->getFromTrueFalse($query,'ext_int_screens');
        $this->getFromTrueFalse($query,'ext_int_sunroof');
        $this->getFromTrueFalse($query,'ext_int_glass');
        $this->getFromTrueFalse($query,'ext_int_other');
        $this->getFromTrueFalse($query,'ext_ext_light');
        $this->getFromTrueFalse($query,'ext_ext_mirrors');
        $this->getFromTrueFalse($query,'ext_ext_rims');
        $this->getFromTrueFalse($query,'ext_ext_cameras');
        $this->getFromTrueFalse($query,'ext_ext_sensors');
        $this->getFromTrueFalse($query,'ext_ext_other');
        $this->getFromTrueFalse($query,'ext_gen_other');
       




      
      

                 
    }

    private function getArrayFromString($query,$string)
    {
        if(request()->has($string))
        {
            if(count(request()->$string))
            {
                $query->whereIn($string,request()->$string );  
            }
        }
          
        
    }
     private function getBetween($query,$string)
    {
        $min_string = $string.'_min';   
        $max_string = $string.'_max';   

        if(request()->has($min_string))
        {   
            if(request()->$min_string)
            {   
                $query->where($string,'>=', request()->$min_string); 
            } 
        }
        if(request()->has($max_string))
        {
             if(request()->$max_string)
            {
                $query->where($string,'<=',request()->$max_string); 
            } 
        }
       
    }
    private function getExact($query,$string)
    {
   
        

        if(request()->has($string))
        {
            if(request()->$string)
            {
               $query->where($string,request()->$string); 
            }
              
        }
     
       
    }
    private function getFromTrueFalse($query,$string)
    {

    
        if(request()->has($string))
        {

            if(count(request()->$string)) 
            {   

                $query->whereHas('equipments', function($q) use ($string){

                    $q->where('equipment_type',$string)->whereIn('equipment_value', request()->$string);
                });
            }
        }
      
    }

    


    public function scopeBetweenYearOfProduct($query,$year_of_product_from,$year_of_product_to)
    {
        $year_of_product_from ? $query->where('year_of_product','>=',$year_of_product_from) : "";
        $year_of_product_to   ? $query->where('year_of_product','<=',$year_of_product_to) : "";
        
    }
    public function scopeBetweenYearOfWork($query,$year_of_work_from,$year_of_work_to)
    {
        $year_of_work_from ? $query->where('year_of_work','>=',$year_of_work_from) : "";
        $year_of_work_to   ? $query->where('year_of_work','<=',$year_of_work_to) : "";
        
    }
    public function scopeBetweenPrice($query,$price_from,$price_to)
    {
        $price_from ? $query->where('price','>=',$price_from) : "";
        $price_to   ? $query->where('price','<=',$price_to) : "";
        
    }
    public function scopeBetweenPower($query,$power_from,$power_to)
    {
        $power_from ? $query->where('power','>=',$power_from) : "";
        $power_to   ? $query->where('power','<=',$power_to) : "";
        
    }
    public function scopeBetweenHp($query,$hp_from,$hp_to)
    {
        $hp_from ? $query->where('hp','>=',$hp_from) : "";
        $hp_to   ? $query->where('hp','<=',$hp_to) : "";
        
    }
    public function scopeBetweenMileage($query,$mileage_from,$mileage_to)
    {
        $mileage_from ? $query->where('mileage','>=',$mileage_from) : "";
        $mileage_to   ? $query->where('mileage','<=',$mileage_to) : "";
        
    }
    public function scopeEqualSellers($query,$seller_id)
    {
        $seller_id ? $query->where('user_id',$seller_id) : "";
        
    }
    public function scopeInSellers($query,$seller_id)
    {
        $seller_id ? $query->whereIn('user_id',$seller_id) : "";
        
    }
    public function scopeInMakes($query,$make_id)
    {

        $make_id ? $query->whereIn('make_id',$make_id) : "";
        
    }
    public function scopeInMould($query,$mould_id)
    {
  
        $mould_id ? $query->whereIn('mould_id',$mould_id) : "";
        
    }
    public function scopeInBody($query,$body)
    {

        $body ? $query->whereIn('body',$body) : "";
        
    }
    public function scopeInGear($query,$gear)
    {
        $gear ? $query->whereIn('gear',$gear) : "";
        
    }
    public function scopeInFuel($query,$fuel)
    {
        $fuel ? $query->whereIn('fuel',$fuel) : "";
        
    }
    public function scopeInDrivetrainSystem($query,$drivetrain_system)
    {
        $drivetrain_system ? $query->whereIn('drivetrain_system',$drivetrain_system) : "";
        
    }
    public function scopeInDrivetrainType($query,$drivetrain_type)
    {
        $drivetrain_type ? $query->whereIn('drivetrain_type',$drivetrain_type) : "";
        
    }
    public function scopeInNumOfSeats($query,$num_of_seats)
    {
        $num_of_seats ? $query->whereIn('num_of_seats',$num_of_seats) : "";
        
    }
    public function scopeInVehicleStatus($query,$vehicle_status)
    {
        $vehicle_status ? $query->whereIn('vehicle_status',$vehicle_status) : "";
        
    }
    public function scopeInBodyColor($query,$body_color)
    {
        $body_color ? $query->whereIn('body_color',$body_color) : "";
        
    }
    public function scopeInInteriorColor($query,$interior_color)
    {
        $interior_color ? $query->whereIn('interior_color',$interior_color) : "";
        
    }
    public function scopeInCustomsExemption($query,$customs_exemption)
    {
        $customs_exemption ? $query->whereIn('customs_exemption',$customs_exemption) : "";
        
    }
    public function scopeInNumOfDoors($query,$num_of_doors)
    {
        $num_of_doors ? $query->whereIn('num_of_doors',$num_of_doors) : "";
        
    }
    public function scopeInPreviousOwners($query,$previous_owners)
    {
        $previous_owners ? $query->whereIn('previous_owners',$previous_owners) : "";
        
    }
    public function scopeInOrigin($query,$origin)
    {
        $origin ? $query->whereIn('origin',$origin) : "";
        
    }
    public function scopeInNumOfKeys($query,$num_of_keys)
    {
        $num_of_keys ? $query->whereIn('num_of_keys',$num_of_keys) : "";
        
    }
    public function scopeInIsDeleted($query,$is_deleted)
    {
        $is_deleted ? $query->whereIn('is_deleted',$is_deleted) : "";
        
    }
    public function scopeInPublishStatus($query,$publish_status)
    {
        $publish_status ? $query->whereIn('publish_status',$publish_status) : "";
        
    }
    public function scopeInStared($query,$stared)
    {
        $stared ? $query->whereIn('stared',$stared) : "";
        
    }
    public function scopeEqualSlider($query,$slider)
    {
        if($slider && $slider=="yes")
        {
            $slider ? $query->whereNotNull('slider_id') : "";
        }
        
    }
    public function scopeInEquipments($query,$equipments)
    {
        
        if(is_array($equipments))
        {
         $query->whereHas('equipments', function($q) use ($equipments){

                    $q->whereIn('equipment_value', $equipments);
                });
        }

    }



    



    public function scopeLikeName($query,$name)
    {
    	$name ? $query->where('name','like','%'.$name.'%') : "";
    }
    public function scopePriceBetween($query,$min,$max)
    {
    	
    	($min AND $max) ? $query->whereBetween('price',[$min,$max]) : "";
    }
    public function scopeYearBetween($query,$min,$max)
    {
    	($min AND $max) ? $query->whereBetween('year',[$min,$max]) : "";
    }
    public function scopePowerBetween($query,$min,$max)
    {
    	($min AND $max) ? $query->whereBetween('power',[$min,$max]) : "";
    }
    public function scopeEqualNumOfSeat($query,$num_of_seats)
    {
    	$num_of_seats ? $query->where('num_of_seats',$num_of_seats) : "";
    }
    public function scopeEqualBodyColor($query,$body_color)
    {
    	$body_color ? $query->where('body_color',$body_color) : "";
    }
    public function scopeEqualPreviousWwners($query,$previous_owners)
    {
    	$previous_owners ? $query->where('previous_owners',$previous_owners) : "";
    }

      public function scopeHisOwen($query)
    {
        //$sellers = Auth::user()->sellers->pluck('id')->toArray();
        //$query->whereIn('seller_id',$sellers);
    }
    public function scopeNotDeleted($query)
    {
        $query->where('is_deleted','no');
    }
    public function scopePublished($query)
    {
        $query->where('publish_status','pub');
    }

    public function scopeDoSort($query)
    {
       

        $sort_by = "price";
        $sort_type =  "ASC";    
        if(request()->sort)
        {

            if(request()->sort == "price_asc" )
            {
                $sort_by = "price";
                $sort_type =  "ASC";   
            }
            if(request()->sort == "price_desc" )
            {

                $sort_by = "price";
                $sort_type =  "DESC";   
            }
            if(request()->sort == "year_asc" )
            {
                $sort_by = "year_of_product";
                $sort_type =  "ASC";   
            }
            if(request()->sort == "year_desc" )
            {
                $sort_by = "year_of_product";
                $sort_type =  "DESC";   
            }
        }
       
        $query->orderBy('stared', 'DESC')->orderBy($sort_by,$sort_type);
    }
    

      




}
