<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{

	protected $fillable = [
        'disk_name' ,
			        	 	'file_name' ,
			        	 	'file_size' ,
			        	 	'content_type',
			        	 	'title',
			        	 	'description',
			        	 	'field',
			        	 	'fileable_id',
			        	 	'fileable_type',
			        	 	'sort_order'
    ];
    
    
    public function fileable()
    {
        return $this->morphTo();
    }
}
