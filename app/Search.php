<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Search extends Model
{

    protected $fillable = ['text','uuid'];

      protected $casts = [
        'text' => 'array',
    ];

      public function user()
    {
       return $this->belongsTo(User::class);
    }

}