<?php
namespace App\Enums;

class EquipmentsTypes
{
    const EXTRA    = 'EXTR';
    const INTERIOR = 'INTE';
    const EXTERIOR = 'EXTE';
}