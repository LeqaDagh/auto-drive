<?php
namespace App\Enums;

class UserGroups
{
    const ADMIN = 'ad';
    const SELLER = 'se';
    const CLIENT = 'cl';
    const REGISTER = 'rg';
    const SUPPORT = 'su';
}