<?php
namespace App\Enums;

class UserStatus
{
    const ACTIVE = 'ac';
    const FORBIDDEN = 'fb';
}