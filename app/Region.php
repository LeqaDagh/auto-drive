<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{

	    use RecordsActivity;
    
    	protected $fillable = ['name'];


    public function users(){
       return $this->hasMany(User::class);
    }


    public function region_childs()
    {
        return $this->hasMany('App\Region','id','parent');
    }  
    
    public function region_parent(){

       return $this->belongsTo('App\Region','parent','id');
    }  

   public function scopeLikeName($query,$name)
    {       
            if($name){

                    $query->where('regions.name','like','%'.$name.'%');
                }       
    }



    public function scopeEqualParent($query,$parent)
    {       
            if($parent){
                    $query->where('regions.parent','=',$parent);
                }       
    }

   public function scopeEqualFrom($query,$from)
    {   
            if($from){


                    $query->where('regions.created_at','>=', date('Y-m-d H:i:s',strtotime($from)));
                }       
    }
    public function scopeEqualTo($query,$to)
    {       
            if($to){

                     $query->where('regions.created_at','<=', date('Y-m-d H:i:s',strtotime($to)));
                }       
    }
}
