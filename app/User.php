<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Cache;

class User extends Authenticatable
{
    use Notifiable, HasRoles,RecordsActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','api_token','group','mobile','username','user_status','address',
        'seller_name','seller_mobile','seller_email','max_vehicles','seller_address','seller_order','seller_lat','seller_lon','publish_status','can_ignor_price','can_show_first_payment'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function session(){
       return $this->hasOne(Session::class);
    }

    
      public function isOnline()
      {
          return Cache::has('user-is-online-' . $this->id);
      }

      public function connectStatus(){

        return $this->isOnline() ? ' <span style="color:green;font-weight:bolder;font-size:largest">&#9679;</span> ': '' ;
      }


    public function recordUserActivity($name,$related=""){

      if(!$related){

        $related = $this;

      }

      if(!method_exists($related, 'recordActivity'))
      {

        throw new \Exception("$name Method  Not Found $related");

      }

      return $related->recordActivity($name);
    }

    public function region(){
       return $this->belongsTo(Region::class);
    }
    
    public function favorites()
    {
        return $this->hasMany(Favorite::class);
    }

    public function searches()
    {
        return $this->hasMany(Search::class);
    }

    public function photos()
    {
        return $this->morphMany(Media::class, 'fileable')->oldest();
    }
    public function cover()
    {
         return $this->morphOne(Media::class, 'fileable');
    }
     public function vehicles()
    {
         return $this->hasMany(Vehicle::class);
    }
    public function seller_region(){

       return $this->belongsTo(Region::class,'seller_region_id');
    }  


    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
   
    }

    public function scopeAdmin($query)
    {       
  
      $query->whereGroup('ad');
               
    }

    public function scopeClient($query)
    {       
  
      $query->whereGroup('cl');
               
    }
    public function scopeSeller($query)
    {       
  
      $query->whereGroup('se');
               
    }
    

    public function scopeSupport($query)
    {       
  
      $query->whereGroup('su');
               
    }


    public function scopeHisOwen($query)
    {    

 

    }

    public function scopeCanPublish($query)
    {       
  
      $query->where('publish_status','yes');
               
    }



    public function scopeLikeName($query,$name)
    {       
      if($name){

              $query->where('users.name','like','%'.$name.'%');
          }       
    }

    public function scopeLikeSellerName($query,$seller_name)
    {       
      if($seller_name){

              $query->where('users.seller_name','like','%'.$seller_name.'%');
          }       
    }

    public function scopeLikeSellerMobile($query,$seller_mobile)
    {       
      if($seller_mobile){

              $query->where('users.seller_mobile','like','%'.$seller_mobile.'%');
          }       
    }
       public function scopeLikeSellerEmail($query,$seller_email)
    {       
      if($seller_email){

              $query->where('users.seller_email','like','%'.$seller_email.'%');
          }       
    }

     public function scopeLikeUsername($query,$username)
    {       
      if($username){

              $query->where('users.username','like','%'.$username.'%');
          }       
    }

     public function scopeOrLikeIdentity($query,$identity)
    {       
      if($identity){

              $query->orWhere('users.identity','like','%'.$identity.'%');
          }       
    }

     public function scopeLikeIdentity($query,$identity)
    {       
        if($identity){

                $query->where('users.identity','like','%'.$identity.'%');
            }       
    }

   public function scopeLikeMobile($query,$mobile)
    {       
        if($mobile){

                $query->where('users.mobile','like','%'.$mobile.'%');
            }       
    }

   public function scopeLikeTruckNumber($query,$truck_number)
    {       
        if($truck_number){

                $query->where('users.truck_number','like','%'.$truck_number.'%');
            }       
    }

   public function scopeLikeEmail($query,$email)
    {       
        if($email){

                $query->where('users.email','like','%'.$email.'%');
            }       
    }
    public function scopeEqualUser($query,$user_id)
    {       
        if($user_id){

                $query->where('users.id','=',$user_id);
            }       
    }

    public function scopeEqualGroup($query,$group)
    {       
        if($group){

                $query->where('users.group_id','=',$group);
            }       
    }

        public function scopeEqualUserStatus($query,$status)
    {       
        if($status){

                $query->where('users.user_status','=',$status);
            }       
    }

    public function scopeEqualRegion($query,$region_id)
    {       
        if($region_id){
          
                $query->where('users.region_id','=',$region_id);
            }       
    }

  

    

    public function scopeFrom($query,$from)
    {       
            if($from){

                    $query->where('users.created_at','>=', date('Y-m-d H:i:s',strtotime($from)));
                }       
    }
    public function scopeTo($query,$to)
    {       
            if($to){

                     $query->where('users.created_at','<=', date('Y-m-d H:i:s',strtotime($to)));
                }       
    }




}
