<?php

namespace App\Transformers;

/**
 * Class LessonTransformer
 *
 * This class is used to transform lessons, in JSON format,
 * by explicitly stating which lesson properties to serve up.
 *
 * @package Acme\Transformers
 */
use Carbon\Carbon;


class SearchTransformer extends Transformer{


    public function __construct()
    {
      
    }

    /**
     * This function transforms a single lesson -
     * from JSON format with specified fields.
     *
     * @param $device A Device
     * @return array Returns an individual lesson,
     * according to specified fields.
     */
    public function  transform($search)
    {
                                      
        return [
                'id'          	            => @$search['id'],
                'uuid'                      => @$search['uuid'],
                'text'    	  	            => @$search['text'],
                'saved_moulds'              => 'FORD FIEST , BMW X6',
                'saved_bodies'              => 'SEDAN , HATCHBACK',
                'saved_years_of_products'   => '1990-2000 , 2000-2003'

                
        ];
    }
}


