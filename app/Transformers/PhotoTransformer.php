<?php

namespace App\Transformers;

/**
 * Class LessonTransformer
 *
 * This class is used to transform lessons, in JSON format,
 * by explicitly stating which lesson properties to serve up.
 *
 * @package Acme\Transformers
 */
use Carbon\Carbon;

class PhotoTransformer extends Transformer{



    /**
     * This function transforms a single lesson -
     * from JSON format with specified fields.
     *
     * @param $device A Device
     * @return array Returns an individual lesson,
     * according to specified fields.
     */
    public function  transform($photo)
    {
                                 
        return [
                'path'          => url('public/storage/'.@$photo['disk_name'],@$photo['file_name']),
                'thumb_path'    =>  url('public/storage/'.@$photo['disk_name'],@$photo['file_name']),
                
        ];
    }
}