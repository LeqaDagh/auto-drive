<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

use App\User;

class CheckPhoneNumber implements Rule
{

    /**
     * Create a new rule instance.
     *
     * @return void
     */
 

    public function __construct()
    {
    		
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

    		$first_two_char = substr($value, 0, 2);

    		$length = strlen($value);

          
            if( ($first_two_char=="05" AND $length=="10") OR ($first_two_char!="05" AND $length=="9")  )
            {
                return true;
            }

            return  false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return  'صيغية الهاتف غير صحيحه  ';
    }
}
