<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

use Auth;

class CanPublish implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

        /**
     * Create a new rule instance.
     *
     * @return void
     */
      
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
            $publish_status  = Auth::user()->publish_status;


            if($publish_status=="yes")
            {
                return true;
            }

            return  false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'خاصية النشر معطلة لدى هذا الحساب';
    }
}
