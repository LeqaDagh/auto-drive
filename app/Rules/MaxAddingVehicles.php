<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

use Auth;

class MaxAddingVehicles implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

        /**
     * Create a new rule instance.
     *
     * @return void
     */
      
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
            $user  = Auth::user();

            $vehicles = $user->vehicles()->count();

            if($vehicles<$user->max_vehicles)
            {
                return true;
            }

            return  false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'لقد تجاوزت الحد الاعلى المسموح به لاضافة مركبات';
    }
}
