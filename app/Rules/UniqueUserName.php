<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

use App\User;

class UniqueUserName implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

        /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $user_id;

    public function __construct($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
            $user  = User::where('username',$value)
                         ->where('id','!=',$this->user_id)
                         ->count();
            if(!$user)
            {
                return true;
            }

            return  false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'users is exit';
    }
}
