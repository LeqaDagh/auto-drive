<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMouldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moulds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('make_id')->unsigned()->nullable();
            $table->string('name');
            $table->string('slug');
            $table->string('logo')->nullable();
            $table->integer('order');
            $table->string('review')->nullable();
            $table->integer('parent')->unsigned()->nullable();
            $table->string('visible',3)->default('no');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moulds');
    }
}
