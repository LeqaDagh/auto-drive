<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('api_token', 80)->unique()->nullable()->default(null);
            $table->string('group',2)->default('cl');
            $table->string('mobile')->unique();
            $table->string('username')->unique()->nullable();
            $table->integer('region_id')->unsigned()->nullable();
            $table->string('user_status',3)->default('yes');
            $table->string('address')->nullable();

            $table->string('seller_name')->nullable();
            $table->string('seller_mobile')->nullable();
            $table->string('seller_email')->nullable();
            $table->integer('max_vehicles')->default(0);
            $table->string('seller_address')->nullable();
            $table->integer('seller_region_id')->unsigned()->nullable();
            $table->integer('seller_order')->default(0);
            $table->string('seller_lat')->nullable();
            $table->string('seller_lon')->nullable();
            $table->string('publish_status',3)->default('no');
            $table->string('can_ignor_price',3)->default('no');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
          