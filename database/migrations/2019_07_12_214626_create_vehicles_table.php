<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned();      
            $table->integer('make_id')->unsigned();
            $table->integer('mould_id')->unsigned();
            $table->string('body',4)->nullable();
            $table->string('extra_title')->nullable();
            $table->integer('year_of_product')->nullable();
            $table->integer('year_of_work')->nullable();
            $table->integer('price')->nullable();
            $table->integer('first_payment')->nullable();
            $table->string('price_type')->nullable();
            $table->integer('power')->nullable();
            $table->integer('hp')->nullable();
            $table->integer('mileage')->nullable();
            $table->string('gear',4)->nullable();
            $table->string('fuel',8)->nullable();
            $table->string('drivetrain_system',4)->nullable();
            $table->string('drivetrain_type',4)->nullable();
            $table->string('num_of_seats',4)->nullable();
            $table->string('vehicle_status',4)->nullable();
            $table->string('body_color',7)->nullable();
            $table->string('interior_color',7)->nullable();
            
            $table->string('customs_exemption',4)->nullable();
            $table->string('num_of_doors',4)->nullable();
            $table->date('license_date')->nullable(); 
            $table->string('previous_owners',4)->nullable();
            $table->string('origin',4)->nullable();
            $table->string('num_of_keys',4)->nullable();
            $table->text('desc')->nullable();

            $table->string('order_type',4)->nullable();
            $table->integer('order')->default(1000);
            $table->string('is_deleted',3)->default('no');

            $table->string('publish_status',4)->default('ini'); 
            $table->string('stared',3)->default('no');
            $table->integer('slider_id')->unsigned()->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
