<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
  
   public function getRandEquType()
    {
    	$yes_no_array = ['EXTR','INTE','EXTE'];

    	$rand_index = array_rand($yes_no_array);
 
		return $yes_no_array[$rand_index];

    }
    public function getRandYesNo()
    {
    	$yes_no_array = ['yes','no'];

    	$rand_index = array_rand($yes_no_array);
 
		return $yes_no_array[$rand_index];

    }

    public function getRandDate()
    {
    	//Generate a timestamp using mt_rand.
		$timestamp = mt_rand(1, time());
		 
		//Format that timestamp into a readable date string.
		$randomDate = date("Y-m-d", $timestamp);
		 
		//Print it out.
		return $randomDate;
    }

    public function run()
    {

    	
        
				  					DB::table('regions')->insert([
											['name' => 'الخليل'],
											['name' => 'بيت لحم'],
											['name' => 'رام الله']]
									);

				  				
									//==========================
										for ($i=0; $i <3 ; $i++) { 
										$id = DB::table('sellers')->insertGetId(
																					array(
																					'name' => 'معرض '.$i,
																					'slug' => 'show_'.$i,
																					'region_id' => rand(1,3),
																					'user_id' => 1,
																					'publish_status' => $this->getRandYesNo(),
																					'order' => rand(1,3),
																					'lat'=>'33.23453456',
																					'lon'=>'35.34567467'

																					)
																			);

	for ($j=0; $j <rand(2,3) ; $j++) 
							 { 

	 	$json = file_get_contents('https://loremflickr.com/json/g/320/240/company/all');
								$obj = json_decode($json);
								$obj->file;

								$url = $obj->file;
								$contents = file_get_contents($url);
								$name = substr($url, strrpos($url, '/') + 1);
								Storage::put('sellers/'.$name, $contents);



							 	DB::table('media')->insert(
											array(
												"disk_name"=> "sellers",
												"file_name"=> $name,
												"file_size"=> rand(128,1024),
												"content_type"=> "",
												"title"=> "",
												"description"=> "",
												"field"=> "photos",
												"fileable_id"=> $id,
												"fileable_type"=> "App\Seller",
												"sort_order"=> rand(1,5)

											)
									);
							 }


										}


						

							
							//===================================================

		$id = DB::table('types')->insertGetId(
							array(
							'name' => 'النوع',
							'slug' => 'sell_type',
							'logo'=>rand(1,6).'.png',
							'mobile_logo'=>rand(1,6).'.png',
							'order'=>rand(1,10),
							'search_type'=>'single',
							'search_field'=>'sell_type_id',
							'visible'=>$this->getRandYesNo(),
							)
					);//8


							DB::table('attributes')->insert(
									array(
									'name' => 'بيع',
									'slug' => 'sell',
									'logo'=>rand(1,6).'.png',
									'mobile_logo'=>rand(1,6).'.png',
									'parent'=>null,
									'order'=>3,
									'type_id' => $id,
									'visible'=>$this->getRandYesNo(),
									)
							);//a23

							DB::table('attributes')->insert(
									array(
									'name' => 'تأجير',
									'slug' => 'rent',
									'logo'=>rand(1,6).'.png',
									'mobile_logo'=>rand(1,6).'.png',
									'parent'=>null,
									'order'=>2,
									'type_id' => $id,
									'visible'=>$this->getRandYesNo(),
									)
							);//24
							//==================

								$id = DB::table('types')->insertGetId(
															array(
															'name' => 'نوع المركبة',
															'slug' => 'vehicle_type',
															'logo'=>rand(1,6).'.png',
															'order'=>rand(1,10),
															'search_type'=>'single',
															'search_field'=>'vehicle_type_id',
															'visible'=>$this->getRandYesNo(),
															)
								);//1


							DB::table('attributes')->insert(
									array(
									'name' => 'سيارة',
									'slug' => 'car',
									'logo'=>rand(1,6).'.png',
									'parent'=>null,
									'order'=>3,
									'type_id' => $id,
									'visible'=>$this->getRandYesNo(),
									)
							);//a1

							DB::table('attributes')->insert(
									array(
									'name' => 'دراجة',
									'slug' => 'motorcycle',
									'logo'=>rand(1,6).'.png',
									'parent'=>null,
									'order'=>2,
									'type_id' => $id,
									'visible'=>$this->getRandYesNo(),
									)
							);//a2

							DB::table('attributes')->insert(
									array(
									'name' => 'حافلة',
									'slug' => 'van',
									'logo'=>rand(1,6).'.png',
									'parent'=>null,
									'order'=>1,
									'type_id' => $id,
									'visible'=>$this->getRandYesNo(),
									)
							);//a3

					//===================================================


		 	


					


						$make = DB::table('types')->insertGetId(
												array(
												'name' => 'الشكة',
												'slug' => 'vehicle_make',
												'logo'=>rand(1,6).'.png',
												'order'=>rand(1,10),
												'search_type'=>'single',
												'search_field'=>'make_id',
												'visible'=>$this->getRandYesNo(),
												)
										);//2


						$model = DB::table('types')->insertGetId(
							array(
							'name' => 'الموديل',
							'slug' => 'vehicle_model',
							'logo'=>rand(1,6).'.png',
							'order'=>rand(1,10),
							'search_type'=>'single',
							'search_field'=>'model_id',
							'visible'=>$this->getRandYesNo(),
							)
					);//2

							$ford = DB::table('attributes')->insertGetId(
									array(
									'name' => 'فورد',
									'slug' => 'ford',
									'type_id' => $make,
									'visible'=>$this->getRandYesNo(),
									)
							);//a4

									DB::table('attributes')->insert(
										array(
										'name' => 'فورد فيستا',
										'slug' => 'fordfesta',
										'type_id' => $model,
										'logo'=>rand(1,6).'.png',
										'parent' => $ford,
										'order'=>1,
										'visible'=>$this->getRandYesNo(),
										)
									);//a5

									DB::table('attributes')->insert(
										array(
										'name' => 'فورد فوكس',
										'slug' => 'fordfoucous',
										'type_id' => $model,
										'logo'=>rand(1,6).'.png',
										'parent' => $ford,
										'order'=>1,
										'visible'=>$this->getRandYesNo(),
										)
									);//a6


							$bmw = DB::table('attributes')->insertGetId(
									array(
									'name' => 'BMW',
									'slug' => 'bmw',
									'type_id' => $make,
									'visible'=>$this->getRandYesNo(),
									)
							);//a7
									$id2 = DB::table('attributes')->insert(
										array(
										'name' => 'X6',
										'slug' => 'x6',
										'type_id' => $model,
										'logo'=>rand(1,6).'.png',
										'parent' => $bmw,
										'order'=>1,
										'visible'=>$this->getRandYesNo(),
										)
									);//a8

									DB::table('attributes')->insert(
										array(
										'name' => 'X7',
										'slug' => 'x7',
										'type_id' => $model,
										'logo'=>rand(1,6).'.png',
										'parent' => $bmw,
										'order'=>1,
										'visible'=>$this->getRandYesNo(),
										)
									);//a9

					//===================================================
				
  $id = DB::table('types')->insertGetId(
							array(
							'name' => 'نوع ناقل الحركة',
							'slug' => 'gear_type',
							'logo'=>rand(1,6).'.png',
							'order'=>rand(1,10),
							'search_type'=>'single',
							'search_field'=>'gear_id',
							'visible'=>$this->getRandYesNo(),
							)
					);//4

				  				DB::table('attributes')->insert(
											array(
											'name' => 'يدوي',
											'slug' => 'Manual',
											'type_id' => $id,
											'visible'=>$this->getRandYesNo(),
											)
									);//a13

				  				DB::table('attributes')->insert(
											array(
											'name' => 'اوتوماتيكي',
											'slug' => 'auto',
											'type_id' => $id,
											'visible'=>$this->getRandYesNo(),
											)
									);//a14
				  //===================================================

				 $id =  DB::table('types')->insertGetId(
							array(
							'name' => 'حالة مركبة',
							'slug' => 'condtion_type',
							'logo'=>rand(1,6).'.png',
							'order'=>rand(1,10),
							'search_type'=>'single',
							'search_field'=>'condtion_id',
							'visible'=>$this->getRandYesNo(),
							)
					);//5

				  				DB::table('attributes')->insert(
											array(
											'name' => 'حالة 1',
											'slug' => 'con1',
											'type_id' => $id,
											'visible'=>$this->getRandYesNo(),
											)
									);//a15

				  					DB::table('attributes')->insert(
											array(
											'name' => 'حالة 2',
											'slug' => 'con2',
											'type_id' => $id,
											'visible'=>$this->getRandYesNo(),
											)
									);//a16
				  //===================================================
					$id = DB::table('types')->insertGetId(
							array(
							'name' => 'نوع الهيكل',
							'slug' => 'body_type',
							'logo'=>rand(1,6).'.png',
							'order'=>rand(1,10),
							'search_type'=>'single',
							'search_field'=>'body_type_id',
							'visible'=>$this->getRandYesNo(),
							)
					);//3

									DB::table('attributes')->insert(
									array(
									'name' => 'سيدان',
									'slug' => 'sedan',
									'logo'=>rand(1,6).'.png',
									'parent'=>null,
									'order'=>2,
									'type_id' => $id,
									'visible'=>$this->getRandYesNo(),

									)
									);//a11

									DB::table('attributes')->insert(
											array(
											'name' => 'كومباكت',
											'slug' => 'compact',
											'logo'=>rand(1,6).'.png',
											'parent'=>null,
											'order'=>2,
											'type_id' => $id,
											'visible'=>$this->getRandYesNo(),
											)
									);//a12
					//===================================================
					$id= DB::table('types')->insertGetId(
							array(
							'name' => 'نوع الوقود',
							'slug' => 'fuel_type',
							'logo'=>rand(1,6).'.png',
							'order'=>rand(1,10),
							'search_type'=>'single',
							'search_field'=>'fuel_type_id'
							)
					);//3

									DB::table('attributes')->insert(
									array(
									'name' => 'بنزين',
									'slug' => 'banzin',
									'logo'=>rand(1,6).'.png',
									'parent'=>null,
									'order'=>2,
									'type_id' => $id,

									)
									);//a11

									DB::table('attributes')->insert(
											array(
											'name' => 'ديزل',
											'slug' => 'desil',
											'logo'=>rand(1,6).'.png',
											'parent'=>null,
											'order'=>2,
											'type_id' => $id
											)
									);//a12
					//===================================================

				

				 $id =  DB::table('types')->insertGetId(
							array(
							'name' => 'حالة المركبة',
							'slug' => 'vehicle_status',
							'logo'=>rand(1,6).'.png',
							'order'=>rand(1,10),
							'search_type'=>'single',
							'search_field'=>'vehicle_status_id'
							)
					);//6

				  		DB::table('attributes')->insert(
											array(
											'name' => 'مستعمل',
											'slug' => 'used',
											'type_id' => $id
											)
									);//a17


				  		DB::table('attributes')->insert(
											array(
											'name' => 'جديد',
											'slug' => 'new',
											'type_id' => $id
											)
									);//a18
	  //===================================================

				 $id =  DB::table('types')->insertGetId(
							array(
							'name' => 'نوع الترتيب',
							'slug' => 'order_type',
							'logo'=>rand(1,6).'.png',
							'order'=>rand(1,10),
							'search_type'=>'single',
							'search_field'=>'order_type_id'
							)
					);//6

				  		DB::table('attributes')->insert(
											array(
											'name' => 'الخطة العادية',
											'slug' => 'normal_order',
											'type_id' => $id
											)
									);//a17


				  		DB::table('attributes')->insert(
											array(
											'name' => 'الخطة الذهبية',
											'slug' => 'gold_order',
											'type_id' => $id
											)
									);//a18
				  //===================================================
				  			  //===================================================

				 $id =  DB::table('types')->insertGetId(
							array(
							'name' => 'طريقة الدفع',
							'slug' => 'payment_method',
							'logo'=>rand(1,6).'.png',
							'order'=>rand(1,10),
							'search_type'=>'single',
							'search_field'=>'payment_method_id'
							)
					);//6

				  		DB::table('attributes')->insert(
											array(
											'name' => 'نقدي',
											'slug' => 'cash_pay',
											'type_id' => $id
											)
									);//a17


				  		DB::table('attributes')->insert(
											array(
											'name' => 'تقسيط',
											'slug' => 'installment_pay',
											'type_id' => $id
											)
									);//a18
				  //===================================================
				  				  			  //===================================================

				 $id =  DB::table('types')->insertGetId(
							array(
							'name' => 'طريقة الدفع',
							'slug' => 'origin',
							'logo'=>rand(1,6).'.png',
							'order'=>rand(1,10),
							'search_type'=>'single',
							'search_field'=>'origin_id'
							)
					);//6

				  		DB::table('attributes')->insert(
											array(
											'name' => 'عمومي',
											'slug' => 'public_origin',
											'type_id' => $id
											)
									);//a17


				  		DB::table('attributes')->insert(
											array(
											'name' => 'خصوصي',
											'slug' => 'spicaal_origin',
											'type_id' => $id
											)
									);//a18
				  //===================================================
				 $id =  DB::table('types')->insertGetId(
							array(
							'name' => 'اضافت عامة',
							'slug' => 'extra_equipments',
							'logo'=>rand(1,6).'.png',
							'order'=>rand(1,10),
							'search_type'=>'multi',
							'search_field'=>'extra_equipments'

							)
					);//7

				  		DB::table('attributes')->insert(
											array(
											'name' => 'ABS',
											'slug' => 'ABS',
											'type_id' =>  $id 
											)
									);//a19

				  		DB::table('attributes')->insert(
											array(
											'name' => 'ركن تلقائي',
											'slug' => 'autopark',
											'type_id' =>  $id 
											)
									);//a20

				  			DB::table('attributes')->insert(
											array(
											'name' => 'كاميرا',
											'slug' => 'camera',
											'type_id' =>  $id 
											)
									);//a21

				  				DB::table('attributes')->insert(
											array(
											'name' => 'حساس',
											'slug' => 'ultrasonic',
											'type_id' =>  $id 
											)
									);//a22

				 //===================================================

				  				  //===================================================
				 $id =  DB::table('types')->insertGetId(
							array(
							'name' => 'اضافت داخلية',
							'slug' => 'interior_equipments',
							'logo'=>rand(1,6).'.png',
							'order'=>rand(1,10),
							'search_type'=>'multi',
							'search_field'=>'interior_equipments'

							)
					);//7

				  		DB::table('attributes')->insert(
											array(
											'name' => 'intrex',
											'slug' => 'intrex',
											'type_id' =>  $id 
											)
									);//a19

				  
				 //===================================================
 					  //===================================================
				 $id =  DB::table('types')->insertGetId(
							array(
							'name' => 'اضافت خارجية',
							'slug' => 'exterior_equipments',
							'logo'=>rand(1,6).'.png',
							'order'=>rand(1,10),
							'search_type'=>'multi',
							'search_field'=>'exterior_equipments'

							)
					);//7

				  		DB::table('attributes')->insert(
											array(
											'name' => 'exteinr',
											'slug' => 'exteinr',
											'type_id' =>  $id 
											)
									);//a19

				  
				 //===================================================
			
				  				function getSomething($field)
				  				{
				  					$id = 
				  					DB::table('attributes')
						  					->select('id')
						  					->where('type_id',DB::table('types')->select('id')->where('search_field',$field)
						  					->first()->id)
						  					->inRandomOrder()
						  					->first()
						  					->id;
				  					return $id;
				  				}
					

				for ($i=0; $i < 10 ; $i++) 
				{ 

						$id= DB::table('vehicles')->insertGetId(
											array(
													'seller_id' => rand(1,3),
													'region_id' => rand(1,3),
													'vehicle_type_id'=> getSomething('vehicle_type_id'),
													'sell_type_id'=> getSomething('sell_type_id'),
													'make_id' => getSomething('make_id'),
													'model_id' => getSomething('model_id'),
													'body_type_id' =>  getSomething('body_type_id'),
													'fuel_type_id' =>  getSomething('fuel_type_id'),
													'gear_id' => getSomething('gear_id'),
													'condtion_id' => getSomething('condtion_id'),
													'vehicle_status_id' => getSomething('vehicle_status_id'),
													'payment_method_id' => getSomething('payment_method_id'),
													'origin_id' => getSomething('origin_id'),
													'order_type_id' => getSomething('order_type_id'),
													'order' => rand(1,300),
													'name' => 'Vehicle '.$i,
													'price' => rand(22000,155000),
													'year' => rand(2000,2019),
													'hp' => rand(1200,1600),
													'power' => rand(1200,1600),
													'mileage' => rand(1200,1600),
													'num_of_doors' => rand(4,6),
													'num_of_seats' => rand(4,7),
													'body_color' => $this->random_color(),
													'interior_color' => $this->random_color(),
													'previous_owners' => rand(1,3),
													'license_date' => $this->getRandDate(),
													'customs_exemption' => $this->getRandYesNo(),
													'published'=>$this->getRandYesNo()

											)
									);//7

						  $equipments   =  DB::table('attributes')->where('type_id',13)->get( );

							 foreach ( $equipments as $key => $equipment) 
							 {

							 	$r = (bool)rand(0,1);

							 	if($r)
							 	{

									DB::table('attribute_vehicle')->insert(
											array(
											'vehicle_id' => $id,
											'attribute_id' => $equipment->id,
											'type' => 'EXTR'
											)
									);
							 	}

							 }

							   $equipments   =  DB::table('attributes')->where('type_id',14)->get( );

							 foreach ( $equipments as $key => $equipment) 
							 {

							 	$r = (bool)rand(0,1);

							 	if($r)
							 	{

									DB::table('attribute_vehicle')->insert(
											array(
											'vehicle_id' => $id,
											'attribute_id' => $equipment->id,
											'type' => 'INTE'
											)
									);
							 	}

							 }	

							   $equipments   =  DB::table('attributes')->where('type_id',15)->get( );

							 foreach ( $equipments as $key => $equipment) 
							 {

							 	$r = (bool)rand(0,1);

							 	if($r)
							 	{

									DB::table('attribute_vehicle')->insert(
											array(
											'vehicle_id' => $id,
											'attribute_id' => $equipment->id,
											'type' => 'EXTE'
											)
									);
							 	}

							 }


							 for ($j=0; $j <rand(2,3) ; $j++) 
							 { 

							 	

							 	$json = file_get_contents('https://loremflickr.com/json/g/320/240/cars/all');
								$obj = json_decode($json);
								$obj->file;

								$url = $obj->file;
								$contents = file_get_contents($url);
								$name = substr($url, strrpos($url, '/') + 1);
								Storage::put('vehicles/'.$name, $contents);

							 	DB::table('media')->insert(
											array(
												"disk_name"=> "vehicles",
												"file_name"=> $name,
												"file_size"=> rand(128,1024),
												"content_type"=> "",
												"title"=> "",
												"description"=> "",
												"field"=> "photos",
												"fileable_id"=> $id,
												"fileable_type"=> "App\Vehicle",
												"sort_order"=> rand(1,5)

											)
									);
							 }


						

				}
	 		


	

    }


function random_color_part() {
    return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
}

function random_color() {
    return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
}



}
