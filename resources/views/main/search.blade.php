@extends('layouts.main')

@section('content')

<div class="container">
  <div class="row">

    <div class="col-lg-4">
      
  <div style="background: white;" >
    <div >
    






{!! Form::open(['url' => 'search','method'=>'get','class'=>'form-horizontal search_form','style'=>'padding:10px;']) !!}


<div >
                <div class="" style="margin:5px;">
                	<div>
                		      {!! Form::select('sell_type_id[]',$sell_types,request()->sell_type_id,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>trans('main.sell_type'),' multiple data-selected-text-format'=>'count > 3']) !!}

                	</div>
              
                    </div>
             <div class="" style="margin:5px;">        
        
            <div >
             {!! Form::select('vehicle_type_id[]',$vehicle_types,request()->vehicle_type_id,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'النوع',' multiple data-selected-text-format'=>'count > 3']) !!}
       
            </div>
          </div>
            <div class="" style="margin:5px;">
            <div >
              {!! Form::select('region_id[]',$regions,request()->region_id,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'المنطقة',' multiple data-selected-text-format'=>'count > 3']) !!}
            </div>
            <div >
             {!! Form::select('make_id[]',$makes,request()->make_id,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'الشركة',' multiple data-selected-text-format'=>'count > 3']) !!}
            </div>
          </div>
            <div class="" style="margin:5px;">
            <div >
              {!! Form::select('model_id[]',$models,request()->model_id,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'الموديل',' multiple data-selected-text-format'=>'count > 3']) !!}
            </div>
            <div >
               {!! Form::select('gear_id[]',$gears,request()->gear_id,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'ناقل الحركىة',' multiple data-selected-text-format'=>'count > 3']) !!}
            </div>
          </div>
             <div class="" style="margin:5px;">
            <div >
               {!! Form::select('condtion_id[]',$condtions,request()->condtion_id,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'حالة المركبة',' multiple data-selected-text-format'=>'count > 3']) !!}
            </div>
            <div >
               {!! Form::select('body_type_id[]',$body_types,request()->body_type_id,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'حالة جسم المركبة',' multiple data-selected-text-format'=>'count > 3']) !!}
            </div>
          </div>
            <div class="" style="margin:5px;">
            <div >
                 {!! Form::select('fuel_type_id[]',$fuel_types,request()->fuel_type_id,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'نوع الوقود',' multiple data-selected-text-format'=>'count > 3']) !!}
            </div>
            <div >
              {!! Form::select('vehicle_status_id[]',$vehicle_status,request()->vehicle_status_id,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'حالة المركبة',' multiple data-selected-text-format'=>'count > 3']) !!}
            </div>
          </div>

     


          <div class="form-row" style="margin:5px;">
            <div  class="col">
                {!! Form::number('price_min',request()->price_min,['class'=>'text-center  form-control ','placeholder'=>'اقل سعر']) !!}
            </div>
            <div  class="col">
             {!! Form::number('price_max',request()->price_max,['class'=>'text-center  form-control ','placeholder'=>'اعلى سعر']) !!}
            </div>
          </div>

           <div class="form-row" style="margin:5px;">
            <div class="col">
                {!! Form::number('year_min',request()->year_min,['class'=>'text-center  form-control ','placeholder'=>'من موديل']) !!}
            </div>
            <div class="col">
             {!! Form::number('year_max',request()->year_max,['class'=>'text-center  form-control ','placeholder'=>'الى موديل ']) !!}
            </div>
          </div>

           <div class="form-row" style="margin:5px;">
            <div class="col">
                {!! Form::number('power_min',request()->power_min,['class'=>'text-center  form-control ','placeholder'=>'اقل قوة محرك']) !!}
            </div>
            <div class="col">
             {!! Form::number('power_max',request()->power_max,['class'=>'text-center  form-control ','placeholder'=>'اعل قوة محرك']) !!}
            </div>
          </div>

          <div class="" style="margin:5px;">
            <div >
                {!! Form::number('num_of_seats',request()->num_of_seats,['class'=>'text-center  form-control ','placeholder'=>'عدد المقاعد']) !!}
            </div>
            <div >
             {!! Form::number('body_color',request()->body_color,['class'=>'text-center  form-control ','placeholder'=>'لون المركبة']) !!}
            </div>
          </div>
          <div class="" style="margin:5px;">
            <div >
                {!! Form::number('previous_owners',request()->previous_owners,['class'=>'text-center  form-control ','placeholder'=>'ملاك سابقون']) !!}
            </div>
           
          </div>


      

  <div class="" style="margin:5px;">

	<div>
                		      {!! Form::select('extra_equipments_id[]',$extra_equipments,request()->equipment_id,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>trans('equipments'),' multiple data-selected-text-format'=>'count > 3']) !!}

                	</div>

  </div>

    <div class="" style="margin:5px;">

  <div>
                          {!! Form::select('interior_equipments_id[]',$interior_equipments,request()->equipment_id,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>trans('equipments'),' multiple data-selected-text-format'=>'count > 3']) !!}

                  </div>

  </div>

    <div class="" style="margin:5px;">

  <div>
                          {!! Form::select('exterior_equipments_id[]',$exterior_equipments,request()->equipment_id,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>trans('equipments'),' multiple data-selected-text-format'=>'count > 3']) !!}

                  </div>

  </div>


</div>


        <div class="text-center"><button type="submit" class="btn btn-primary"><span class="total_results"></span>بحث</button></div>



      

{!! Form::close() !!}

    </div>

  </div>

    </div>

        <div class="col-lg-8" >
      <div class="row">
      @foreach($vehicles as $vehicle)

      <div class="col-lg-12" >
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative" style="background: white;">
      	  <div class="col-lg-4  d-lg-block">
          <img style="width: 100%;" src="{{url('public/storage/'.@$vehicle->cover->disk_name,@$vehicle->cover->file_name)}}">
        </div>
        <div class="col p-4 d-flex flex-column position-static">
          <h3 class="mb-0">{{$vehicle->make->name}},{{$vehicle->model->name}},<b>{{@$vehicle->year}}</b></h3>
          <div class="mb-1 text-muted">{{@$vehicle->seller->name}},{{@$vehicle->region->name}}</div>
          <p class="card-text mb-auto">
          	 <div class="form-row">
          		<div class="col">{{@$vehicle->price}}</div>
          		<div class="col">{{@$vehicle->power}}</div>
          		<div class="col">{{@$vehicle->num_of_seats}}</div>
          		<div class="col">{{@$vehicle->body_color}}</div>
          	</div>
          	<div class="form-row">
          		<div class="col">{{@$vehicle->sell_type->name}}</div>
          		<div class="col">{{@$vehicle->vehicle_type->name}}</div>
          		<div class="col">{{@$vehicle->gear->name}}</div>
          		<div class="col">{{@$vehicle->condtion->name}}</div>
          		<div class="col">{{@$vehicle->fuel_type->name}}</div>
          	</div>

          </p>
         
          <div>
          	@foreach($vehicle->photos as $photo)
          	<img style="height: 30px;margin: 2px;" src="{{url('public/storage/'.@$photo->disk_name,@$photo->file_name)}}">
          	@endforeach
          </div>
           <a href="{{url('show',$vehicle->id)}}" class="stretched-link">عرض</a>
        </div>
      
      </div>
    </div>
      @endforeach
    </div>
    </div>
  </div>
</div>
@endsection