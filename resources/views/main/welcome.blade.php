@extends('layouts.main')

@section('content')

  <div class="jumbotron p-4 p-md-5 text-white rounded bg-dark" style="background: url('https://png.pngtree.com/thumb_back/fw800/back_pic/00/02/73/22561a5edf330fc.jpg')no-repeat; background-size: cover;height: 450px;"  >
    <div class="col-md-6 px-0" style="background: white;color: #666;">
    






{!! Form::open(['url' => 'search','method'=>'get','class'=>'form-horizontal search_form','style'=>'padding:10px;']) !!}
<nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">اساسي</a>
    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">اضافي</a>
    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">اضافات</a>
  </div>
</nav>

<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="form-row" style="margin:5px;">
              
        
            <div class="col">
             {!! Form::select('vehicle_type_id[]',$vehicle_types,null,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'النوع',' multiple data-selected-text-format'=>'count > 3']) !!}
       
            </div>
              <div class="col">
              	 {!! Form::select('sell_type_id[]',$sell_types,null,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>trans('main.sell_type'),' multiple data-selected-text-format'=>'count > 3']) !!}
       
              </div>
          </div>
            <div class="form-row" style="margin:5px;">
            <div class="col">
              {!! Form::select('region_id[]',$regions,null,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'المنطقة',' multiple data-selected-text-format'=>'count > 3']) !!}
            </div>
            <div class="col">
             {!! Form::select('make_id[]',$makes,null,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'الشركة',' multiple data-selected-text-format'=>'count > 3']) !!}
            </div>
          </div>
            <div class="form-row" style="margin:5px;">
            <div class="col">
              {!! Form::select('model_id[]',$models,null,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'الموديل',' multiple data-selected-text-format'=>'count > 3']) !!}
            </div>
            <div class="col">
               {!! Form::select('gear_id[]',$gears,null,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'ناقل الحركىة',' multiple data-selected-text-format'=>'count > 3']) !!}
            </div>
          </div>
             <div class="form-row" style="margin:5px;">
            <div class="col">
               {!! Form::select('condtion_id[]',$condtions,null,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'حالة المركبة',' multiple data-selected-text-format'=>'count > 3']) !!}
            </div>
            <div class="col">
               {!! Form::select('body_type_id[]',$body_types,null,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'حالة جسم المركبة',' multiple data-selected-text-format'=>'count > 3']) !!}
            </div>
          </div>
            <div class="form-row" style="margin:5px;">
            <div class="col">
                 {!! Form::select('fuel_type_id[]',$fuel_types,null,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'نوع الوقود',' multiple data-selected-text-format'=>'count > 3']) !!}
            </div>
            <div class="col">
              {!! Form::select('vehicle_status_id[]',$vehicle_status,null,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'حالة المركبة',' multiple data-selected-text-format'=>'count > 3']) !!}
            </div>
          </div>

     

  </div>
  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
          <div class="form-row" style="margin:5px;">
            <div class="col">
                {!! Form::number('price_min','',['class'=>'text-center  form-control ','placeholder'=>'اقل سعر']) !!}
            </div>
            <div class="col">
             {!! Form::number('price_max','',['class'=>'text-center  form-control ','placeholder'=>'اعلى سعر']) !!}
            </div>
          </div>

           <div class="form-row" style="margin:5px;">
            <div class="col">
                {!! Form::number('year_min','',['class'=>'text-center  form-control ','placeholder'=>'من موديل']) !!}
            </div>
            <div class="col">
             {!! Form::number('year_max','',['class'=>'text-center  form-control ','placeholder'=>'الى موديل ']) !!}
            </div>
          </div>

           <div class="form-row" style="margin:5px;">
            <div class="col">
                {!! Form::number('power_min','',['class'=>'text-center  form-control ','placeholder'=>'اقل قوة محرك']) !!}
            </div>
            <div class="col">
             {!! Form::number('power_max','',['class'=>'text-center  form-control ','placeholder'=>'اعل قوة محرك']) !!}
            </div>
          </div>

          <div class="form-row" style="margin:5px;">
            <div class="col">
                {!! Form::number('num_of_seats','',['class'=>'text-center  form-control ','placeholder'=>'عدد المقاعد']) !!}
            </div>
            <div class="col">
             {!! Form::number('body_color','',['class'=>'text-center  form-control ','placeholder'=>'لون المركبة']) !!}
            </div>
          </div>
          <div class="form-row" style="margin:5px;">
            <div class="col">
                {!! Form::number('previous_owners','',['class'=>'text-center  form-control ','placeholder'=>'ملاك سابقون']) !!}
            </div>
           
          </div>

  </div>
  <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
      

  <div class="form-row" style="margin:5px;">
                    @foreach($equipments as $key=>$equipment)
                         <div class="col"><input type="checkbox" name="equipment_id[]" value="{{$key}}"> {{($equipment)}}</div>
                    @endforeach
  </div>

  </div>
</div>


        <div class="text-center"><button type="submit" class="btn btn-primary"><span class="total_results"></span>بحث</button></div>



      

{!! Form::close() !!}

    </div>

  </div>



 <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 300px;
        margin: 5px;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
        <div id="map">here</div>
<script>

    @php
    $sellers = App\Seller::all();

    $mosts = App\Vehicle::limit(4)->get();

    @endphp
      function initMap() {
     

    var optionsCarte = {
        center: {lat: 31.606895, lng: 35.028917},
        maxZoom: 13

    }

     var map = new google.maps.Map(document.getElementById("map"),optionsCarte);
    var markerBounds = new google.maps.LatLngBounds();
      

        @foreach($sellers  as $seller)

     

          //marker start
          var myLatLng = {lat: {{$seller->lat}}, lng: {{$seller->lon}}};
 markerBounds.extend(myLatLng);
          var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'Hello World!'
          });

           

          //info windo start
            var infoWindow = null;

           infoWindow = new google.maps.InfoWindow();
              var windowLatLng = new google.maps.LatLng({{$seller->lat}},{{$seller->lon}});
              infoWindow.setOptions({
                  content: "{{$seller->name}}",
                  position: windowLatLng,
              });
          infoWindow.open(map);

          //info windo end



    

        @endforeach

           map.fitBounds(markerBounds);



      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyBToaaKJAoTl6Wa6ckuPP0QwTe34-6A4&callback=initMap">
    </script>



  <div class="row mb-2">

    @foreach($mosts as $vehicle)
 <div class="col-lg-6" >
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative" style="background: white;">
          <div class="col-lg-4  d-lg-block">
          <img style="width: 100%;" src="{{url('public/storage/'.@$vehicle->cover->disk_name,@$vehicle->cover->file_name)}}">
        </div>
        <div class="col p-4 d-flex flex-column position-static">
          <h3 class="mb-0">{{$vehicle->make->name}},{{$vehicle->model->name}},<b>{{@$vehicle->year}}</b></h3>
          <div class="mb-1 text-muted">{{@$vehicle->seller->name}},{{@$vehicle->region->name}}</div>
          <p class="card-text mb-auto">
             <div class="form-row">
              <div class="col">{{@$vehicle->price}}</div>
              <div class="col">{{@$vehicle->power}}</div>
              <div class="col">{{@$vehicle->num_of_seats}}</div>
              <div class="col">{{@$vehicle->body_color}}</div>
            </div>
            <div class="form-row">
              <div class="col">{{@$vehicle->sell_type->name}}</div>
              <div class="col">{{@$vehicle->vehicle_type->name}}</div>
              <div class="col">{{@$vehicle->gear->name}}</div>
              <div class="col">{{@$vehicle->condtion->name}}</div>
              <div class="col">{{@$vehicle->fuel_type->name}}</div>
            </div>

          </p>
         
          <div>
            @foreach($vehicle->photos as $photo)
            <img style="height: 30px;margin: 2px;" src="{{url('public/storage/'.@$photo->disk_name,@$photo->file_name)}}">
            @endforeach
          </div>
           <a href="{{url('show',$vehicle->id)}}" class="stretched-link">عرض</a>
        </div>
      
      </div>
    </div>
    @endforeach


  </div>



    <div class="row">
    <div class="col-md-8 blog-main">
      <h3 class="pb-4 mb-4 font-italic border-bottom">
        About maricars
      </h3>


 

    </div><!-- /.blog-main -->

    <aside class="col-md-4 blog-sidebar">
      <div class="p-4 mb-3 bg-light rounded">
        <h4 class="font-italic">About</h4>
        <p class="mb-0">Maria cars is ve</p>
      </div>

    </aside><!-- /.blog-sidebar -->

  </div><!-- /.row -->
@endsection
