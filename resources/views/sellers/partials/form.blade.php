


 {!! Form::hidden('form', $form) !!}

 <section class="basic-elements">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@lang('main.seller')</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <div class="row">

                            <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('name',trans('main.name'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!!  Form::text('name',old('name'), ['id'=>"name",'class'=>"form-control",'required'=>'true']) !!}
                                </fieldset>
                            </div>

                            <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('email',trans('main.email'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!!  Form::text('email',request()->get('email'), ['id'=>"email",'class'=>"form-control",'required'=>'true']) !!}
                                </fieldset>
                            </div>

                              <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('mobile',trans('main.mobile'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!!  Form::text('mobile',request()->get('mobile'), ['id'=>"mobile",'class'=>"form-control",'required'=>'true']) !!}
                                </fieldset>
                            </div>


                            <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('username',trans('main.username'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!!  Form::text('username',request()->get('username'), ['id'=>"username",'class'=>"form-control"]) !!}
                                </fieldset>
                            </div>


                            <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('region',trans('main.region'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!! Form::select('region_id',$regions,@$user->region_id,['class'=>'form-control','id'=>'region_id']) !!}
                                </fieldset>
                            </div>

                            <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('user_status',trans('main.status'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!! Form::select('user_status',trans('main.yes_no'),null,['class'=>'form-control','id'=>'user_status']) !!}
                                </fieldset>
                            </div>
                  

                             <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('address',trans('main.address'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!!  Form::text('address',old('address'), ['id'=>"address",'class'=>"form-control",'required'=>'true']) !!}
                                </fieldset>
                            </div>




                            <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('seller_name',trans('main.seller_name'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!!  Form::text('seller_name',old('seller_name'), ['id'=>"seller_name",'class'=>"form-control",'required'=>'true']) !!}
                                </fieldset>
                            </div>

                            <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('seller_mobile',trans('main.seller_mobile'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!!  Form::text('seller_mobile',old('seller_mobile'), ['id'=>"seller_mobile",'class'=>"form-control",'required'=>'true']) !!}
                                </fieldset>
                            </div>

                                <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('seller_email',trans('main.seller_email'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!!  Form::text('seller_email',old('seller_email'), ['id'=>"seller_email",'class'=>"form-control",'required'=>'true']) !!}
                                </fieldset>
                            </div>

                             <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('max_vehicles',trans('main.max_vehicles'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!!  Form::text('max_vehicles',old('max_vehicles'), ['id'=>"max_vehicles",'class'=>"form-control",'required'=>'true']) !!}
                                </fieldset>
                            </div>

                             <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('seller_address',trans('main.seller_address'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!!  Form::text('seller_address',old('seller_address'), ['id'=>"seller_address",'class'=>"form-control",'required'=>'true']) !!}
                                </fieldset>
                            </div>

                                 <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('seller_region_id',trans('main.seller_region_id'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!! Form::select('seller_region_id',$regions,@$user->seller_region_id,['class'=>'form-control','id'=>'seller_region_id']) !!}
                                </fieldset>
                            </div>


                                <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('seller_order',trans('main.seller_order'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!!  Form::text('seller_order',old('seller_order'), ['id'=>"seller_order",'class'=>"form-control",'required'=>'true']) !!}
                                </fieldset>
                            </div>



                             <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('seller_lat',trans('main.seller_lat'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!!  Form::text('seller_lat',old('seller_lat'), ['id'=>"seller_lat",'class'=>"form-control",'required'=>'true']) !!}
                                </fieldset>
                            </div>

                             <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('seller_lon',trans('main.seller_lon'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!!  Form::text('seller_lon',old('seller_lon'), ['id'=>"seller_lon",'class'=>"form-control",'required'=>'true']) !!}
                                </fieldset>
                            </div>

                         <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('publish_status',trans('main.publish_status'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!! Form::select('publish_status',trans('main.yes_no'),null,['class'=>'form-control','id'=>'publish_status']) !!}
                                </fieldset>
                            </div>

                            
                         <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('can_ignor_price',trans('main.can_ignor_price'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!! Form::select('can_ignor_price',trans('main.yes_no'),null,['class'=>'form-control','id'=>'can_ignor_price']) !!}
                                </fieldset>
                            </div>

                            <div class="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                    {!! Form::label('can_show_first_payment',trans('main.can_show_first_payment'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
                                    {!! Form::select('can_show_first_payment',trans('main.yes_no'),null,['class'=>'form-control','id'=>'can_show_first_payment']) !!}
                                </fieldset>
                            </div>

                        </div>
  {!! Form::hidden('seller_id',$seller->id) !!}

                        <div class="form-group overflow-hidden">
                                <div class="col-12">
                                    <button data-repeater-create="" class="btn btn-primary btn-lg">
                                        <i class="icon-plus4"></i>  {{$btn}}
                                    </button>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">


            <div class="card">
                <div class="card-header">
                <h4 class="card-title">@lang('main.map')</h4>
                </div>
                <div class="card-content">
                <div class="card-body">
                    <div id="map"></div>
                    <div class="text-center" id="current"></div>
                </div>
                </div>
            </div>

               <div class="card">
                <div class="card-header">
                <h4 class="card-title">@lang('main.photos')</h4>
                </div>
                <div class="card-content">
                <div class="card-body">
                    
                        @foreach($seller->photos as $photo)
                            <div class="col-md-4" style="position: relative;">
                                 <img  class="img-thumbnail rounded" style="width: 100%; margin: 5px;height: 120px;" src="{{url('public/storage/'.@$photo->disk_name,@$photo->file_name)}}">
                            </div>
                            @endforeach

                </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('js')


<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB6Y-6fXlb6F3Wix7R2Bj5A5QsTgQF0JkU&callback=initMap">
</script>


<script type="text/javascript">
    function initMap() {
  var myLatLng = {lat: {{$seller->seller_lat}}, lng: {{$seller->seller_lon}} };

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 13,
    center: myLatLng,

  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
     draggable: true,
    title: 'Hello World!'
  });

google.maps.event.addListener(marker, 'dragend', function(evt){
    document.getElementById('current').innerHTML = '<p>Marker dropped: Current Lat: ' + evt.latLng.lat().toFixed(3) + ' Current Lng: ' + evt.latLng.lng().toFixed(3) + '</p>';

    $('#seller_lat').val(evt.latLng.lat().toFixed(8));
    $('#seller_lon').val(evt.latLng.lng().toFixed(8));
});

google.maps.event.addListener(marker, 'dragstart', function(evt){
    document.getElementById('current').innerHTML = '<p>Currently dragging marker...</p>';
     $('#seller_lat').val(evt.latLng.lat().toFixed(8));
    $('#seller_lon').val(evt.latLng.lng().toFixed(8));
});



}
</script>

<style type="text/css">
    #map {
  height: 400px;
}
/* Optional: Makes the sample page fill the window. */
html, body {
  height: 100%;
  margin: 0;
  padding: 0;
}
</style>
@endsection



