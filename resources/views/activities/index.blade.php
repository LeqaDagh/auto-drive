@extends('layouts.app')

@section('content')
<div  class="container-fluid">
	



@include('partials.breadcrumbs', ['method' =>trans('main.view'), 'action' =>trans('main.activities')])

@include('partials.search_form',['name'=>trans('main.activities'),'url'=>'activities','blade'=>'activities.partials.search_form'])



            <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="fa fa-list" aria-hidden="true"></i> </span>
        </div>
        <div class="widget-content nopadding">
                    <div class="row">
                      {!! Form::open(['method' =>'GET','class'=>'form-horizontal','url'=>'/activities','autocomplete'=>'off']) !!}





	@if (count($activities))

<table class="table table-striped {{trans('css.text-align')}} {{trans('css.direction')}}" >
	<tr>
		<td>{{trans('main.activity')}}</td>

		<td>{{trans('main.activity_time')}}</td>

	</tr>
	@foreach ($activities as $activity)
	<tr>
    <td>
      {{trans('main.do')}}  
      @if($activity->user!=null) {!!$activity->user->connectStatus()!!}     @endif
      {{@$activity->user->name}}
       {{ trans('activities.'.$activity->name) }}

    </td>

		<td>{{$activity->created_at}}</td>
	</tr>
			
		@endforeach
</table>


</div>
	@endif
@endsection

@section('js')

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript">
  $('.multiselect').select2();
  
</script>  
@endsection

