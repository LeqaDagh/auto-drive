@extends('layouts.app')

@section('content')
  @include('partials.breadcrumbs', ['method' =>['name'=>trans('main.moulds'),'url'=>url('moulds')], 'action' =>$mould->name])


  @include('partials.errors')

        {!! Form::model($mould,['method'=>'PATCH','class'=>'form-horizontal','files' => true,'action'=>['MouldsController@update',$mould->id]]) !!}
       @include('moulds.partials.form',['btn' =>trans('main.edit'), 'form' =>'editing'])
     {!! Form::close() !!}

@endsection


@section('js')
  @include('js.csrf')
@endsection



