@extends('layouts.app')

@section('content')
        @include('partials.breadcrumbs', ['method' =>['name'=>trans('main.moulds'),'url'=>url('moulds')], 'action' =>trans('main.create')])
        @include('partials.errors')

            {!! Form::open(['url' => 'moulds','class'=>'form-horizontal']) !!}
                @include('moulds.partials.form',['btn' =>trans('main.create'), 'form' =>'adding'])
            {!! Form::close() !!}

@endsection
    





