<!doctype html>
<html lang="ar" dir="rtl">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>{{ config('app.name', '') }}</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/blog/">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">


    <style>
    body{
        background: #eee;
        text-align: right;
    }
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">

    <!-- Custom styles for this template -->
    <style type="text/css">
        
        /* stylelint-disable selector-list-comma-newline-after */

.blog-header {
  line-height: 1;
  border-bottom: 1px solid #e5e5e5;
}

.blog-header-logo {
  font-family: "Playfair Display", Georgia, "Times New Roman", serif;
  font-size: 2.25rem;
}

.blog-header-logo:hover {
  text-decoration: none;
}

h1, h2, h3, h4, h5, h6 {
  font-family: "Playfair Display", Georgia, "Times New Roman", serif;
}

.display-4 {
  font-size: 2.5rem;
}
@media (min-width: 768px) {
  .display-4 {
    font-size: 3rem;
  }
}

.nav-scroller {
  position: relative;
  z-index: 2;
  height: 2.75rem;
  overflow-y: hidden;
}

.nav-scroller .nav {
  display: -ms-flexbox;
  display: flex;
  -ms-flex-wrap: nowrap;
  flex-wrap: nowrap;
  padding-bottom: 1rem;
  margin-top: -1px;
  overflow-x: auto;
  text-align: center;
  white-space: nowrap;
  -webkit-overflow-scrolling: touch;
}

.nav-scroller .nav-link {
  padding-top: .75rem;
  padding-bottom: .75rem;
  font-size: .875rem;
}

.card-img-right {
  height: 100%;
  border-radius: 0 3px 3px 0;
}

.flex-auto {
  -ms-flex: 0 0 auto;
  flex: 0 0 auto;
}

.h-250 { height: 250px; }
@media (min-width: 768px) {
  .h-md-250 { height: 250px; }
}

/*
 * Blog name and description
 */
.blog-title {
  margin-bottom: 0;
  font-size: 2rem;
  font-weight: 400;
}
.blog-description {
  font-size: 1.1rem;
  color: #999;
}

@media (min-width: 40em) {
  .blog-title {
    font-size: 3.5rem;
  }
}

/* Pagination */
.blog-pagination {
  margin-bottom: 4rem;
}
.blog-pagination > .btn {
  border-radius: 2rem;
}

/*
 * Blog posts
 */
.blog-post {
  margin-bottom: 4rem;
}
.blog-post-title {
  margin-bottom: .25rem;
  font-size: 2.5rem;
}
.blog-post-meta {
  margin-bottom: 1.25rem;
  color: #999;
}

/*
 * Footer
 */
.blog-footer {
  padding: 2.5rem 0;
  color: #999;
  text-align: center;
  background-color: #f9f9f9;
  border-top: .05rem solid #e5e5e5;
}
.blog-footer p:last-child {
  margin-bottom: 0;
}
    </style>
  </head>
  <body>
    <div class="container" style="background: white;">
  <header class="blog-header py-3">
    <div class="row flex-nowrap justify-content-between align-items-center">
      <div class="col-4 pt-1 text-right" >
        <a class="text-muted" href="#">Subscribe</a>
      </div>
      <div class="col-4 text-center">
        <a class="blog-header-logo text-dark" href="#">
            <img src="{{url('public/logo.png')}}" height="60">
        </a>
      </div>
      <div class="col-4 d-flex justify-content-end align-items-center">
        <a class="text-muted" href="#">
          <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="mx-3" role="img" viewBox="0 0 24 24" focusable="false"><title>Search</title><circle cx="10.5" cy="10.5" r="7.5"/><path d="M21 21l-5.2-5.2"/></svg>
        </a>
        <a class="btn btn-sm btn-outline-secondary" href="#">تسجيل</a>
      </div>
    </div>
  </header>

  <div class="nav-scroller py-1 mb-2">
    <nav class="nav d-flex justify-content-between">
      <a class="p-2 text-muted" href="#"></a>
      <a class="p-2 text-muted" href="#">دراجات للايجار</a>
      <a class="p-2 text-muted" href="#">افضل العروض</a>
      <a class="p-2 text-muted" href="#">سيارات للايجار</a>
      <a class="p-2 text-muted" href="#">سيارات للبيع</a>
      <a class="p-2 text-muted" href="#">سيارات مستعمل</a>
      <a class="p-2 text-muted" href="#">دراجات للايجار</a>
      <a class="p-2 text-muted" href="#">افضل العروض</a>
      <a class="p-2 text-muted" href="#">سيارات للايجار</a>
      <a class="p-2 text-muted" href="#">سيارات للبيع</a>
      <a class="p-2 text-muted" href="#">سيارات مستعمل</a>
    </nav>
  </div>



</div>

<div class="container">
  <div class="row">
    <div class="col-lg-8" >
      <div class="row">
      @foreach($vehicles as $vehicle)

      <div class="col-lg-6" >
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative" style="background: white;">
        <div class="col p-4 d-flex flex-column position-static">
          <h3 class="mb-0">{{$vehicle->name}}</h3>
          <div class="mb-1 text-muted">{{$vehicle->make->name}},{{$vehicle->model->name}}</div>
          <p class="card-text mb-auto">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
          <a href="#" class="stretched-link">more</a>
        </div>
        <div class="col d-none d-lg-block">
          <img style="width: 100%;" src="{{url('public/storage/'.@$vehicle->cover->disk_name,@$vehicle->cover->file_name)}}">
        </div>
      </div>
    </div>
      @endforeach
    </div>
    </div>
    <div class="col-lg-4">
      
  <div style="background: white;" >
    <div >
    






{!! Form::open(['url' => 'search','method'=>'get','class'=>'form-horizontal search_form','style'=>'padding:10px;']) !!}


<div >
                <div class="" style="margin:5px;">
                    @foreach($sell_types as $key=>$sell_type)
                         <div ><input type="checkbox" name="sell_type_id[]" value="{{($key)}}"> {{($sell_type)}}</div>
                    @endforeach
                    </div>
             <div class="" style="margin:5px;">        
        
            <div >
             {!! Form::select('vehicle_type_id[]',$vehicle_types,request()->vehicle_type_id,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'النوع',' multiple data-selected-text-format'=>'count > 3']) !!}
       
            </div>
          </div>
            <div class="" style="margin:5px;">
            <div >
              {!! Form::select('region_id[]',$regions,request()->region_id,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'المنطقة',' multiple data-selected-text-format'=>'count > 3']) !!}
            </div>
            <div >
             {!! Form::select('make_id[]',$makes,request()->make_id,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'الشركة',' multiple data-selected-text-format'=>'count > 3']) !!}
            </div>
          </div>
            <div class="" style="margin:5px;">
            <div >
              {!! Form::select('model_id[]',$models,request()->model_id,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'الموديل',' multiple data-selected-text-format'=>'count > 3']) !!}
            </div>
            <div >
               {!! Form::select('gear_id[]',$gears,request()->gear_id,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'ناقل الحركىة',' multiple data-selected-text-format'=>'count > 3']) !!}
            </div>
          </div>
             <div class="" style="margin:5px;">
            <div >
               {!! Form::select('condtion_id[]',$condtions,request()->condtion_id,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'حالة المركبة',' multiple data-selected-text-format'=>'count > 3']) !!}
            </div>
            <div >
               {!! Form::select('body_type_id[]',$body_types,request()->body_type_id,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'حالة جسم المركبة',' multiple data-selected-text-format'=>'count > 3']) !!}
            </div>
          </div>
            <div class="" style="margin:5px;">
            <div >
                 {!! Form::select('fuel_type_id[]',$fuel_types,request()->fuel_type_id,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'نوع الوقود',' multiple data-selected-text-format'=>'count > 3']) !!}
            </div>
            <div >
              {!! Form::select('vehicle_status_id[]',$vehicle_status,request()->vehicle_status_id,['class'=>'form-control selectpicker  show-tick' ,'multiple'=>'','data-live-search'=>'true','title'=>'حالة المركبة',' multiple data-selected-text-format'=>'count > 3']) !!}
            </div>
          </div>

     


          <div class="form-row" style="margin:5px;">
            <div  class="col">
                {!! Form::number('price_min',request()->price_min,['class'=>'text-center  form-control ','placeholder'=>'اقل سعر']) !!}
            </div>
            <div  class="col">
             {!! Form::number('price_max',request()->price_max,['class'=>'text-center  form-control ','placeholder'=>'اعلى سعر']) !!}
            </div>
          </div>

           <div class="form-row" style="margin:5px;">
            <div class="col">
                {!! Form::number('year_min',request()->year_min,['class'=>'text-center  form-control ','placeholder'=>'من موديل']) !!}
            </div>
            <div class="col">
             {!! Form::number('year_max',request()->year_max,['class'=>'text-center  form-control ','placeholder'=>'الى موديل ']) !!}
            </div>
          </div>

           <div class="form-row" style="margin:5px;">
            <div class="col">
                {!! Form::number('power_min',request()->power_min,['class'=>'text-center  form-control ','placeholder'=>'اقل قوة محرك']) !!}
            </div>
            <div class="col">
             {!! Form::number('power_max',request()->power_max,['class'=>'text-center  form-control ','placeholder'=>'اعل قوة محرك']) !!}
            </div>
          </div>

          <div class="" style="margin:5px;">
            <div >
                {!! Form::number('num_of_seats',request()->num_of_seats,['class'=>'text-center  form-control ','placeholder'=>'عدد المقاعد']) !!}
            </div>
            <div >
             {!! Form::number('body_color',request()->body_color,['class'=>'text-center  form-control ','placeholder'=>'لون المركبة']) !!}
            </div>
          </div>
          <div class="" style="margin:5px;">
            <div >
                {!! Form::number('previous_owners',request()->previous_owners,['class'=>'text-center  form-control ','placeholder'=>'ملاك سابقون']) !!}
            </div>
           
          </div>


      

  <div class="" style="margin:5px;">
                    @foreach($equipments as $key=>$equipment)
                    {!! Form::label('equipment_id') !!}
                    {!! Form::checkbox('equipment_id[]',$key,request()->equipment_id,['class'=>'text-center  form-control ']) !!}
                       
                    @endforeach
  </div>


</div>


        <div class="text-center"><button type="submit" class="btn btn-primary"><span class="total_results"></span>بحث</button></div>



      

{!! Form::close() !!}

    </div>

  </div>

    </div>
  </div>
</div>

<footer class="blog-footer">
  <p>Blog template built for <a href="https://getbootstrap.com/">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.</p>
  <p>
    <a href="#">Back to top</a>
  </p>
</footer>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>

<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/i18n/defaults-*.min.js"></script>

<script type="text/javascript">
    
      $( document ).delegate( ".search_form select,input", "change", function() {


         //call by ajax
         $.post( '{{url("api/v1/vehicles")}}', $('.search_form').serialize())
          .done(function( data ) {

                $('.total_results').text(data);
             

            }); 

      });


</script>
</body>
</html>
