@extends('layouts.app')

@section('content')
  @include('partials.breadcrumbs', ['method' =>['name'=>trans('main.register'),'url'=>url('register')], 'action' =>trans('main.edit')])


  @include('partials.errors')
   
        {!! Form::model($register,['method'=>'PATCH','class'=>'form-horizontal','files' => true,'action'=>['RegistersController@update',$register->id]]) !!}
	   @include('registers.partials.form',['btn' =>trans('main.edit'), 'form' =>'editing'])
     {!! Form::close() !!}
 
@endsection





