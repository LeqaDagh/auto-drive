@extends('layouts.app')

@section('content')


@include('partials.breadcrumbs', ['method' =>['name'=>trans('main.registers'),'url'=>url('registers')], 'action' =>trans('main.show')])
<h1>{{$register->name}}</h1>



   <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@lang('main.sellers')</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                    		
                    	@if(count($register->sellers))


							<table class="table table-striped " >
							  <thead>
							  <tr>
							  	<th>{{trans('main.cover')}}</th>
							  	<th>{{trans('main.name')}}</th>
							  	<th>{{trans('main.mobile')}}</th>
							    <th>{{trans('main.region')}}</th>
							    <th>{{trans('main.vehicles')}}</th>
							    @can('show_sellers')
							    <th >{{trans('main.show')}}</th>
							    @endcan
							    <th>@lang('main.options')</th>

							  </tr>
							</thead>
							<tbody>
							  @foreach ($register->sellers()->latest('id')->get() as $seller)
							  <tr>
							    <th scope="row"><img width="30" src="{{url('public/storage/'.@$seller->cover->disk_name,@$seller->cover->file_name)}}"></u></th>
							    <td >{{@$seller->seller_name}}</td>
							    <td >{{@$seller->seller_mobile}}</td>
							    <td >{{@$seller->region->name}}</td>
							    <td >{{@$seller->vehicles()->count()}}</td>
							    @can('show_sellers')
							    <td><a href="{{url('sellers',$seller->id)}}">{{trans('main.show')}}</a></td>
							    @endcan
							    <td>
							    
							    	<a class="btn btn-sm btn-primary" href="{{url('vehicles')}}?seller_id={{$seller->id}}">@lang('main.vehicles')</a>
							    	<a class="btn btn-sm btn-primary" href="{{url('sellers',$seller->id)}}/edit">@lang('main.edit')</a>
							    </td>
							  </tr>
							      
							    @endforeach
							  </tbody>
							</table>
                    	@endif


                    </div>
                </div>
            </div>


            		<div class="card">
		                <div class="card-header">
		                    <h4 class="card-title">@lang('main.register')</h4>
		                </div>
		                <div class="card-content">
		                    <div class="card-body">

		                    	<table class="table table-striped " >
									
									  	 <tr><td>{{trans('main.id')}}</td><td>{{$register->id}}</td></tr>  	 
									     <tr><td>{{trans('main.name')}}</td><td>{{$register->name}}</td></tr>
									     <tr><td>{{trans('main.region')}}</td><td>{{@$register->region->name}}</td></tr>
									     <tr><td>{{trans('main.mobile')}}</td><td>{{$register->mobile}}</td></tr>
									     <tr><td>{{trans('main.status')}}</td><td> {{trans('main.user_status')[$register->user_status]}}</td></tr>

							
								</table>
		                    </div>
		                </div>
		            </div>
        </div>

		 <div class="col-md-4">
		            <div class="card">
		                <div class="card-header">
		                    <h4 class="card-title">@lang('main.options')</h4>
		                </div>
		                <div class="card-content">
		                    <div class="card-body">
		                    	<a href="#" class="btn btn-primary add_seller_for_register" register_id="{{$register->id}}">@lang('main.add_new_seller')</a>
		                    </div>
		                </div>
		            </div>


		              <div class="card">
		                <div class="card-header">
		                    <h4 class="card-title">@lang('main.map')</h4>
		                </div>
		                <div class="card-content">
		                    <div class="card-body">
		                    	
		                    	<div id="map2" style="height: 300px; width: 100%;"></div>
		                    </div>
		                </div>
		            </div>
		</div>


    </div>






@endsection


@section('js')
@include('registers.js.show.add_seller')
@include('registers.js.show.add_vehicle')
@include('registers.js.show.show_vehicles')
@endsection

 
  
   
