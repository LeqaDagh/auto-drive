<!-- Modal -->
<div class="modal fade" id="show_vehicle_for_seller_modal" tabindex="-1" role="dialog" aria-labelledby="show_vehicle_for_seller_modal_label">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="show_vehicle_for_seller_modal_label">@lang('main.add_seller')</h4>
      </div>
      <div class="modal-body">
         {!! Form::open(['autocomplete'=>'off','class'=>'form-horizontal show_vehicle_for_seller_form ','url'=>'registers/add_car_for_register_form_insert' ,'files'=>true]) !!}
               
		<div class="text-center"><h1>{{$seller->name}}</h1></div>

    	<div class="text-center">
				@foreach($seller->vehicles as $vehicle)
          <div class="form-row">
            <div class="form-group col-md-3">
            <div><img  class="img-thumbnail rounded" style="width: 100%;max-height: 220px;" src="{{url('public/storage/'.@$vehicle->cover->disk_name,@$vehicle->cover->file_name)}}"></div>
            <div>
                <div>{{$vehicle->make->name}},{{$vehicle->model->name}}</div>
                <div><a class="btn btn-primary" href="{{url('vehicles',$vehicle->id)}}">@lang('main.view')</a></div>
            </div>
            </div>
          </div>
					
				@endforeach
                   
</div>
 				

                    {!! Form::hidden('seller_id', $seller->id) !!}




              {!! Form::close() !!}
      </div>
      <div ><div class="show_vehicle_for_seller_errors alert alert-danger" style="display: none;"></div></div>
       <div ><div class="show_vehicle_for_seller_success alert alert-success text-center" style="display: none;"></div></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">الغاء</button>
      </div>
    </div>
  </div>
</div>


