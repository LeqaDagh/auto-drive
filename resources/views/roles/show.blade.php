@extends('layouts.app')

@section('content')

    @include('partials.breadcrumbs', ['method' =>['name'=>trans('main.roles'),'url'=>url('roles')], 'action' =>$role->label])

          		@if(count($role->permissions))
				@foreach ($role->permissions as $permission)
				
				<div class="col-lg-4 {{trans('main.style.pull')}}">
          			<?php $id ='checkbox'.$permission->id;  ?>
                      
					{!! Form::label($id, $permission->label) !!}
					
				</div>

				@endforeach
				@endif




	@endsection
