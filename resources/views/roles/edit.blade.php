@extends('layouts.app')

@section('content')
  @include('partials.breadcrumbs', ['method' =>['name'=>trans('main.roles'),'url'=>url('roles')], 'action' =>$role->label])

  @include('partials.errors')

        {!! Form::model($role,['method'=>'PATCH','class'=>'form-horizontal','files' => true,'action'=>['RolesController@update',$role->id]]) !!}
       @include('roles.partials.form',['btn' =>trans('main.edit'), 'form' =>'editing'])
     {!! Form::close() !!}

@endsection


@section('js')
  

@endsection



