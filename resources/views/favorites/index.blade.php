@extends('layouts.app')

@section('content')
<div >
	


@include('partials.breadcrumbs', ['method' =>['name'=>trans('main.favorites'),'url'=>url('favorites')], 'action' =>trans('main.view')])


<div class="card">
    		@include('partials.card_header', ['title' =>trans('main.favorites')])
            <div class="card-content collapse show">
                <div class="card-body">
                    <p class="card-text">
				
					</p>
           
                </div>
                
                    @if (count($favorites))
                    <div class="table-responsive">

<table class="table table-striped " >
	<thead>
	<tr>
		<th>{{trans('main.id')}}</th>
		<th>{{trans('main.name')}}</th>
		<th>{{trans('main.make')}}</th>
		<th>{{trans('main.mould')}}</th>
		<th>{{trans('main.created_at')}}</th>
	</tr>
</thead>
<tbody>
	@foreach ($favorites as $favorite)
	<tr>
		<th scope="row">{{$favorite->id}}</th>
		<td>{{@$favorite->user->name}}</td>
		<td>{{@$favorite->vehicle->make->name }}</td>
		<td>{{@$favorite->vehicle->mould->name }}</td>
		<td>{{$favorite->created_at}}</td>
	</tr>
			
		@endforeach
	</tbody>
</table>

{{$favorites->appends(request()->query())}}
</div>
	@endif
                </div>
            </div>
        </div>






	
@endsection


