@extends('layouts.app')

@section('content')


@include('partials.breadcrumbs', ['method' =>['name'=>trans('main.supports'),'url'=>url('supports')], 'action' =>trans('main.show')])
<h1>{{$support->name}}</h1>
@endsection
