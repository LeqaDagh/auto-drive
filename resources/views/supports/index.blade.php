@extends('layouts.app')

@section('content')
<div >
  


@include('partials.breadcrumbs', ['method' =>['name'=>trans('main.supports'),'url'=>url('supports')], 'action' =>trans('main.view')])



@include('partials.search_form',['name'=>trans('main.search'),'url'=>'supports','blade'=>'supports.partials.search_form'])


<div class="card">
        @include('partials.card_header', ['title' =>trans('main.supports')])
            <div class="card-content collapse show">
                <div class="card-body">
                    <p class="card-text">
           @can('create_supports')
           <div >
            <a href="{{url('supports','create')}}" class="btn btn-primary">@lang('main.create')</a>
           </div>
           @endcan 
          </p>
           
                </div>
               
                    @if (count($supports))
 <div class="table-responsive">
<table class="table table-striped " >
  <thead>
  <tr>
    <th>{{trans('main.id')}}</th>
    <th>{{trans('main.name')}}</th>
    <th>{{trans('main.username')}}</th>
    <th>{{trans('main.region')}}</th>
    <th>{{trans('main.mobile')}}</th>
    <th>{{trans('main.status')}}</th>
    <th class="text-center">{{trans('main.options')}}</th>

    @can('show_supports')
    <th >{{trans('main.show')}}</th>
    @endcan

  </tr>
</thead>
<tbody>
  @foreach ($supports as $user)
  <tr>
    <th scope="row">{{$user->id}}</th>
    <td>{!!$user->connectStatus()!!} {{$user->name}} </td>
    <td>{{$user->username}} </td>
    <td>{{@$user->region->name}}</td>
    <td>{{$user->mobile}}</td>
    <td>{{trans('main.user_status.'.$user->user_status)}}</td>
    <td class="text-center">

    @can('edit_supports')
    <a href="{{url('supports',$user->id)}}/edit"><i class="fas fa-edit"></i></a>
    @endcan
    
    @can('destroy_supports')
    <i class="fas fa-trash-alt delete-item"  delete_item_id="{{$user->id}}"></i>
    @endcan

    </td>

    @can('show_supports')
    <td><a href="{{url('supports',$user->id)}}">{{trans('main.show')}}</a></td>
    @endcan
  </tr>
      
    @endforeach
  </tbody>
</table>

{{$supports->appends(request()->query())}}


</div>
  @endif
                </div>
            </div>
        </div>






  
@endsection

