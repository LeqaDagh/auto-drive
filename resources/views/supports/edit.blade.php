@extends('layouts.app')

@section('content')
  @include('partials.breadcrumbs', ['method' =>['name'=>trans('main.supports'),'url'=>url('supports')], 'action' =>trans('main.edit')])


  @include('partials.errors')
   
        {!! Form::model($support,['method'=>'PATCH','class'=>'form-horizontal','files' => true,'action'=>['SupportsController@update',$support->id]]) !!}
	   @include('supports.partials.form',['btn' =>trans('main.edit'), 'form' =>'editing'])
     {!! Form::close() !!}
 
@endsection





