@extends('layouts.app')

@section('content')
        @include('partials.breadcrumbs', ['method' =>['name'=>trans('main.supports'),'url'=>url('supports')], 'action' =>trans('main.create')])
        @include('partials.errors')

            {!! Form::open(['url' => 'supports','class'=>'form-horizontal']) !!}
                @include('supports.partials.form',['btn' =>trans('main.create'), 'form' =>'adding'])
            {!! Form::close() !!}

@endsection
    



