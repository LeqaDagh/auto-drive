@extends('layouts.app')

@section('content')
<div >
  


@include('partials.breadcrumbs', ['method' =>['name'=>trans('main.admins'),'url'=>url('admins')], 'action' =>trans('main.view')])



@include('partials.search_form',['name'=>trans('main.search'),'url'=>'admins','blade'=>'admins.partials.search_form'])


<div class="card">
        @include('partials.card_header', ['title' =>trans('main.admins')])
            <div class="card-content collapse show">
                <div class="card-body">
                    <p class="card-text">
           @can('create_admins')
           <div >
            <a href="{{url('admins','create')}}" class="btn btn-primary">@lang('main.create')</a>
           </div>
           @endcan 
          </p>
           
                </div>
               
                    @if (count($admins))
 <div class="table-responsive">
<table class="table table-striped " >
  <thead>
  <tr>
    <th>{{trans('main.id')}}</th>
    <th>{{trans('main.name')}}</th>
    <th>{{trans('main.region')}}</th>
    <th>{{trans('main.mobile')}}</th>
    <th>{{trans('main.status')}}</th>
    <th class="text-center">{{trans('main.options')}}</th>

    @can('show_admins')
    <th >{{trans('main.show')}}</th>
    @endcan

  </tr>
</thead>
<tbody>
  @foreach ($admins as $user)
  <tr>
    <th scope="row">{{$user->id}}</th>
    <td>{!!$user->connectStatus()!!} {{$user->name}} </td>
    <td>{{@$user->region->name}}</td>
    <td> {{$user->mobile}}</td>
    <td> {{trans('main.user_status.'.$user->user_status)}}</td>
    <td class="text-center">

    @can('edit_admins')
    <a href="{{url('admins',$user->id)}}/edit"><i class="fas fa-edit"></i></a>
    @endcan
    
    @can('destroy_admins')
    <i class="fas fa-trash-alt delete-item"  delete_item_id="{{$user->id}}"></i>
    @endcan

    </td>

    @can('show_admins')
    <td><a href="{{url('admins',$user->id)}}">{{trans('main.show')}}</a></td>
    @endcan
  </tr>
      
    @endforeach
  </tbody>
</table>

{{$admins->appends(request()->query())}}

</div>
  @endif
                </div>
            </div>
        </div>






  
@endsection

