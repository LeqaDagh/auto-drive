@extends('layouts.app')

@section('content')
        @include('partials.breadcrumbs', ['method' =>['name'=>trans('main.admins'),'url'=>url('admins')], 'action' =>trans('main.create')])
        @include('partials.errors')

            {!! Form::open(['url' => 'admins','class'=>'form-horizontal']) !!}
                @include('admins.partials.form',['btn' =>trans('main.create'), 'form' =>'adding'])
            {!! Form::close() !!}

@endsection
    



