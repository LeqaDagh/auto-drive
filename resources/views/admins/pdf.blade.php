@extends('layouts.pdf')

@section('content')

	 @if (count($contractors))

<table class="zui-table" >
  <thead>
  <tr>
    <th>{{trans('main.id')}}</th>
    <th>{{trans('main.name')}}</th>
    <th>{{trans('main.region')}}</th>
    <th>{{trans('main.station')}}</th>
    <th>{{trans('main.mobile')}}</th>
    <th>{{trans('main.status')}}</th>
    <th class="text-center">{{trans('main.options')}}</th>

    @can('show_contractors')
    <th >{{trans('main.show')}}</th>
    @endcan

  </tr>
</thead>
<tbody>
  @foreach ($contractors as $user)
  <tr>
    <th scope="row">{{$user->id}}</th>
    <td>{!!$user->connectStatus()!!} {{$user->name}} </td>
    <td>{{@$user->region->name}}</td>
    <td>{{@$user->station->name}}</td>
    <td> {{$user->mobile}}</td>
    <td> {{trans('main.user_status')[$user->user_status]}}</td>
    <td class="text-center">

    @can('edit_contractors')
    <a href="{{url('contractors',$user->id)}}/edit"><i class="fas fa-edit"></i></a>
    @endcan
    
    @can('destroy_contractors')
    <i class="fas fa-trash-alt delete-item"  delete_item_id="{{$user->id}}"></i>
    @endcan

    </td>

    @can('show_contractors')
    <td><a href="{{url('contractors',$user->id)}}">{{trans('main.show')}}</a></td>
    @endcan
  </tr>
      
    @endforeach
  </tbody>
</table>

@include('pagination', ['paginator' => $contractors, 'interval' => 5])


  @endif



@endsection