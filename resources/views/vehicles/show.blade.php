@extends('layouts.app')

@section('content')


	<h1>{{@$vehicle->name}}</h1>

<div class="row">
	<div class="col-lg-8">
		          <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@lang('main.show')</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">

                    	<table class="table">
                    		<tr>
                    			<td>@lang('main.id')</td><td>{{$vehicle->id}}</td>
                    		</tr>
                              <tr>
                                   <td>@lang('main.extra_title')</td><td>{{$vehicle->extra_title}}</td>
                              </tr>
                    		<tr>
                    			<td>@lang('main.seller_name')</td><td>{{@$vehicle->seller->seller_name}}</td>
                    		</tr>
                              <tr>
                                   <td>@lang('main.seller_mobile')</td><td>{{@$vehicle->seller->seller_mobile}}</td>
                              </tr>
                    		<tr>
                    			<td>@lang('main.region')</td><td>{{@$vehicle->seller->region->name}}</td>
                    		</tr>
                    		<tr>
                    			<td>@lang('main.make')</td><td>{{@$vehicle->make->name}}</td>
                    		</tr>

                    		<tr>
                    			<td>@lang('main.mould')</td><td>{{@$vehicle->mould->name}}</td>
                    		</tr>
                    		
                    	     <tr>
                    			<td>@lang('main.body_type')</td><td>{{trans('types.body_types.'.@$vehicle->body.'.title') }}</td>
                    		</tr>
                               <tr>
                                   <td>@lang('main.year_of_product')</td><td>{{$vehicle->year_of_product}}</td>
                              </tr>
                               <tr>
                                   <td>@lang('main.year_of_work')</td><td>{{$vehicle->year_of_work}}</td>
                              </tr>
                              </tr>
                                   <tr>
                                   <td>@lang('main.price')</td><td>{{$vehicle->price}}</td>
                              </tr>
                                   <tr>
                                   <td>@lang('main.power')</td><td>{{$vehicle->power}}</td>
                              </tr>
                                <tr>
                                   <td>@lang('main.hp')</td><td>{{$vehicle->hp}}</td>
                              </tr>
                              <tr>
                                   <td>@lang('main.first_payment')</td><td>{{$vehicle->first_payment}}</td>
                              </tr>
                              <tr>
                                   <td>@lang('main.mileage')</td><td>{{$vehicle->mileage}}</td>
                              </tr>
                              <tr>
                                   <td>@lang('main.gear')</td><td>{{trans('types.gear_types.'.@$vehicle->gear.'.title') }}</td>
                              </tr>
                              <tr>
                                   <td>@lang('main.fuel')</td><td>{{trans('types.fuel_types.'.@$vehicle->fuel.'.title') }}</td>
                              </tr>
                               <tr>
                                   <td>@lang('main.drivetrain_system')</td><td>{{trans('types.drivetrain_system_types.'.@$vehicle->drivetrain_system.'.title') }}</td>
                              </tr>
                              <tr>
                                   <td>@lang('main.drivetrain_type')</td><td>{{trans('types.drivetrain_type_types.'.@$vehicle->drivetrain_type.'.title') }}</td>
                              </tr>
                              <tr>
                                   <td>@lang('main.num_of_seats')</td><td>{{trans('types.num_of_seats_types.'.@$vehicle->num_of_seats.'.title') }}</td>
                              </tr>
                              <tr>
                                   <td>@lang('main.vehicle_status')</td><td>{{trans('types.vehicle_status_types.'.@$vehicle->vehicle_status.'.title') }}</td>
                              </tr>
                              <tr>
                                   <td>@lang('main.body_color')</td><td>{{trans('types.color_types.'.@$vehicle->body_color.'.title') }}</td>
                              </tr>
                              <tr>
                                   <td>@lang('main.interior_color')</td><td>{{trans('types.color_types.'.@$vehicle->interior_color.'.title') }}</td>
                              </tr>
                              <tr>
                                   <td>@lang('main.customs_exemption')</td>
                                   <td>{{trans('types.customs_exemption_types.'.@$vehicle->customs_exemption.'.title') }}</td>
                              </tr>
                              <tr>
                                   <td>@lang('main.num_of_doors')</td><td>{{trans('types.num_of_doors_types.'.@$vehicle->num_of_doors.'.title') }}</td>
                              </tr>
                               <tr>
                                   <td>@lang('main.license_date')</td>
                                   <td>{{$vehicle->license_date }}</td>
                              </tr>
                              <tr>
                                   <td>@lang('main.previous_owners')</td><td>{{trans('types.previous_owners_types.'.@$vehicle->previous_owners.'.title') }}</td>
                              </tr>
                              <tr>
                                   <td>@lang('main.origin')</td><td>{{trans('types.origin_types.'.@$vehicle->origin.'.title') }}</td>
                              </tr>
                              <tr>
                                   <td>@lang('main.num_of_keys')</td><td>{{trans('types.num_of_keys_types.'.@$vehicle->num_of_keys.'.title') }}</td>
                              </tr>
                               <tr>
                                   <td>@lang('main.desc')</td>
                                   <td>{{$vehicle->desc }}</td>
                              </tr>

                              <tr>
                                   <td>@lang('main.payment_method')</td>
                                   <td>
                                       @foreach(trans('types.payment_method_types') as $key=>$item)
                                       <span class="btn btn-sm @if($vehicle->$key==1) btn-primary @endif">{{$item['title']}}</span>
                                       @endforeach
                                   </td>
                              </tr>
                    		<tr>
                    			<td>@lang('main.publish_status')</td><td>{{trans('types.publish_status_type.'.$vehicle->publish_status.'.title')}}</td>
                    		</tr>
                    		<tr>
                    			<td>@lang('main.created_at')</td><td>{{$vehicle->created_at}}</td>
                    		</tr>
                    	</table>
@php
   $all_equipments =$vehicle->equipments()->pluck('equipment_value')->toArray();
@endphp  
                    @lang('main.extra')
                          @php   
                          $e =['ext_int_furniture',
                              'ext_int_seats',
                              'ext_int_steering',
                              'ext_int_screens',
                              'ext_int_sunroof',
                              'ext_int_glass',
                              'ext_int_other',
                              'ext_ext_light',
                              'ext_ext_mirrors',
                              'ext_ext_rims',
                              'ext_ext_cameras',
                              'ext_ext_sensors',
                              'ext_ext_other',
                              'ext_gen_other'] 
                          @endphp  
                          <table class="table">
                          @foreach($e as $i)
                          <tr>
                           <td>{{trans('main.'.$i)}}</td>
                           <td>
                           @foreach(trans('types.'.$i) as $key=>$item)
                                       <span class="btn btn-sm @if(in_array($key,$all_equipments)) btn-primary @endif">{{$item['title']}}</span>
                           @endforeach
                           </td>
                         </tr>
                          @endforeach
                          </table>


                    	 <div class="text-center">
                           @if($vehicle->stared=="no")
                           <a href="#" class="btn btn-primary make_as_stared" vehicle_id="{{$vehicle->id}}">
                            <i class="fas fa-star"></i>
                            @lang('main.make_as_stared')
                           </a>  
                           @else
                           <a href="#" class="btn btn-danger make_as_unstared" vehicle_id="{{$vehicle->id}}">
                            <i class="fas fa-star"></i>
                           @lang('main.make_as_unstared')
                           </a>
                           @endif 

                           @if($vehicle->is_deleted=="no")
                           <a href="#" class="btn btn-danger delete_vehicle" vehicle_id="{{$vehicle->id}}">
                            <i class="fas fa-trash-alt"></i>
                            @lang('main.delete_vehicle')
                           </a>  
                           @else
                            <a href="#" class="btn btn-success restore_vehicle" vehicle_id="{{$vehicle->id}}">
                            <i class="fas fa-trash-alt"></i>
                            @lang('main.restore_vehicle')
                           </a>  
                           @endif

                           @if($vehicle->publish_status!="pub")
                           <a href="#" class="btn btn-success publish_vehicle" vehicle_id="{{$vehicle->id}}">
                            <i class="fas fa-paper-plane"></i>
                            @lang('main.publish_vehicle')
                           </a> 
                           @else 
                           <a href="#" class="btn btn-danger unpublish_vehicle" vehicle_id="{{$vehicle->id}}">
                            <i class="fas fa-paper-plane"></i>
                            @lang('main.unpublish_vehicle')
                           </a> 
                           @endif 
                         </div>

                    </div>
                </div>  
                </div>  

	</div>
	<div class="col-lg-4">
		    <div class="card">
                <div class="card-header">
                    <h4 class="card-title " style="float: right;">@lang('main.photos')</h4>
                    <a class="btn btn-primary add_photos_for_vehicle " vehicle_id="{{$vehicle->id}}" style="float: left;"><i class="fas fa-plus"></i></a>
                    <div class="clearfix"></div>
                </div>
                <div class="card-content">
                    <div class="card-body">
                    	<hr>
                    	<div class="text-center">
                    		<div>@lang('main.cover')</div>
                    	<img  class="img-thumbnail rounded" style="max-width: 80%;" src="{{url('public/storage/'.@$vehicle->cover->disk_name,@$vehicle->cover->file_name)}}">
                    	</div>
                    	<hr>
                    	<div class="text-center">
                    		<div>@lang('main.photos')</div>
                    		<div class="form-row">
                    		@foreach($vehicle->photos as $photo)
                    		<div class="col-md-4" style="position: relative;">
                    			<span style="position: absolute;bottom: 10px;left: 5px;color: red;background: white;border:1px solid #666;padding: 1px;" class="remove_photo" media_id="{{$photo->id}}"><i class="far fa-trash-alt"></i></span>

                          @if($photo->id==$vehicle->slider_id)
                          <span style="position: absolute;bottom: 10px;left: 30px;color: grey;background: white;border:1px solid #666;padding: 1px;" class="delete_as_slider" media_id="{{$photo->id}}" vehicle_id="{{$vehicle->id}}"><i class="far fa-images"></i>
                          </span>
                          @else
                          <span style="position: absolute;bottom: 10px;left: 30px;color: blue;background: white;border:1px solid #666;padding: 1px;" class="set_as_slider" media_id="{{$photo->id}}" vehicle_id="{{$vehicle->id}}"><i class="far fa-images"></i>
                          </span>
                          @endif


                    			<img  class="img-thumbnail rounded" style="width: 100%; margin: 5px;height: 120px;" src="{{url('public/storage/'.@$photo->disk_name,@$photo->file_name)}}">
                    		</div>
                    		@endforeach
                    		</div>
                    	</div>
                    </div>
                </div>  
                </div> 


  @include('partials.errors')

        {!! Form::model($vehicle,['method'=>'post','class'=>'form-horizontal','files' => true,'action'=>['VehiclesController@updateOrder','id'=>$vehicle->id]]) !!}





 <section class="basic-elements">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@lang('main.regions')</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-12 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                 
                                          {!! Form::label('order',trans('main.order'), ['class' => 'col-md-12  '.trans('main.style.pull').' control-label']) !!}
                                        {!!  Form::number('order',old('order'),['id'=>"name",'class'=>"form-control",'autofocus'=>'true']) !!}

                                </fieldset>
                            </div>

                             





                         

                        </div>
                        <div class="form-group overflow-hidden">
                                <div class="col-12">
                                    <button data-repeater-create="" class="btn btn-primary btn-lg">
                                        <i class="icon-plus4"></i>  @lang('main.edit_order')
                                    </button>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>





     {!! Form::close() !!}
	</div>
</div>

	@endsection


@section('js')

@include('vehicles.js.show.remove_photo')
@include('vehicles.js.show.add_photos_for_vehicle')
@include('vehicles.js.show.stared')
@endsection
