<div class="row">



<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 mb-1">@lang('main.year_of_product')</div>
	<div class="col-xl-6 col-lg-6 col-md-6 mb-1">
        <fieldset class="form-group">
            {!! Form::label('year_of_product_min',trans('main.from'), ['class' => '  control-label']) !!}
            {!! Form::number('year_of_product_min',Request::get('year_of_product_min'),['class'=>'form-control ','id'=>'year_of_product_min']) !!}
        </fieldset>
	</div>

	<div class="col-xl-6 col-lg-6 col-md-6 mb-1">
        <fieldset class="form-group">
            {!! Form::label('year_of_product_max',trans('main.to'), ['class' => '   control-label']) !!}
            {!! Form::number('year_of_product_max',Request::get('year_of_product_max'),['class'=>'form-control ','id'=>'year_of_product_max']) !!}
        </fieldset>
	</div>
</div>

<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 mb-1">@lang('main.year_of_work')</div>
	<div class="col-xl-6 col-lg-6 col-md-6 mb-1">
        <fieldset class="form-group">
            {!! Form::label('year_of_work_min',trans('main.from'), ['class' => '  control-label']) !!}
            {!! Form::number('year_of_work_min',Request::get('year_of_work_min'),['class'=>'form-control ','id'=>'year_of_work_min']) !!}
        </fieldset>
	</div>

	<div class="col-xl-6 col-lg-6 col-md-6 mb-1">
        <fieldset class="form-group">
            {!! Form::label('year_of_work_max',trans('main.to'), ['class' => '   control-label']) !!}
            {!! Form::number('year_of_work_max',Request::get('year_of_work_max'),['class'=>'form-control ','id'=>'year_of_work_max']) !!}
        </fieldset>
	</div>
</div>


<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 mb-1">@lang('main.price')</div>
	<div class="col-xl-6 col-lg-6 col-md-6 mb-1">
        <fieldset class="form-group">
            {!! Form::label('price_min',trans('main.from'), ['class' => '  control-label']) !!}
            {!! Form::number('price_min',Request::get('price_min'),['class'=>'form-control ','id'=>'price_min']) !!}
        </fieldset>
	</div>

	<div class="col-xl-6 col-lg-6 col-md-6 mb-1">
        <fieldset class="form-group">
            {!! Form::label('price_max',trans('main.to'), ['class' => '   control-label']) !!}
            {!! Form::number('price_max',Request::get('price_max'),['class'=>'form-control ','id'=>'price_max']) !!}
        </fieldset>
	</div>
</div>



<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 mb-1">@lang('main.power')</div>
	<div class="col-xl-6 col-lg-6 col-md-6 mb-1">
        <fieldset class="form-group">
            {!! Form::label('power_min',trans('main.from'), ['class' => '  control-label']) !!}
            {!! Form::number('power_min',Request::get('power_min'),['class'=>'form-control ','id'=>'power_min']) !!}
        </fieldset>
	</div>

	<div class="col-xl-6 col-lg-6 col-md-6 mb-1">
        <fieldset class="form-group">
            {!! Form::label('power_max',trans('main.to'), ['class' => '   control-label']) !!}
            {!! Form::number('power_max',Request::get('power_max'),['class'=>'form-control ','id'=>'power_max']) !!}
        </fieldset>
	</div>
</div>

<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 mb-1">@lang('main.hp')</div>
	<div class="col-xl-6 col-lg-6 col-md-6 mb-1">
        <fieldset class="form-group">
            {!! Form::label('hp_min',trans('main.from'), ['class' => '  control-label']) !!}
            {!! Form::number('hp_min',Request::get('hp_min'),['class'=>'form-control ','id'=>'hp_min']) !!}
        </fieldset>
	</div>

	<div class="col-xl-6 col-lg-6 col-md-6 mb-1">
        <fieldset class="form-group">
            {!! Form::label('hp_max',trans('main.to'), ['class' => '   control-label']) !!}
            {!! Form::number('hp_max',Request::get('hp_max'),['class'=>'form-control ','id'=>'hp_max']) !!}
        </fieldset>
	</div>
</div>

<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 mb-1">@lang('main.mileage')</div>
	<div class="col-xl-6 col-lg-6 col-md-6 mb-1">
        <fieldset class="form-group">
            {!! Form::label('mileage_min',trans('main.from'), ['class' => '  control-label']) !!}
            {!! Form::number('mileage_min',Request::get('mileage_min'),['class'=>'form-control ','id'=>'mileage_min']) !!}
        </fieldset>
	</div>

	<div class="col-xl-6 col-lg-6 col-md-6 mb-1">
        <fieldset class="form-group">
            {!! Form::label('mileage_max',trans('main.to'), ['class' => '   control-label']) !!}
            {!! Form::number('mileage_max',Request::get('mileage_max'),['class'=>'form-control ','id'=>'mileage_max']) !!}
        </fieldset>
	</div>
</div>


	<div class="col-xl-12 col-lg-12 col-md-12 mb-1">
        <fieldset class="form-group">
            {!! Form::label('sellers',trans('main.sellers'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
            {!! Form::select('sellers[]',$sellers,Request::get('sellers'),['class'=>'form-control select2tags select2tags','id'=>'sellers','multiple'=>"multiple"]) !!}
        </fieldset>
	</div>

		<div class="col-xl-12 col-lg-12 col-md-12 mb-1">
        <fieldset class="form-group">
            {!! Form::label('make_id',trans('main.makes'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
            {!! Form::select('make_id[]',$makes,Request::get('make_id'),['class'=>'form-control select2tags select2tags','id'=>'make_id','multiple'=>"multiple"]) !!}
        </fieldset>
	</div>

	<div class="col-xl-12 col-lg-12 col-md-12 mb-1">
        <fieldset class="form-group">
            {!! Form::label('mould_id',trans('main.moulds'), ['class' => '  '.trans('main.style.pull').' control-label','multiple'=>"multiple"]) !!}
            {!! Form::select('mould_id[]',$moulds,Request::get('mould_id'),['class'=>'form-control select2tags','id'=>'mould_id','multiple'=>"multiple"]) !!}
        </fieldset>
	</div>

	<div class="col-xl-12 col-lg-12 col-md-12 mb-1">
        <fieldset class="form-group">
            {!! Form::label('body',trans('main.body'), ['class' => '  '.trans('main.style.pull').' control-label','multiple'=>"multiple"]) !!}
            {!! Form::select('body[]',$body,Request::get('body'),['class'=>'form-control select2tags','id'=>'body','multiple'=>"multiple"]) !!}
        </fieldset>
	</div>

	<div class="col-xl-12 col-lg-12 col-md-12 mb-1">
        <fieldset class="form-group">
            {!! Form::label('gear',trans('main.gear'), ['class' => '  '.trans('main.style.pull').' control-label','multiple'=>"multiple"]) !!}
            {!! Form::select('gear[]',$gear,Request::get('gear'),['class'=>'form-control select2tags','id'=>'gear','multiple'=>"multiple"]) !!}
        </fieldset>
	</div>

	<div class="col-xl-12 col-lg-12 col-md-12 mb-1">
        <fieldset class="form-group">
            {!! Form::label('fuel',trans('main.fuel'), ['class' => '  '.trans('main.style.pull').' control-label','multiple'=>"multiple"]) !!}
            {!! Form::select('fuel[]',$fuel,Request::get('fuel'),['class'=>'form-control select2tags','id'=>'fuel','multiple'=>"multiple"]) !!}
        </fieldset>
	</div>
	<div class="col-xl-12 col-lg-12 col-md-12 mb-1">
        <fieldset class="form-group">
            {!! Form::label('drivetrain_system',trans('main.drivetrain_system'), ['class' => '  '.trans('main.style.pull').' control-label','multiple'=>"multiple"]) !!}
            {!! Form::select('drivetrain_system[]',$drivetrain_system,Request::get('drivetrain_system'),['class'=>'form-control select2tags','id'=>'drivetrain_system','multiple'=>"multiple"]) !!}
        </fieldset>
	</div>
	<div class="col-xl-12 col-lg-12 col-md-12 mb-1">
        <fieldset class="form-group">
            {!! Form::label('drivetrain_type',trans('main.drivetrain_type'), ['class' => '  '.trans('main.style.pull').' control-label','multiple'=>"multiple"]) !!}
            {!! Form::select('drivetrain_type[]',$drivetrain_type,Request::get('drivetrain_type'),['class'=>'form-control select2tags','id'=>'drivetrain_type','multiple'=>"multiple"]) !!}
        </fieldset>
	</div>




    <div class="col-xl-12 col-lg-12 col-md-12 mb-1">
        <fieldset class="form-group">
            {!! Form::label('num_of_seats',trans('main.num_of_seats'), ['class' => '  '.trans('main.style.pull').' control-label','multiple'=>"multiple"]) !!}
            {!! Form::select('num_of_seats[]',$num_of_seats,Request::get('num_of_seats'), ['id'=>"num_of_seats",'class'=>"form-control select2tags",'multiple'=>"multiple"]) !!}
        </fieldset>
    </div>

     <div class="col-xl-12 col-lg-12 col-md-12 mb-1">
        <fieldset class="form-group">
            {!! Form::label('vehicle_status',trans('main.vehicle_status'), ['class' => '  '.trans('main.style.pull').' control-label','multiple'=>"multiple"]) !!}
            {!! Form::select('vehicle_status[]',$vehicle_status,Request::get('vehicle_status'), ['id'=>"vehicle_status",'class'=>"form-control select2tags",'multiple'=>"multiple"]) !!}
        </fieldset>
    </div>

    <div class="col-xl-12 col-lg-12 col-md-12 mb-1">
        <fieldset class="form-group">
            {!! Form::label('body_color',trans('main.body_color'), ['class' => '  '.trans('main.style.pull').' control-label','multiple'=>"multiple"]) !!}
            {!! Form::select('body_color[]',$body_color,Request::get('body_color'), ['id'=>"body_color",'class'=>"form-control select2tags",'multiple'=>"multiple"]) !!}
        </fieldset>
    </div>
    <div class="col-xl-12 col-lg-12 col-md-12 mb-1">
        <fieldset class="form-group">
            {!! Form::label('interior_color',trans('main.interior_color'), ['class' => '  '.trans('main.style.pull').' control-label','multiple'=>"multiple"]) !!}
            {!! Form::select('interior_color[]',$interior_color,Request::get('interior_color'), ['id'=>"interior_color",'class'=>"form-control select2tags",'multiple'=>"multiple"]) !!}
        </fieldset>
    </div>

    <div class="col-xl-12 col-lg-12 col-md-12 mb-1">
        <fieldset class="form-group">
            {!! Form::label('customs_exemption',trans('main.customs_exemption'), ['class' => '  '.trans('main.style.pull').' control-label','multiple'=>"multiple"]) !!}
            {!! Form::select('customs_exemption[]',$customs_exemption,Request::get('customs_exemption'), ['id'=>"customs_exemption",'class'=>"form-control select2tags",'multiple'=>"multiple"]) !!}
        </fieldset>
    </div>

   <div class="col-xl-12 col-lg-12 col-md-12 mb-1">
        <fieldset class="form-group">
            {!! Form::label('num_of_doors',trans('main.num_of_doors'), ['class' => '  '.trans('main.style.pull').' control-label','multiple'=>"multiple"]) !!}
            {!! Form::select('num_of_doors[]',$num_of_doors,Request::get('num_of_doors'), ['id'=>"num_of_doors",'class'=>"form-control select2tags",'multiple'=>"multiple"]) !!}
        </fieldset>
    </div>

    <div class="col-xl-12 col-lg-12 col-md-12 mb-1">
        <fieldset class="form-group">
            {!! Form::label('previous_owners',trans('main.previous_owners'), ['class' => '  '.trans('main.style.pull').' control-label','multiple'=>"multiple"]) !!}
            {!! Form::select('previous_owners[]',$previous_owners,Request::get('previous_owners'), ['id'=>"previous_owners",'class'=>"form-control select2tags",'multiple'=>"multiple"]) !!}
        </fieldset>
    </div>
    <div class="col-xl-12 col-lg-12 col-md-12 mb-1">
        <fieldset class="form-group">
            {!! Form::label('origin',trans('main.origin'), ['class' => '  '.trans('main.style.pull').' control-label','multiple'=>"multiple"]) !!}
            {!! Form::select('origin[]',$origin,Request::get('origin'), ['id'=>"origin",'class'=>"form-control select2tags",'multiple'=>"multiple"]) !!}
        </fieldset>
    </div>
     <div class="col-xl-12 col-lg-12 col-md-12 mb-1">
        <fieldset class="form-group">
            {!! Form::label('num_of_keys',trans('main.num_of_keys'), ['class' => '  '.trans('main.style.pull').' control-label','multiple'=>"multiple"]) !!}
            {!! Form::select('num_of_keys[]',$num_of_keys,Request::get('num_of_keys'), ['id'=>"num_of_keys",'class'=>"form-control select2tags",'multiple'=>"multiple"]) !!}
        </fieldset>
    </div>
    <div class="col-xl-12 col-lg-12 col-md-12 mb-1">
        <fieldset class="form-group">
            {!! Form::label('is_deleted',trans('main.is_deleted'), ['class' => '  '.trans('main.style.pull').' control-label','multiple'=>"multiple"]) !!}
            {!! Form::select('is_deleted[]',$yes_no,Request::get('is_deleted'), ['id'=>"is_deleted",'class'=>"form-control select2tags",'multiple'=>"multiple"]) !!}
        </fieldset>
    </div>
     <div class="col-xl-12 col-lg-12 col-md-12 mb-1">
        <fieldset class="form-group">
            {!! Form::label('publish_status',trans('main.publish_status'), ['class' => '  '.trans('main.style.pull').' control-label','multiple'=>"multiple"]) !!}
            {!! Form::select('publish_status[]',$publish_status,Request::get('publish_status'), ['id'=>"publish_status",'class'=>"form-control select2tags",'multiple'=>"multiple"]) !!}
        </fieldset>
    </div>
    <div class="col-xl-12 col-lg-12 col-md-12 mb-1">
        <fieldset class="form-group">
            {!! Form::label('stared',trans('main.stared'), ['class' => '  '.trans('main.style.pull').' control-label','multiple'=>"multiple"]) !!}
            {!! Form::select('stared[]',$yes_no,Request::get('stared'), ['id'=>"stared",'class'=>"form-control select2tags",'multiple'=>"multiple"]) !!}
        </fieldset>
    </div>


     <div class="col-xl-12 col-lg-12 col-md-12 mb-1">
        <fieldset class="form-group">
            {!! Form::label('equipments',trans('main.equipments'), ['class' => '  '.trans('main.style.pull').' control-label','multiple'=>"multiple"]) !!}
            {!! Form::select('equipments[]',$equipments,Request::get('equipments'), ['id'=>"equipments",'class'=>"form-control select2tags",'multiple'=>"multiple"]) !!}
        </fieldset>
    </div>

     <div class="col-xl-12 col-lg-12 col-md-12 mb-1">
        <fieldset class="form-group">
            {!! Form::label('slider',trans('main.slider'), ['class' => '  '.trans('main.style.pull').' control-label']) !!}
            {!! Form::select('slider',$yes_no,Request::get('slider'), ['id'=>"slider",'class'=>"form-control select2tags",'multiple'=>"multiple"]) !!}
        </fieldset>

    </div>







</div>