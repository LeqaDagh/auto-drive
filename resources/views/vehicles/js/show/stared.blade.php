<script type="text/javascript">
	
	  $( document ).delegate( ".make_as_stared", "click", function() {

	  	 // get transaction id from div
         var vehicle_id =$(this).attr('vehicle_id');
  var c = confirm("@lang('main.confirm_action')");
if(c==true)
{

         //call by ajax
         $.post( '{{url("vehicles/do/make_stared")}}', {vehicle_id:vehicle_id})
          .done(function( data ) {


                   document.location ="";
             

            }); 
}
return false;

	  });


	  $( document ).delegate( ".make_as_unstared", "click", function() {

	  	 // get transaction id from div
         var vehicle_id =$(this).attr('vehicle_id');
  var c = confirm("@lang('main.confirm_action')");
if(c==true)
{

         //call by ajax
         $.post( '{{url("vehicles/do/make_unstared")}}', {vehicle_id:vehicle_id})
          .done(function( data ) {


                   document.location ="";
             

            }); 
}

return false;
	  });


  $( document ).delegate( ".delete_vehicle", "click", function() {

      // get transaction id from div
      var vehicle_id =$(this).attr('vehicle_id');
      var c = confirm("@lang('main.confirm_action')");
      if(c==true)
      {
        //call by ajax
        $.post( '{{url("vehicles/do/delete_vehicle")}}', {vehicle_id:vehicle_id})
        .done(function( data ) {
             document.location ="";
        }); 
      }

      return false;
  });

  $( document ).delegate( ".restore_vehicle", "click", function() {

      // get transaction id from div
      var vehicle_id =$(this).attr('vehicle_id');
      var c = confirm("@lang('main.confirm_action')");
      if(c==true)
      {
        //call by ajax
        $.post( '{{url("vehicles/do/restore_vehicle")}}', {vehicle_id:vehicle_id})
        .done(function( data ) {
             document.location ="";
        }); 
      }

      return false;
  });

    $( document ).delegate( ".publish_vehicle", "click", function() {

      // get transaction id from div
      var vehicle_id =$(this).attr('vehicle_id');
      var c = confirm("@lang('main.confirm_action')");
      if(c==true)
      {
        //call by ajax
        $.post( '{{url("vehicles/do/publish_vehicle")}}', {vehicle_id:vehicle_id})
        .done(function( data ) {
             document.location ="";
        }); 
      }

      return false;
  });
    $( document ).delegate( ".unpublish_vehicle", "click", function() {

      // get transaction id from div
      var vehicle_id =$(this).attr('vehicle_id');
      var c = confirm("@lang('main.confirm_action')");
      if(c==true)
      {
        //call by ajax
        $.post( '{{url("vehicles/do/unpublish_vehicle")}}', {vehicle_id:vehicle_id})
        .done(function( data ) {
             document.location ="";
        }); 
      }

      return false;
  });

    
</script>