<script type="text/javascript">
	
	  $( document ).delegate( ".remove_photo", "click", function() {

	  	 // get transaction id from div
         var media_id =$(this).attr('media_id');
  var c = confirm("@lang('main.confirm_remove_photo')");
if(c==true)
{

         //call by ajax
         $.post( '{{url("vehicles/remove_photo")}}', {media_id:media_id})
          .done(function( data ) {


                   document.location ="";
             

            }); 
}


	  });



    $( document ).delegate( ".set_as_slider", "click", function() {

       // get transaction id from div
         var media_id =$(this).attr('media_id');
         var vehicle_id =$(this).attr('vehicle_id');
  var c = confirm("@lang('main.confirm_set_slider')");
if(c==true)
{

         //call by ajax
         $.post( '{{url("vehicles/set_as_slider")}}', {media_id:media_id,vehicle_id:vehicle_id})
          .done(function( data ) {


                   document.location ="";
             

            }); 
}


    });


    $( document ).delegate( ".delete_as_slider", "click", function() {

       // get transaction id from div
         var media_id =$(this).attr('media_id');
         var vehicle_id =$(this).attr('vehicle_id');
  var c = confirm("@lang('main.confirm_delete_slider')");
if(c==true)
{

         //call by ajax
         $.post( '{{url("vehicles/delete_as_slider")}}', {media_id:media_id,vehicle_id:vehicle_id})
          .done(function( data ) {


                   document.location ="";
             

            }); 
}


    });


</script>