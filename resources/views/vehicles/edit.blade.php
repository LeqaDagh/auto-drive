@extends('layouts.app')

@section('content')
  @include('partials.breadcrumbs', ['method'=>['name'=>trans('main.vehicles'),'url'=>url('vehicles')], 'action' =>trans('main.edit')])


  @include('partials.errors')
      
        {!! Form::model($vehicle,['method'=>'PATCH','class'=>'form-horizontal','files' => true,'action'=>['VehiclesController@update',$vehicle->id]]) !!}
       @include('vehicles.partials.form',['btn' =>trans('main.edit'), 'form' =>'editing'])
     {!! Form::close() !!}

@endsection


