<script type="text/javascript">
	


     var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat:31.74327487274828, lng: 35.258345627343715 },
          zoom: 9
        });

         var marker = new google.maps.Marker({
          position: map.center,
          map: map,
          draggable:true
        });



    google.maps.event.addListener(marker, 'dragend', function (evt) {
       document.getElementById('current').innerHTML = '<p> احداثي طول: ' + evt.latLng.lat().toFixed(3) + '  احداثي عرض: ' + evt.latLng.lng().toFixed(3) + '</p>';
         document.getElementById('lat').value = evt.latLng.lat();
         document.getElementById('lon').value = evt.latLng.lng();
    });

    google.maps.event.addListener(marker, 'dragstart', function (evt) {
        document.getElementById('current').innerHTML = '<p>...</p>';
    });

      }



$(document).on('show.bs.modal','#add_seller_for_client_modal', function () {
  initMap();
})

	  $( document ).delegate( ".add_seller_for_client", "click", function() {

	  	 // get transaction id from div
         var client_id =$(this).attr('client_id');

         //call by ajax
         $.post( '{{url("clients/add_seller_for_client")}}', {client_id:client_id})
          .done(function( data ) {


                          //remove model form html dom if exist
                $('#add_seller_for_client_modal').remove();

                //append model to html dom
                $( "body").append( data );

                //popup a model
                $('#add_seller_for_client_modal').modal();


             

            }); 

	  });

$( document ).delegate( ".add_seller_for_client_form", "submit", function() {




var formData = new FormData(this);
 
 
 
$.ajax({
 
   type:'POST',
 
   url: '{{url("clients/add_seller_for_client_post")}}',
 
   data:formData,
 
   cache:false,
 
   contentType: false,
 
   processData: false,
 
   success:function(data){
 
       document.location ="";
 
   },
 
   error: function(xhr){
 


             if( xhr.status === 401 ) 
              { 
                document.location= "{{url('login')}}";
              }
              else if( xhr.status === 422 ) 
              {

            
                let errors = xhr.responseJSON;
                let errorsHtml ='<ul>';

                   $.each( errors.errors , function( key, value ) {
                      errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                  });
                    errorsHtml+='</ul>';
                    $('.add_seller_for_client_errors').removeClass('d-none');
                    $('.add_seller_for_client_errors').html(errorsHtml);

               }
               else
               {
                   $('.add_seller_for_client_errors').removeClass('d-none');
                   $('.add_seller_for_client_errors').html(error);
               }
 
   }
 
});


          return false;

	  });



</script>