@extends('layouts.app')

@section('content')
<div >
  


@include('partials.breadcrumbs', ['method' =>['name'=>trans('main.clients'),'url'=>url('clients')], 'action' =>trans('main.view')])



@include('partials.search_form',['name'=>trans('main.search'),'url'=>'clients','blade'=>'clients.partials.search_form'])


<div class="card">
        @include('partials.card_header', ['title' =>trans('main.clients')])
            <div class="card-content collapse show">
                <div class="card-body">
                    <p class="card-text">


           <p><br><i><code><span>عدد النتائج : </span><span>{{ $clients->total()}}</span></code></i></p>
          </p>
           
                </div>
               
                    @if (count($clients))
 <div class="table-responsive">
<table class="table table-striped " >
  <thead>
  <tr>
    <th>{{trans('main.id')}}</th>
    <th>{{trans('main.name')}}</th>
    @can('show_add_by_client')<th>{{trans('main.added_by')}}</th>@endcan
    <th>{{trans('main.region')}}</th>
    <th>{{trans('main.mobile')}}</th>
    <th>{{trans('main.status')}}</th>
    <th class="text-center">{{trans('main.options')}}</th>

    @can('show_clients')
    <th >{{trans('main.show')}}</th>
    @endcan

  </tr>
</thead>
<tbody>
  @foreach ($clients as $user)
  <tr>
    <th scope="row">{{$user->id}}</th>
    <td>{!!$user->connectStatus()!!} {{$user->name}} </td>
    @can('show_add_by_client')<td>{{@$user->parent->name}} </td>@endcan
    <td>{{@$user->region->name}}</td>
    <td> {{$user->mobile}}</td>
    <td> {{trans('main.yes_no.'.$user->user_status)}}</td>
    <td class="text-center">

    @can('edit_clients')
    <a href="{{url('clients',$user->id)}}/edit"><i class="fas fa-edit"></i></a>
    @endcan
    
    @can('destroy_clients')
    <i class="fas fa-trash-alt remove_client"  client_id="{{$user->id}}"></i>
    @endcan

    </td>

    @can('show_clients')
    <td><a href="{{url('clients',$user->id)}}">{{trans('main.show')}}</a></td>
    @endcan
  </tr>
      
    @endforeach
  </tbody>
</table>


{{$clients->appends(request()->query())}}


</div>
  @endif
                </div>
            </div>
        </div>






  
@endsection

@section('js')
  <script type="text/javascript">
        $( document ).delegate( ".remove_client", "click", function() {

       // get transaction id from div
         var client_id =$(this).attr('client_id');
  var c = confirm("@lang('main.confirm_remove_client')");
if(c==true)
{

$.ajax({
    url: '{{url("")}}/clients/'+client_id,
    method: 'DELETE',
    contentType: 'application/json',
    success: function(result) {
          document.location ="";
    },
    error: function(request,msg,error) {
        // handle failure
    }
});

   
}


    });
  </script>
@endsection



