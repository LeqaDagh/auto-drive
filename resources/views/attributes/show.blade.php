@extends('layouts.app')

@section('content')

  @include('partials.breadcrumbs', ['method' =>['name'=>trans('main.attributes'),'url'=>url('attributes')], 'action' =>$attribute->label])

<h1>{{$attribute->name}}</h1>

	@endsection
