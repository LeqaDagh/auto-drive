@extends('layouts.app')

@section('content')
  @include('partials.breadcrumbs', ['method'=>['name'=>trans('main.attributes'),'url'=>url('attributes')], 'action' =>trans('main.edit')])


  @include('partials.errors')
      
        {!! Form::model($attribute,['method'=>'PATCH','class'=>'form-horizontal','files' => true,'action'=>['AttributesController@update',$attribute->id]]) !!}
       @include('attributes.partials.form',['btn' =>trans('main.edit'), 'form' =>'editing'])
     {!! Form::close() !!}

@endsection


