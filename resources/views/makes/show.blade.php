@extends('layouts.app')

@section('content')

@include('partials.breadcrumbs', ['method' =>['name'=>trans('main.makes'),'url'=>url('makes')], 'action' =>$make->name])

<h1>{{$make->name}}</h1>

                    @if (count($make->moulds))

<table class="table table-striped " >
	<thead>
	<tr>
		<th>{{trans('main.id')}}</th>
		<th>{{trans('main.name')}}</th>
		<th>{{trans('main.parent')}}</th>
		<th>{{trans('main.make')}}</th>
		<th>{{trans('main.logo')}}</th>
		<th>{{trans('main.order')}}</th>
		@can('show_moulds')
		<th >{{trans('main.show')}}</th>
		@endcan

	</tr>
</thead>
<tbody>
	@foreach ($make->moulds as $mould)
	<tr>
		<th scope="row">{{$mould->id}}</th>
		<td>{{$mould->name}}</td>
		<td>{{@$mould->father->name}}</td>
		<td>{{@$mould->make->name}}</td>
		<td>{{$mould->logo}}</td>
		<td>{{$mould->order}}</td>
		@can('show_moulds')
		<td><a href="{{url('moulds',$mould->id)}}">{{trans('main.show')}}</a></td>
		@endcan
	</tr>
			
		@endforeach
	</tbody>
</table>


	@endif

@endsection