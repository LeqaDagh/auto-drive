@extends('layouts.app')

@section('content')

  @include('partials.breadcrumbs', ['method' =>['name'=>trans('main.medias'),'url'=>url('medias')], 'action' =>$media->label])

<h1><div>{{$media->name}}</h1>

<div class="col-md-3" > 
		<div style="border: 1px solid #aaa;margin: 5px;border-radius: 5px;">
	<img style="width: 100%;height: 200px;border-radius: 5px;" src="{{url('public/storage/'.@$media->disk_name,@$media->file_name)}}">
		<div class="text-center">
				<h3><div>{{$media->title}}</h3>
				<div>{{$media->file_name}}</div>
				<div>{{$media->file_size}}</div>
				<div>{{$media->content_type}}</div>
				<div>{{$media->description}}</div>
				<div>{{$media->field}}</div>
				<div>{{$media->fileable_id}}</div>
				<div>{{$media->fileable_type}}</div>
				<div>{{$media->sort_order}}</div>
				<div>{{$media->created_at}}</div>

		</div>
		</div>
	</div>

	@endsection
