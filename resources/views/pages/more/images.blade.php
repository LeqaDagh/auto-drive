@extends('layouts.apps')

@section('content')

<div id="imagesForVehicle" class="row" style="display:block"> 
    <div class="col-md-12">
        <form class="form" enctype="multipart/form-data" method="POST" id="upload-images-vehicle-edit-form"
            action="{{ route ('media.images') }}" >
        @csrf
            <div class="">
                <div class=" ">
                    <center>
                        <h5>
                        التحكم بالصور
                        </h5>
                    </center>
                </div>
                <div class="card-body ">
                    <input type="file" name="photos[]" id="ImagesVehicleUploadEdit" style="display: none"
                        class="text-center form-control" multiple="multiple">
                    <input type="text" id="vehicleId" name="vehcile_id" value="{{$id}}" hidden >
                    <div class="" id="choosenImagesDiv" style="display:none">
                    </br>

                    <div class="row">
                        <div class="col-lg-6 ml-auto mr-auto">
                            <center>
                            <div class="card"></br>
                            <center><h5> الصور المختارة</h5></center>
                                <div class="card-body ">
                                    <div id="myImages" > 
                                    </div>
                                </div>  
                            </div>    
                        </div>      
                    </div> 
                             
                    </div> </br>

                    <div class="row">
                        <div class="col-lg-6 ml-auto mr-auto">
                            <center>
                                <div class="card">
                                </br>
                                    <center><h5> صور المركبة</h5></center>
                                
                                    <div class="card-body ">
                                    @foreach($collection as $key => $data)
                                        <div class="text-center" >           
                                            <input id="" value="" style="display:none" disabled>
                                            <div class="control-group text-center d-flex flex-row">
                                                <div class=" col-lg-8 col-sm-8 "> 
                                                    <div id="picturebox" class=" ">
                                                        <img src="{{strstr($data->thumb_path, '/storage/')}}" 
                                                        class="img-responsive" style="width:100%; height:70px; object-fit: contain">
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-sm-4" id="imagebuttonadd" >
                                                    <div class="input-group-btn"> </br>
                                                        <a class="btn remove-image"  href="{{ route('media.destroy', $data->id) }}">
                                                            <i class="fa fa-minus-circle" style="color:#0875ba; font-size:1.1em;"></i> 
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    </div>    
                                          
                                </div> </br>
                            </center>
                        </div> 
                    </div>

                    <div class="container"> 
                        <div class="text-center"> 
                            <button class="btn btn-primary add-multiple-images-edit" type="button" >اختر الصور
                            </button>
                            <button id="upload-multiple-images-edit" class="btn btn-danger upload-multiple-images-edit" 
                            type="submit" > رفع
                            </button>
                        </div> 
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<script>
    $(document).ready(function() {
        $("#upload-multiple-images-edit").fadeOut(200);
        var fileupload = $("#ImagesVehicleUploadEdit");
        var count = 0;

        $(".add-multiple-images-edit").click(function() { 
            fileupload.click();

        });

        fileupload.change(function () {
            readURL(this);
        });

        function readURL(input) {
            var filesAmount = input.files.length;

            if (input.files ) {
                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();
                    
                    reader.onload = function (e) {

                        $('#myImages').append('<div class="control-group text-center d-flex flex-row"><div class=" col-lg-8 col-sm-8 "> '
                        + '<div id="picturebox"class="card-img-top "><img src=' + e.target.result + ' style="width:100%; height:70px; object-fit: contain">'
                        + '</div></div><div class="col-lg-4 col-sm-4" id="imagebuttonadd" >'
                        + '<div class="input-group-btn"></br> '
                        + '<button class="btn remove-image" type="button"><i class="fa fa-minus-circle" style="color:#0875ba; font-size:1.1em;"></i> </button>'
                        + '</div></div></div>');
                        $("#upload-multiple-images-edit").fadeIn(200);
                        $("#choosenImagesDiv").fadeIn(200);
                    }
                    reader.readAsDataURL(input.files[i]);
                }

            }
            
            
        }

        $('#myImages').on('click', '.remove-image', function(){
            $(this).parents(".control-group").remove();
        });
        
        $('#upload-multiple-images-edit').on('click', function(event){
            var data =  $('#upload-images-vehicle-edit-form').serialize();
            console.log(data);
                
        });

    });
</script>
@endsection