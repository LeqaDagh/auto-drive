@extends('layouts.apps')

@section('content')
    <div class="row"> 
        <div class="col-lg-12 col-md-12 col-sm-12">
            <center>
                <h5>
                تعديل الملف الشخصي
                </h5>
            </center></br>

            <center>
                <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-5 ml-auto mr-auto" >
                        <div class="card p-1 mb-1 bg-white rounded">
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                                        <label for="">عدد المركبات المسموح بالاضافة</label>
                                    </div>  
                                    <div class="col-lg-4 col-md-4 ml-auto mr-auto">
                                        <label style="color: blue">{{$allowedVehicles}}</label>
                                    </div>     
                                </div> 
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                                        <label for="">عدد المركبات المضافة</label>
                                    </div>  
                                    <div class="col-lg-4 col-md-4 ml-auto mr-auto">
                                        <label style="color: blue">{{$vehicleCount}}</label>
                                    </div>     
                                </div> 
                            </div>
                        </div></br>
                    </div>
                </div>
            </center>
        </div>
    </div>     
            
    <div class="row"> 
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10"> 
            <div class="card p-1 mb-1 bg-white rounded">
                <div class="card-body" style="padding: 0.7rem">
                    <form class="form" 
                    method="post" action="{{ route ('profile.update', auth()->user()) }}" >
                    {{ csrf_field() }}
                    {{ method_field('patch') }}
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 ml-auto mr-auto">
                                <center>
                                    <label style="font-size:14px;color:#595959; font-weight: bold;">الاسم</label>
                                </center>
                                <div class="form-group">
                                    <input class="text-center form-control" style="font-size:13px;"
                                    placeholder="الاسم" type="text" value="{{$user->name}}" name="name">
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 ml-auto mr-auto">
                                <center>
                                    <label style="font-size:14px;color:#595959; font-weight: bold;">رقم المحمول</label>
                                </center>
                                <div class="form-group">
                                    <input class="text-center form-control"  name="mobile" style="font-size:13px;"
                                    placeholder="رقم المحمول"  value="{{$user->mobile}}"  type="number">
                            
                                </div>
                            </div> 
                            
                            <div class="col-lg-3 col-md-3 col-sm-3 ml-auto mr-auto">
                                <center>
                                    <label style="font-size:14px;color:#595959; font-weight: bold;">العنوان</label>
                                </center>
                                <div class="form-group">
                                    <input class="text-center form-control"  name="address"style="font-size:13px;"
                                    placeholder="العنوان" value="{{$user->address}}"  type="text">
                                </div> 
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 ml-auto mr-auto">
                                <center>
                                    <label style="font-size:14px;color:#595959; font-weight: bold;">البريد الالكتروني</label>
                                </center>
                                <div class="form-group">
                                    <input class="text-center form-control" value="{{$user->email}}"  name="email" type="email"
                                    placeholder="البريد الالكتروني" style="font-size:13px;">
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 ml-auto mr-auto">
                                <center>
                                    <label style="font-size:14px;color:#595959; font-weight: bold;">المدينة</label>
                                </center>
                                <div class="form-group">
                                    <select name="region_id" class="text-center form-control"style="font-size:13px; text-align-last: center;" >
                                        
                                        <option selected disabled>{{$name[0]->name}}</option>
                                        @foreach($regions as $key => $data)
                                            <option value="{{$data->id}}">{{$data->name}}</option>
                                        @endforeach
                                    </select> 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 ml-auto mr-auto">
                                <center>
                                    <button type="submit" style="font-size:14px;" class="btn btn-primary btn-block" >
                                        <span>تعديل </span>
                                    </button>
                                </center>
                            </div>
                        </div>
                    </form>
                </div>
            </div></br>
            <div class="card p-1 mb-1 bg-white rounded">
                <div class="card-body" style="padding: 0.7rem">
                    <form class="form" method="post" action="{{ route ('profile.password', auth()->user()) }}" >
                    {{ csrf_field() }}
                    {{ method_field('patch') }}
                        <div class="row">
                            
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group">
                                        <input class="text-center form-control"style="font-size:13px;"
                                        placeholder="كلمة المرور الحالية" type="password" name="password">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group">
                                        <input class="text-center form-control"  name="password_confirmation" 
                                        placeholder="كلمة المرور"  type="password" style="font-size:13px;" >
                                    </div> 
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group">
                                        <input class="text-center form-control"  name="password_confirmation" 
                                        placeholder="تأكيد كلمة المرور"  style="font-size:13px;" type="password">
                                    </div> 
                                </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-lg-3 ml-auto mr-auto">
                                <center>
                                    <button type="submit" style="font-size:14px;" class="btn btn-primary btn-block" >
                                        <span>تعديل كلمة المرور </span>
                                    </button>
                                </center>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
    
        </div>
    </div>
@endsection