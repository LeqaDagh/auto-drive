@extends('layouts.apps')


@section('content')
<style>

.cardStyle {padding:0.5rem;border-radius: 10px;background:#0070B0;
    width:70%}
</style>
<div class="row" >
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class=" ">
                <div class="">
                    <center>
                        <h5>
                        المزيد
                        </h5>
                    </center>
                </div> <hr>
                <div class="card-body ">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 ml-auto mr-auto">
                            @auth()
                            <a id="toggleProfile" style="cursor:pointer;">
                                <div class="cardStyle card text-center h-100" >
                                    <div class="card-block">
                                        <h3>
                                        <i  style="color:white" class="material-icons" >person</i>
                                        </h3>
                                    </div>
                                    <h6 style="font-size:14px;color:white" class="card-title">حسابي</h6>
                                </div>
                            </a>
                            @endauth

                            @guest
                                <a href="{{ route('login') }}">
                                    <div class="cardStyle card text-center h-100" >
                                        <div class="card-block">
                                            <h3><i style="color:white"  class="material-icons" >transit_enterexit</i></h3>
                                        </div>
                                        <h6 style="font-size:14px;color:white"  class="card-title">تسجيل دخول</h6>
                                    </div>
                                </a>
                            @endguest
                        </div>  
                        <div class="col-lg-2 col-md-2 ml-auto mr-auto">
                            <a href="{{ route('all-vehicle.index') }}">
                                <div class="cardStyle card text-center h-100">
                                    <div class="card-block">
                                        <h3><i style="color:white" class="material-icons" >commute</i></h3>
                                    </div>
                                    <h6 style="font-size:14px;color:white" class="card-title">المعارض</h6>
                                </div>
                            </a>
                        </div>  
                        <div class="col-lg-2 col-md-2 ml-auto mr-auto">
                            <a href="{{ route('contact.index') }}">
                                <div class="cardStyle card text-center h-100">
                                    <div class="card-block">
                                        <h3><i style="color:white" class="material-icons" >
                                        contact_phone
                                        </i></h3>
                                    </div>
                                    <h6 style="font-size:14px;color:white" class="card-title">اٍتصل بنا</h6>
                                </div>
                            </a>
                        </div> 
                        <div class="col-lg-2 col-md-2 ml-auto mr-auto">
                            <a href="{{ route('about-us.index') }}">
                                <div class="cardStyle card text-center h-100">
                                    <div class="card-block">
                                        <h3><i style="color:white" class="material-icons"  >info</i></h3>
                                    </div>
                                    <h6 style="font-size:14px;color:white" class="card-title">من نحن</h6>
                                </div>
                            </a>
                        </div> 
                        <div class="col-lg-2 col-md-2 ml-auto mr-auto">
                            <a href="{{ route('used.index') }}">
                                <div class="cardStyle card text-center h-100">
                                    <div class="card-block">
                                        <h3><i style="color:white" id="toggleProfile" class="material-icons"  >
                                        lock</i></h3>
                                    </div>
                                    <h6 style="font-size:14px;color:white" class="card-title"> اٍتفاقية الاٍستخدام</h6>
                                </div>
                            </a>
                        </div> 
                    </div> </br>
                 
                </div>
                
            </div>
        
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>

@endsection
