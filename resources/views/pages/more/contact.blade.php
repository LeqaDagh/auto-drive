@extends('layouts.apps')

@section('content')
<style>

.social_case{
width:80px;height:80px;background:#385596;border-radius: 10px;}
.social_lock{position:relative;width:80px;height:80px;margin-right:85px;margin-top:20px;}
.vmk1{
font-size:40px !important;
color:#fff;    
}

.mvm{animation:moving 1s linear infinite alternate;}

.social_case2{
width:80px;height:80px;background:#0070B0;border-radius: 10px;
}

.social_case3{
width:80px;height:80px;background:#5689C4;border-radius: 10px;
}

.social_case4{
width:80px;height:80px;border-radius: 10px;
background: radial-gradient(circle at 30% 107%, #fdf497 0%, #fdf497 5%, #fd5949 45%,#d6249f 60%,#285AEB 90%) !important;
}

.social_case5{
width:80px;height:80px;background:#25d366;border-radius: 10px;
}

.mk{position:absolute;top:50%;left:50%;transform:translate(-50%,-50%);}


</style>
    <div id="" class="row"> 
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <div class=" ">
                <div class="">
                    <center>
                        <h5>
                        اٍتصل بنا
                        </h5>
                    </center>
                </div>
                <center>
                <div class="container">
                    <hr>
                    <div class="row" style="margin:0 auto; width:100%">
                        <a href="{{$faceLink}}">
                            <div class="social_lock">
                                <div class="social_case"></div>
                                <div class="mk"><i class="fa fa-facebook vmk1 "></i></div>
                            </div>
                        </a>
                        <a href="{{$instgramLink}}">
                            <div class="social_lock">
                                <div class="social_case4"></div>
                                <div class="mk"><i class="fa fa-instagram vmk1"></i></div>
                            </div>
                        </a> 
                        <a type="button" id="whatsAppLink" data-id="{{$whatsApp}}">
                            <div class="social_lock">
                                <div class="social_case5"></div>
                                <div class="mk"><i class="fa fa-whatsapp vmk1"></i></div>
                            </div>
                        </a> 
                        <div class="social_lock">
                            <div class="social_case2"></div>
                            <div class="mk">
                                <form action="tel:{{ $mobile }} ">
                                    <button type="submit"><i class="fa fa-phone vmk1 "></i></button>
                                </form>
                            </div>
                        </div>
                        <address>
                            <a href = "mailto: {{$email}}">
                                <div class="social_lock">
                                    <div class="social_case3"></div>
                                    <div class="mk"><img src="{{url('/images/icons/gmail.png')}}" width=45></div>
                                </div>
                            </a>
                        </address>
                    </div>
                </div>
                </center>
            </div>
        
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
        
        </div>
    </div>
@endsection
<script>
    
</script>