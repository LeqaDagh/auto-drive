<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="rtl">
  <head>
     @include('partials.head')
  </head>
  <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">

    @include('partials.nav')
    @include('partials.menu')
    @include('partials.content')
    @include('partials.footer')
    @include('partials.scripts')
    @include('js.csrf')
    @yield('js')

<style type="text/css">
  .delete-item{
    color: red;
  }
</style>
 </body>
</html>