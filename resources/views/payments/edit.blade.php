@extends('layouts.app')

@section('content')
  @include('partials.breadcrumbs', ['method'=>['name'=>trans('main.permissions'),'url'=>url('permissions')], 'action' =>trans('main.edit')])


  @include('partials.errors')
      
        {!! Form::model($permission,['method'=>'PATCH','class'=>'form-horizontal','files' => true,'action'=>['PermissionsController@update',$permission->id]]) !!}
       @include('permissions.partials.form',['btn' =>trans('main.edit'), 'form' =>'editing'])
     {!! Form::close() !!}

@endsection


