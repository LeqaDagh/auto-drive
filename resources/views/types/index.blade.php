@extends('layouts.app')

@section('content')
<div >
	


@include('partials.breadcrumbs', ['method' =>['name'=>trans('main.types'),'url'=>url('types')], 'action' =>trans('main.view')])


<div class="card">
    		@include('partials.card_header', ['title' =>trans('main.types')])
            <div class="card-content collapse show">
                <div class="card-body">
                    <p class="card-text">
					 @can('create_types')
					 <div >
					  <a href="{{url('types','create')}}" class="btn btn-primary">@lang('main.create')</a>
					 </div>
					 @endcan 
					</p>
           
                </div>
                
                    @if (count($types))
                    <div class="table-responsive">

<table class="table table-striped " >
	<thead>
	<tr>
		<th>{{trans('main.id')}}</th>
		<th>{{trans('main.name')}}</th>
		<th>{{trans('main.slug')}}</th>
		<th>{{trans('main.order')}}</th>
		<th>{{trans('main.search_type')}}</th>
		<th>{{trans('main.search_field')}}</th>
		<th>{{trans('main.attributes')}}</th>
		<th>{{trans('main.created_at')}}</th>
		<th class="text-center">{{trans('main.options')}}</th>

		@can('show_types')
		<th >{{trans('main.show')}}</th>
		@endcan

	</tr>
</thead>
<tbody>
	@foreach ($types as $type)
	<tr>
		<th scope="row">{{$type->id}}</th>
		<td>{{$type->name}}</td>
		<td>{{$type->slug }}</td>
		<td>{{$type->order }}</td>
		<td>{{$type->search_type }}</td>
		<td>{{$type->search_field }}</td>
		<td>{{count($type->attributes) }}</td>
		<td>{{$type->created_at}}</td>
		<td class="text-center">

		@can('edit_types')
		<a href="{{url('types',$type->id)}}/edit"><i class="fas fa-edit"></i></a>
		@endcan
		
		@can('destroy_groups')
		<i class="fas fa-trash-alt delete-item"  delete_item_id="{{$type->id}}"></i>
		@endcan

		</td>

		@can('show_types')
		<td><a href="{{url('types',$type->id)}}">{{trans('main.show')}}</a></td>
		@endcan
	</tr>
			
		@endforeach
	</tbody>
</table>

{{$types->appends(request()->query())}}
</div>
	@endif
                </div>
            </div>
        </div>






	
@endsection


