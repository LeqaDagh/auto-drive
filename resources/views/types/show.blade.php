@extends('layouts.app')

@section('content')

  @include('partials.breadcrumbs', ['method' =>['name'=>trans('main.types'),'url'=>url('types')], 'action' =>$type->label])

	<h1>{{$type->name}}</h1>

	<ul>
		@foreach($type->attributes as $attribute)
			<li>{{$attribute->id}},{{$attribute->order}},{{$attribute->name}},{{@$attribute->father->name}}</li>
		@endforeach
	</ul>

	@endsection
