

 {!! Form::hidden('form', $form) !!}

 <section class="basic-elements">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@lang('main.types')</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-4 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                 
                                          {!! Form::label('name',trans('main.name'), ['class' => 'col-md-4  '.trans('main.style.pull').' control-label']) !!}
                                        {!!  Form::text('name',old('name'),['id'=>"name",'class'=>"form-control",'required'=>'true','autofocus'=>'true']) !!}

                                </fieldset>
                            </div>

                              <div class="col-xl-4 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                 
                                          {!! Form::label('logo',trans('main.logo'), ['class' => 'col-md-4  '.trans('main.style.pull').' control-label']) !!}
                                        {!!  Form::text('logo',old('logo'),['id'=>"logo",'class'=>"form-control",'required'=>'true']) !!}

                                </fieldset>
                            </div>

                             <div class="col-xl-4 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                 
                                          {!! Form::label('order',trans('main.order'), ['class' => 'col-md-4  '.trans('main.style.pull').' control-label']) !!}
                                        {!!  Form::number('order',old('order'),['id'=>"order",'class'=>"form-control",'required'=>'true']) !!}

                                </fieldset>
                            </div>


                 

                                    <div class="col-xl-4 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                 
                                          {!! Form::label('search_type',trans('main.search_type'), ['class' => 'col-md-4  '.trans('main.style.pull').' control-label']) !!}
                                         {!! Form::select('search_type',$search_types,null,['class'=>'form-control select2l']) !!}

                                </fieldset>
                            </div>


                               <div class="col-xl-4 col-lg-6 col-md-12 mb-1">
                                <fieldset class="form-group">
                                 
                                          {!! Form::label('search_field',trans('main.search_field'), ['class' => 'col-md-4  '.trans('main.style.pull').' control-label']) !!}
                                        {!!  Form::text('search_field',old('search_field'),['id'=>"search_field",'class'=>"form-control",'required'=>'true']) !!}

                                </fieldset>
                            </div>

                     
                         

                        </div>
                        <div class="form-group overflow-hidden">
                                <div class="col-12">
                                    <button data-repeater-create="" class="btn btn-primary btn-lg">
                                        <i class="icon-plus4"></i>  {{$btn}}
                                    </button>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



         


