<!DOCTYPE html>
<html>
<head>
  <title>support</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

</head>
<body>
  <div class="alert alert-info ">As a precautionary health measure for our support specialists in light of COVID-19, we're operating with a limited team. If you need help with a product whose support you had trouble reaching over the phone, consult its product-specific Help Center.
</div>
<div class="text-center"><h1>How can we help you?</h1></div>


<div class="card container" >
  <div class="card-body">
    <h5 class="card-title">Help</h5>
    
    <div class="">
  <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item" role="presentation">
    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Android</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">IOS</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Web</a>
  </li>
</ul>
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
    <div class="row">
      <div class="col-lg-3">
        <img src="https://lh3.googleusercontent.com/eCQ3vPk8Fqg62HyRx2mqCxZTAKSfgL0TlVni3rgTy0cmW0jJHBfoUoFEJ95Q-vB5-lk=w720-h310-rw">
      </div>
       <div class="col-lg-3">
        <img src="https://lh3.googleusercontent.com/uTwU1rmO2r0zEfbOhtxM065iXB2ypjmRISywl97tJXp49qCeM-GcpF6DzbMpMFgsqVA=w720-h310-rw">
      </div>
      <div class="col-lg-3">
        <img src="https://lh3.googleusercontent.com/xBqzldFetFVLD31MjsLKUzgHcucz2apOduazlB8CgIlJyQYpM2X_XEOF9HzqXW8EsZ3L=w720-h310-rw">
      </div>
      <div class="col-lg-3">
        <img src="https://lh3.googleusercontent.com/4eTJrSN47RQ8Q0-BN4geP9BTl8voOZqz7SwW9ZRMHwRaeLdsDWWDSrHdZqa9ikeuXkQ=w720-h310-rw">
      </div>
    </div>

    <div class="m-2 text-center"><a href="https://play.google.com/store/apps/details?id=auto.n.drive">
      <img style="height: 40px;" src="https://www.pngmart.com/files/10/Get-It-On-Google-Play-PNG-Clipart.png">
    </a></div>
  </div>
  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
    <div class="row">
     
       <div class="col-lg-3">
        <img src="https://lh3.googleusercontent.com/uTwU1rmO2r0zEfbOhtxM065iXB2ypjmRISywl97tJXp49qCeM-GcpF6DzbMpMFgsqVA=w720-h310-rw">
      </div>
       <div class="col-lg-3">
        <img src="https://lh3.googleusercontent.com/eCQ3vPk8Fqg62HyRx2mqCxZTAKSfgL0TlVni3rgTy0cmW0jJHBfoUoFEJ95Q-vB5-lk=w720-h310-rw">
      </div>
        <div class="col-lg-3">
        <img src="https://lh3.googleusercontent.com/4eTJrSN47RQ8Q0-BN4geP9BTl8voOZqz7SwW9ZRMHwRaeLdsDWWDSrHdZqa9ikeuXkQ=w720-h310-rw">
      </div>
      <div class="col-lg-3">
        <img src="https://lh3.googleusercontent.com/xBqzldFetFVLD31MjsLKUzgHcucz2apOduazlB8CgIlJyQYpM2X_XEOF9HzqXW8EsZ3L=w720-h310-rw">
      </div>
    
    </div>
  </div>
  <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
      
  </div>
</div>
</div>
  </div>
</div>


<div class="text-center m-3">
  <div>info@autoanddrive.com</div>
  <div class="text-primary">+970597557799</div>
</div>


<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>
</html>